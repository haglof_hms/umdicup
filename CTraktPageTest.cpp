#include "stdafx.h"
#include "CTraktPageTest.h"

#include "ResLangFileReader.h"
#include "UMDiCupGenerics.h"
//#include ".\dicuptraktpages.h"

// -------------------------------------------------------------------------
//	Trakt Page 4 Natur och milj�
// -------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CTraktPageTest, CXTResizeFormView)
BEGIN_MESSAGE_MAP(CTraktPageTest, CXTResizeFormView)	
END_MESSAGE_MAP()

CTraktPageTest::CTraktPageTest()
	: CXTResizeFormView(CTraktPageTest::IDD)
{
	m_bInitialized=FALSE;
}
CTraktPageTest::~CTraktPageTest()
{
}

void CTraktPageTest::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_MYR,m_wndCBox_Trakt_Page4_Myr);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_HALLMARK,m_wndCBox_Trakt_Page4_Hallmark);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_ORORDA,m_wndCBox_Trakt_Page4_OmrOrorda);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_SKROTAS,m_wndCBox_Trakt_Page4_ObjSkrotas);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_KULTUR,m_wndCBox_Trakt_Page4_Kulturminnen);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_FORN,m_wndCBox_Trakt_Page4_Fornminnen);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_SKYDDSZ,m_wndCBox_Trakt_Page4_Skyddszoner);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_NATUR,m_wndCBox_Trakt_Page4_Naturtrad);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_FRAMTID,m_wndCBox_Trakt_Page4_Framtidstrad);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_HOGST,m_wndCBox_Trakt_Page4_Hogstubbar);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_DODA,m_wndCBox_Trakt_Page4_Dodatrad);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_KALYTE,m_wndCBox_Trakt_Page4_Kalytebegr);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_SKRAP,m_wndCBox_Trakt_Page4_Nedskrap);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_AVVTID,m_wndCBox_Trakt_Page4_AvvTidp);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_UPPLEV,m_wndCBox_Trakt_Page4_Upplevelse);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE4_TEKN,m_wndCBox_Trakt_Page4_Tekn);
	
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_NATUR,m_wndEdit_Trakt_Page4_Natur);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_FRAMTID,m_wndEdit_Trakt_Page4_Framtid);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_HOGST,m_wndEdit_Trakt_Page4_Hogst);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_OVRIGHANSYN,m_wndEdit_Trakt_Page4_OvrighHansyn);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_SPARADAREALHA,m_wndEdit_Trakt_Page4_SparadArealHa);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_SPARADAREALPROC,m_wndEdit_Trakt_Page4_SparadArealProc);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_SPARADVOLYMHA,m_wndEdit_Trakt_Page4_SparadVolymHa);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_SPARADVOLYMPROC,m_wndEdit_Trakt_Page4_SparadVolymProc);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_HOGSTUBBST,m_wndEdit_Trakt_Page4_HogstubbSt);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_HOGSTUBBSTHA,m_wndEdit_Trakt_Page4_HogstubbStHa);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_FRAMTNATURST,m_wndEdit_Trakt_Page4_FramNaturSt);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_FRAMTNATURSTHA,m_wndEdit_Trakt_Page4_FramNaturStHa);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE4_OVRIGKOMMENTAR,m_wndEdit_Trakt_Page4_OvrigaKomment);

}

void CTraktPageTest::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

		m_wndCBox_Trakt_Page4_Myr.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Myr.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Hallmark.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Hallmark.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_OmrOrorda.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_OmrOrorda.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_ObjSkrotas.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_ObjSkrotas.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Kulturminnen.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Kulturminnen.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Fornminnen.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Fornminnen.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Skyddszoner.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Skyddszoner.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Naturtrad.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Naturtrad.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Framtidstrad.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Framtidstrad.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Hogstubbar.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Hogstubbar.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Dodatrad.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Dodatrad.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Kalytebegr.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Kalytebegr.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Nedskrap.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Nedskrap.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_AvvTidp.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_AvvTidp.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Upplevelse.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Upplevelse.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page4_Tekn.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page4_Tekn.SetDisabledColor(BLACK,INFOBK);

		m_wndEdit_Trakt_Page4_Natur.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_Natur.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_Natur.SetAsNumeric();

		m_wndEdit_Trakt_Page4_Framtid.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_Framtid.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_Framtid.SetAsNumeric();

		m_wndEdit_Trakt_Page4_Hogst.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_Hogst.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_Hogst.SetAsNumeric();

		m_wndEdit_Trakt_Page4_OvrighHansyn.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_OvrighHansyn.SetDisabledColor(BLACK,INFOBK);

		m_wndEdit_Trakt_Page4_SparadArealHa.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_SparadArealHa.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_SparadArealHa.SetAsNumeric();

		m_wndEdit_Trakt_Page4_SparadArealProc.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_SparadArealProc.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_SparadArealProc.SetAsNumeric();

		m_wndEdit_Trakt_Page4_SparadVolymHa.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_SparadVolymHa.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_SparadVolymHa.SetAsNumeric();

		m_wndEdit_Trakt_Page4_SparadVolymProc.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_SparadVolymProc.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_SparadVolymProc.SetAsNumeric();

		m_wndEdit_Trakt_Page4_HogstubbSt.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_HogstubbSt.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_HogstubbSt.SetAsNumeric();

		m_wndEdit_Trakt_Page4_HogstubbStHa.SetEnabledColor(BLACK,WHITE);	
		m_wndEdit_Trakt_Page4_HogstubbStHa.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_HogstubbStHa.SetAsNumeric();

		m_wndEdit_Trakt_Page4_FramNaturSt.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_FramNaturSt.SetDisabledColor(BLACK,INFOBK);

		m_wndEdit_Trakt_Page4_FramNaturStHa.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_FramNaturStHa.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page4_FramNaturStHa.SetAsNumeric();

		m_wndEdit_Trakt_Page4_OvrigaKomment.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page4_OvrigaKomment.SetDisabledColor(BLACK,INFOBK);

		clearAll();
		setTraktValArrays();
		resetIsDirty();
		m_bInitialized = TRUE;
	}
}

void CTraktPageTest::clearAll(void)
{
		m_wndCBox_Trakt_Page4_Myr.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Hallmark.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_OmrOrorda.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_ObjSkrotas.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Kulturminnen.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Fornminnen.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Skyddszoner.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Naturtrad.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Framtidstrad.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Hogstubbar.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Dodatrad.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Kalytebegr.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Nedskrap.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_AvvTidp.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Upplevelse.SetCurSel(-1);
	m_wndCBox_Trakt_Page4_Tekn.SetCurSel(-1);

	m_wndEdit_Trakt_Page4_Natur.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_Framtid.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_Hogst.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_OvrighHansyn.SetWindowText(_T(""));

	m_wndEdit_Trakt_Page4_SparadArealHa.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_SparadArealProc.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_SparadVolymHa.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_SparadVolymProc.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_HogstubbSt.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_HogstubbStHa.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_FramNaturSt.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_FramNaturStHa.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page4_OvrigaKomment.SetWindowText(_T(""));
}

void CTraktPageTest::setAllReadOnly(BOOL ro1)
{
		m_wndCBox_Trakt_Page4_Myr.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Hallmark.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_OmrOrorda.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_ObjSkrotas.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Kulturminnen.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Fornminnen.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Skyddszoner.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Naturtrad.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Framtidstrad.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Hogstubbar.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Dodatrad.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Kalytebegr.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Nedskrap.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_AvvTidp.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Upplevelse.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page4_Tekn.SetReadOnly( ro1 );

	m_wndEdit_Trakt_Page4_Natur.SetReadOnly( TRUE );			//Only for show no editing
	m_wndEdit_Trakt_Page4_Framtid.SetReadOnly( TRUE );			//Only for show no editing
	m_wndEdit_Trakt_Page4_Hogst.SetReadOnly( TRUE );			//Only for show no editing
	m_wndEdit_Trakt_Page4_OvrighHansyn.SetReadOnly( ro1 );

	m_wndEdit_Trakt_Page4_SparadArealHa.SetReadOnly( TRUE );	//Only for show no editing
	m_wndEdit_Trakt_Page4_SparadArealProc.SetReadOnly( TRUE );	//Only for show no editing
	m_wndEdit_Trakt_Page4_SparadVolymHa.SetReadOnly( TRUE );	//Only for show no editing
	m_wndEdit_Trakt_Page4_SparadVolymProc.SetReadOnly( TRUE );	//Only for show no editing
	m_wndEdit_Trakt_Page4_HogstubbSt.SetReadOnly( TRUE );		//Only for show no editing
	m_wndEdit_Trakt_Page4_HogstubbStHa.SetReadOnly( TRUE );		//Only for show no editing
	m_wndEdit_Trakt_Page4_FramNaturSt.SetReadOnly( TRUE );		//Only for show no editing
	m_wndEdit_Trakt_Page4_FramNaturStHa.SetReadOnly( TRUE );	//Only for show no editing
	m_wndEdit_Trakt_Page4_OvrigaKomment.SetReadOnly( ro1);
}

void CTraktPageTest::setEnabled(BOOL enabled)
{
	m_wndCBox_Trakt_Page4_Myr.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Hallmark.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_OmrOrorda.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_ObjSkrotas.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Kulturminnen.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Fornminnen.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Skyddszoner.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Naturtrad.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Framtidstrad.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Hogstubbar.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Dodatrad.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Kalytebegr.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Nedskrap.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_AvvTidp.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Upplevelse.EnableWindow( enabled );
	m_wndCBox_Trakt_Page4_Tekn.EnableWindow( enabled );

	m_wndEdit_Trakt_Page4_OvrighHansyn.EnableWindow( enabled );

	m_wndEdit_Trakt_Page4_OvrigaKomment.EnableWindow( enabled );

	setAllReadOnly(!enabled);	
}

void CTraktPageTest::populateData(TRAKT_DATA data)
{
	CString s_Data=_T("");
	float SparadArealHa=0.0;
	float SparadArealProc=0.0;
	float SparadVolymHa=0.0;
	float SparadVolymProc=0.0;
	float HogstubbSt=0.0;
	float HogstubbStHa=0.0;
	float FramNaturSt=0.0;
	float FramNaturStHa=0.0;
	float areal=data.m_f_traktareal;

	s_Data.Format(_T("%d"),data.m_n_myr);
	if(data.m_n_myr==-1)
		m_wndCBox_Trakt_Page4_Myr.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Myr.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_hallmark);
	if(data.m_n_hallmark==-1)
		m_wndCBox_Trakt_Page4_Hallmark.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Hallmark.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_omrSomLamnOrorda);
	if(data.m_n_omrSomLamnOrorda==-1)
		m_wndCBox_Trakt_Page4_OmrOrorda.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_OmrOrorda.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_objektSomSkallSkotas);
	if(data.m_n_objektSomSkallSkotas==-1)
		m_wndCBox_Trakt_Page4_ObjSkrotas.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_ObjSkrotas.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_kulturminnen);
	if(data.m_n_kulturminnen==-1)
		m_wndCBox_Trakt_Page4_Kulturminnen.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Kulturminnen.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_fornminnen);
	if(data.m_n_fornminnen==-1)
		m_wndCBox_Trakt_Page4_Fornminnen.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Fornminnen.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_skyddszoner);
	if(data.m_n_skyddszoner==-1)
		m_wndCBox_Trakt_Page4_Skyddszoner.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Skyddszoner.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_naturvardestrad);
	if(data.m_n_naturvardestrad==-1)
		m_wndCBox_Trakt_Page4_Naturtrad.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Naturtrad.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_framtidstrad);
	if(data.m_n_framtidstrad==-1)
		m_wndCBox_Trakt_Page4_Framtidstrad.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Framtidstrad.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_farskaHogstubbar);
	if(data.m_n_farskaHogstubbar==-1)
		m_wndCBox_Trakt_Page4_Hogstubbar.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Hogstubbar.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_dodaTrad);
	if(data.m_n_dodaTrad==-1)
		m_wndCBox_Trakt_Page4_Dodatrad.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Dodatrad.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_kalytebegransning);
	if(data.m_n_kalytebegransning==-1)
		m_wndCBox_Trakt_Page4_Kalytebegr.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Kalytebegr.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_nedskrapning);
	if(data.m_n_nedskrapning==-1)
		m_wndCBox_Trakt_Page4_Nedskrap.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Nedskrap.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_avverkningstidpunkt);
	if(data.m_n_avverkningstidpunkt==-1)
		m_wndCBox_Trakt_Page4_AvvTidp.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_AvvTidp.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_upplevelsehansyn);
	if(data.m_n_upplevelsehansyn==-1)
		m_wndCBox_Trakt_Page4_Upplevelse.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Upplevelse.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_teknEkonimp);
	if(data.m_n_teknEkonimp==-1)
		m_wndCBox_Trakt_Page4_Tekn.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page4_Tekn.SelectString(0,s_Data);

	m_wndEdit_Trakt_Page4_Natur.setInt(data.m_n_naturvardestradSum);
	m_wndEdit_Trakt_Page4_Framtid.setInt(data.m_n_framtidstradSum);
	m_wndEdit_Trakt_Page4_Hogst.setInt(data.m_n_farskaHogstubbarSum);
	m_wndEdit_Trakt_Page4_OvrighHansyn.SetWindowText(data.m_s_ovrigHansyn);

	SparadArealHa=data.m_f_sparadArealSum;
	SparadVolymHa=data.m_n_sparadVolymSum;
	HogstubbSt=data.m_n_farskaHogstubbarSum;
	FramNaturSt=data.m_n_framtidstradSum+data.m_n_naturvardestradSum;
	if(areal>0.0)
	{	
		SparadArealProc=SparadArealHa/areal*100.0;	
		SparadVolymProc=SparadVolymHa/areal*100.0;
		HogstubbStHa=HogstubbSt/areal;
		FramNaturStHa=FramNaturSt/areal;
	}

	m_wndEdit_Trakt_Page4_SparadArealHa.setFloat(SparadArealHa,1);
	m_wndEdit_Trakt_Page4_SparadArealProc.setFloat(SparadArealProc,0);
	m_wndEdit_Trakt_Page4_SparadVolymHa.setFloat(SparadVolymHa,1);
	m_wndEdit_Trakt_Page4_SparadVolymProc.setFloat(SparadVolymProc,0);
	m_wndEdit_Trakt_Page4_HogstubbSt.setFloat(HogstubbSt,0);
	m_wndEdit_Trakt_Page4_HogstubbStHa.setFloat(HogstubbStHa,0);
	m_wndEdit_Trakt_Page4_FramNaturSt.setFloat(FramNaturSt,0);
	m_wndEdit_Trakt_Page4_FramNaturStHa.setFloat(FramNaturStHa,0);

	m_wndEdit_Trakt_Page4_OvrigaKomment.SetWindowText(data.m_s_ovrigaKommentarerNaturOchMiljo);

	setAllReadOnly(FALSE);
	resetIsDirty();
}

void CTraktPageTest::getEnteredData(TRAKT_DATA *trakt)
{	
	trakt->m_n_myr					=	m_wndCBox_Trakt_Page4_Myr.GetItemData(m_wndCBox_Trakt_Page4_Myr.GetCurSel());
	trakt->m_n_hallmark				=	m_wndCBox_Trakt_Page4_Hallmark.GetItemData(m_wndCBox_Trakt_Page4_Hallmark.GetCurSel());
	trakt->m_n_omrSomLamnOrorda		=	m_wndCBox_Trakt_Page4_OmrOrorda.GetItemData(m_wndCBox_Trakt_Page4_OmrOrorda.GetCurSel());
	trakt->m_n_objektSomSkallSkotas	=	m_wndCBox_Trakt_Page4_ObjSkrotas.GetItemData(m_wndCBox_Trakt_Page4_ObjSkrotas.GetCurSel());
	trakt->m_n_kulturminnen			=	m_wndCBox_Trakt_Page4_Kulturminnen.GetItemData(m_wndCBox_Trakt_Page4_Kulturminnen.GetCurSel());
	trakt->m_n_fornminnen			=	m_wndCBox_Trakt_Page4_Fornminnen.GetItemData(m_wndCBox_Trakt_Page4_Fornminnen.GetCurSel());
	trakt->m_n_skyddszoner			=	m_wndCBox_Trakt_Page4_Skyddszoner.GetItemData(m_wndCBox_Trakt_Page4_Skyddszoner.GetCurSel());
	trakt->m_n_naturvardestrad		=	m_wndCBox_Trakt_Page4_Naturtrad.GetItemData(m_wndCBox_Trakt_Page4_Naturtrad.GetCurSel());
	trakt->m_n_framtidstrad			=	m_wndCBox_Trakt_Page4_Framtidstrad.GetItemData(m_wndCBox_Trakt_Page4_Framtidstrad.GetCurSel());
	trakt->m_n_farskaHogstubbar		=	m_wndCBox_Trakt_Page4_Hogstubbar.GetItemData(m_wndCBox_Trakt_Page4_Hogstubbar.GetCurSel());
	trakt->m_n_dodaTrad				=	m_wndCBox_Trakt_Page4_Dodatrad.GetItemData(m_wndCBox_Trakt_Page4_Dodatrad.GetCurSel());
	trakt->m_n_kalytebegransning	=	m_wndCBox_Trakt_Page4_Kalytebegr.GetItemData(m_wndCBox_Trakt_Page4_Kalytebegr.GetCurSel());
	trakt->m_n_nedskrapning			=	m_wndCBox_Trakt_Page4_Nedskrap.GetItemData(m_wndCBox_Trakt_Page4_Nedskrap.GetCurSel());
	trakt->m_n_avverkningstidpunkt	=	m_wndCBox_Trakt_Page4_AvvTidp.GetItemData(m_wndCBox_Trakt_Page4_AvvTidp.GetCurSel());
	trakt->m_n_upplevelsehansyn		=	m_wndCBox_Trakt_Page4_Upplevelse.GetItemData(m_wndCBox_Trakt_Page4_Upplevelse.GetCurSel());
	trakt->m_n_teknEkonimp			=	m_wndCBox_Trakt_Page4_Tekn.GetItemData(m_wndCBox_Trakt_Page4_Tekn.GetCurSel());
	
	trakt->m_s_ovrigHansyn			=	m_wndEdit_Trakt_Page4_OvrighHansyn.getText();
	trakt->m_s_ovrigaKommentarerNaturOchMiljo=m_wndEdit_Trakt_Page4_OvrigaKomment.getText();
	
}

BOOL CTraktPageTest::getIsDirty(void)
{
	if (m_wndCBox_Trakt_Page4_Myr.isDirty() ||
	m_wndCBox_Trakt_Page4_Hallmark.isDirty() ||
	m_wndCBox_Trakt_Page4_OmrOrorda.isDirty() ||
	m_wndCBox_Trakt_Page4_ObjSkrotas.isDirty() ||
	m_wndCBox_Trakt_Page4_Kulturminnen.isDirty() ||
	m_wndCBox_Trakt_Page4_Fornminnen.isDirty() ||
	m_wndCBox_Trakt_Page4_Skyddszoner.isDirty() ||
	m_wndCBox_Trakt_Page4_Naturtrad.isDirty() ||
	m_wndCBox_Trakt_Page4_Framtidstrad.isDirty() ||
	m_wndCBox_Trakt_Page4_Hogstubbar.isDirty() ||
	m_wndCBox_Trakt_Page4_Dodatrad.isDirty() ||
	m_wndCBox_Trakt_Page4_Kalytebegr.isDirty() ||
	m_wndCBox_Trakt_Page4_Nedskrap.isDirty() ||
	m_wndCBox_Trakt_Page4_AvvTidp.isDirty() ||
	m_wndCBox_Trakt_Page4_Upplevelse.isDirty() ||
	m_wndCBox_Trakt_Page4_Tekn.isDirty() ||
	m_wndEdit_Trakt_Page4_OvrighHansyn.isDirty() ||
	m_wndEdit_Trakt_Page4_OvrigaKomment.isDirty())
	{
		return TRUE;
	}
	return FALSE;
}

void CTraktPageTest::resetIsDirty(void)
{
		m_wndCBox_Trakt_Page4_Myr.resetIsDirty();
	m_wndCBox_Trakt_Page4_Hallmark.resetIsDirty();
	m_wndCBox_Trakt_Page4_OmrOrorda.resetIsDirty();
	m_wndCBox_Trakt_Page4_ObjSkrotas.resetIsDirty();
	m_wndCBox_Trakt_Page4_Kulturminnen.resetIsDirty();
	m_wndCBox_Trakt_Page4_Fornminnen.resetIsDirty();
	m_wndCBox_Trakt_Page4_Skyddszoner.resetIsDirty();
	m_wndCBox_Trakt_Page4_Naturtrad.resetIsDirty();
	m_wndCBox_Trakt_Page4_Framtidstrad.resetIsDirty();
	m_wndCBox_Trakt_Page4_Hogstubbar.resetIsDirty();
	m_wndCBox_Trakt_Page4_Dodatrad.resetIsDirty();
	m_wndCBox_Trakt_Page4_Kalytebegr.resetIsDirty();
	m_wndCBox_Trakt_Page4_Nedskrap.resetIsDirty();
	m_wndCBox_Trakt_Page4_AvvTidp.resetIsDirty();
	m_wndCBox_Trakt_Page4_Upplevelse.resetIsDirty();
	m_wndCBox_Trakt_Page4_Tekn.resetIsDirty();
	m_wndEdit_Trakt_Page4_OvrighHansyn.resetIsDirty();
	m_wndEdit_Trakt_Page4_OvrigaKomment.resetIsDirty();
}

void CTraktPageTest::setTraktValArrays()
{
	TRAKT_VAL_DATA data;
	CString s_Data=_T("");
	UINT i=0;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			vec_Trakt_Page4_Myr.clear();
			vec_Trakt_Page4_Myr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Myr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7090)));
			vec_Trakt_Page4_Myr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Myr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Hallmark.clear();
			vec_Trakt_Page4_Hallmark.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Hallmark.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7090)));
			vec_Trakt_Page4_Hallmark.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Hallmark.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_OmrOrorda.clear();
			vec_Trakt_Page4_OmrOrorda.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_OmrOrorda.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7090)));
			vec_Trakt_Page4_OmrOrorda.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_OmrOrorda.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7095)),xml->str(IDS_STRING7094)));
			vec_Trakt_Page4_OmrOrorda.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_ObjSkrotas.clear();
			vec_Trakt_Page4_ObjSkrotas.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_ObjSkrotas.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7090)));
			vec_Trakt_Page4_ObjSkrotas.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_ObjSkrotas.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Kulturminnen.clear();
			vec_Trakt_Page4_Kulturminnen.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Kulturminnen.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7090)));
			vec_Trakt_Page4_Kulturminnen.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Kulturminnen.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Fornminnen.clear();
			vec_Trakt_Page4_Fornminnen.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Fornminnen.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7090)));
			vec_Trakt_Page4_Fornminnen.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Fornminnen.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Skyddszoner.clear();
			vec_Trakt_Page4_Skyddszoner.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Skyddszoner.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7090)));
			vec_Trakt_Page4_Skyddszoner.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Skyddszoner.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7095)),xml->str(IDS_STRING7094)));
			vec_Trakt_Page4_Skyddszoner.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Naturtrad.clear();
			vec_Trakt_Page4_Naturtrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Naturtrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Naturtrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Framtidstrad.clear();
			vec_Trakt_Page4_Framtidstrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Framtidstrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Framtidstrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7095)),xml->str(IDS_STRING7094)));
			vec_Trakt_Page4_Framtidstrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Hogstubbar.clear();
			vec_Trakt_Page4_Hogstubbar.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Hogstubbar.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Hogstubbar.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7094)));
			vec_Trakt_Page4_Hogstubbar.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Dodatrad.clear();
			vec_Trakt_Page4_Dodatrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Dodatrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Dodatrad.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Kalytebegr.clear();
			vec_Trakt_Page4_Kalytebegr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Kalytebegr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Kalytebegr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7095)),xml->str(IDS_STRING7094)));
			vec_Trakt_Page4_Kalytebegr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));
			
			vec_Trakt_Page4_Nedskrap.clear();
			vec_Trakt_Page4_Nedskrap.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Nedskrap.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7099)),xml->str(IDS_STRING7098)));
			vec_Trakt_Page4_Nedskrap.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7101)),xml->str(IDS_STRING7100)));	

			vec_Trakt_Page4_AvvTidp.clear();
			vec_Trakt_Page4_AvvTidp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_AvvTidp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7103)),xml->str(IDS_STRING7102)));
			vec_Trakt_Page4_AvvTidp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7105)),xml->str(IDS_STRING7104)));

			vec_Trakt_Page4_Upplevelse.clear();
			vec_Trakt_Page4_Upplevelse.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Upplevelse.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7090)));
			vec_Trakt_Page4_Upplevelse.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Upplevelse.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7095)),xml->str(IDS_STRING7094)));
			vec_Trakt_Page4_Upplevelse.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

			vec_Trakt_Page4_Tekn.clear();
			vec_Trakt_Page4_Tekn.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page4_Tekn.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7091)),xml->str(IDS_STRING7090)));
			vec_Trakt_Page4_Tekn.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7093)),xml->str(IDS_STRING7092)));
			vec_Trakt_Page4_Tekn.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7095)),xml->str(IDS_STRING7094)));
			vec_Trakt_Page4_Tekn.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7097)),xml->str(IDS_STRING7096)));

		}
	}

	for (i = 0;i < vec_Trakt_Page4_Myr.size();i++)
	{
		data = vec_Trakt_Page4_Myr[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Myr.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Myr.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page4_Hallmark.size();i++)
	{
		data = vec_Trakt_Page4_Hallmark[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Hallmark.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Hallmark.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page4_OmrOrorda.size();i++)
	{
		data = vec_Trakt_Page4_OmrOrorda[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_OmrOrorda.AddString(s_Data);
		m_wndCBox_Trakt_Page4_OmrOrorda.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page4_ObjSkrotas.size();i++)
	{
		data = vec_Trakt_Page4_ObjSkrotas[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_ObjSkrotas.AddString(s_Data);
		m_wndCBox_Trakt_Page4_ObjSkrotas.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page4_Kulturminnen.size();i++)
	{
		data = vec_Trakt_Page4_Kulturminnen[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Kulturminnen.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Kulturminnen.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page4_Fornminnen.size();i++)
	{
		data = vec_Trakt_Page4_Fornminnen[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Fornminnen.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Fornminnen.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page4_Skyddszoner.size();i++)
	{
		data = vec_Trakt_Page4_Skyddszoner[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Skyddszoner.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Skyddszoner.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page4_Naturtrad.size();i++)
	{
		data = vec_Trakt_Page4_Naturtrad[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Naturtrad.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Naturtrad.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page4_Framtidstrad.size();i++)
	{
		data = vec_Trakt_Page4_Framtidstrad[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Framtidstrad.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Framtidstrad.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page4_Hogstubbar.size();i++)
	{
		data = vec_Trakt_Page4_Hogstubbar[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Hogstubbar.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Hogstubbar.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page4_Dodatrad.size();i++)
	{
		data = vec_Trakt_Page4_Dodatrad[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Dodatrad.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Dodatrad.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page4_Kalytebegr.size();i++)
	{
		data = vec_Trakt_Page4_Kalytebegr[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Kalytebegr.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Kalytebegr.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page4_Nedskrap.size();i++)
	{
		data = vec_Trakt_Page4_Nedskrap[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Nedskrap.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Nedskrap.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page4_AvvTidp.size();i++)
	{
		data = vec_Trakt_Page4_AvvTidp[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_AvvTidp.AddString(s_Data);
		m_wndCBox_Trakt_Page4_AvvTidp.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page4_Upplevelse.size();i++)
	{
		data = vec_Trakt_Page4_Upplevelse[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Upplevelse.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Upplevelse.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page4_Tekn.size();i++)
	{
		data = vec_Trakt_Page4_Tekn[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page4_Tekn.AddString(s_Data);
		m_wndCBox_Trakt_Page4_Tekn.SetItemData(i, data.m_nID);		
	}
}
