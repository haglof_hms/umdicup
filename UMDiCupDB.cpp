#include "StdAfx.h"

#include "UMDiCupDB.h"

//////////////////////////////////////////////////////////////////////////////////
// CDiCupDBDB; Handle ALL transactions for DiCup

CDiCupDB::CDiCupDB() 
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CDiCupDB::CDiCupDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CDiCupDB::CDiCupDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}


// PROTECTED

// Check if USER set a question for the Machine. Can be by Region
// or by Region and District; 061010 p�d
void CDiCupDB::getSQLMachineQuestion(LPTSTR sql)
{
	FILE *fp;
	SQL_MACHINE_QUESTION recMachine;
	CString sPathAndFN;

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_MACHINE_QUESTION_FN);

	if (fileExists(sPathAndFN))
	{
		if ((fp = _tfopen(sPathAndFN,_T("rb"))) != NULL)
		{
			fread(&recMachine,sizeof(SQL_MACHINE_QUESTION),1,fp);
			fclose(fp);
			_tcscpy(sql,recMachine.szSQL);
		}
	}
	else
	{
		_stprintf(sql,SQL_MACHINE_QUESTION1,TBL_MACHINE,TBL_DISTRICT,TBL_REGION);
	}
}

void CDiCupDB::getSQLTraktQuestion(LPTSTR sql)
{
	FILE *fp;
	SQL_TRAKT_QUESTION recTrakt;
	CString sPathAndFN;
	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_TRAKT_QUESTION_FN);

	if (fileExists(sPathAndFN))
	{
		if ((fp = _tfopen(sPathAndFN,_T("rb"))) != NULL)
		{
			fread(&recTrakt,sizeof(SQL_TRAKT_QUESTION),1,fp);
			fclose(fp);
			_tcscpy(sql,recTrakt.szSQL);
		}
	}
	else
	{
		_stprintf(sql,SQL_TRAKT_QUESTION1,TBL_TRAKT);
	}
}


// PUBLIC

// REGIONS

// Handle data in region_table; 061002 p�d
BOOL CDiCupDB::getRegions(vecRegionData &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_REGION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(_region_data(m_saCommand.Field(1).asShort(), m_saCommand.Field(2).asString()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

// DISTRICTS

// Handle data in district_table; 061002 p�d
BOOL CDiCupDB::getDistricts(vecDistrictData &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select a.region_num,a.region_name,b.district_num,b.district_name from ")
					_T("%s a,%s b ")
					_T("where b.district_region_num = a.region_num ")
					_T("order by a.region_num, b.district_num"),TBL_REGION,TBL_DISTRICT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(_district_data(m_saCommand.Field(1).asShort(),
										 m_saCommand.Field(2).asString(),
										 m_saCommand.Field(3).asShort(),
										 m_saCommand.Field(4).asString()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

// MACHINES

// Handle data in machine_table; 061005 p�d

BOOL CDiCupDB::isThereAnyMachineData(void)
{
	int	nCnt = 0;
	BOOL bReturn;
	TCHAR szBuffer[1024];
	try
	{
		getSQLMachineQuestion(szBuffer);
		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{	
			nCnt++;
			if (nCnt > 0)
				break;
		}		

		m_saConnection.Commit();

		bReturn = (nCnt > 0);

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CDiCupDB::getMachines(vecMachineData &vec)
{

	TCHAR szBuffer[1024];
	try
	{
		vec.clear();
		getSQLMachineQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(_machine_data(m_saCommand.Field(1).asShort(),
																m_saCommand.Field(2).asString(),
																m_saCommand.Field(3).asShort(),
																m_saCommand.Field(4).asString(),
																m_saCommand.Field(5).asShort(),
																m_saCommand.Field(6).asString()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CDiCupDB::addMachine(MACHINE_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where machine_num = %d and machine_district_num = %d and machine_region_num = %d"),
			TBL_MACHINE,rec.m_nMachineID,rec.m_nDistrictID,rec.m_nRegionID);
		if (!exists(sSQL))
		{
			sSQL.Format(_T("insert into %s (machine_num,machine_region_num,machine_district_num,machine_contractor_num) values(:1,:2,:3,:4)"),TBL_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nMachineID;
			m_saCommand.Param(2).setAsShort() = rec.m_nRegionID;
			m_saCommand.Param(3).setAsShort() = rec.m_nDistrictID;
			m_saCommand.Param(4).setAsString() = rec.m_sContractorName;

			m_saCommand.Execute();
			
			m_saConnection.Commit();
			
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// Update contractor, based on Region,District and Machine; 061010 p�d
BOOL CDiCupDB::updMachine(MACHINE_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		sSQL.Format(_T("select * from %s where machine_num = %d and machine_district_num = %d and machine_region_num = %d"),
					TBL_MACHINE,rec.m_nMachineID,rec.m_nDistrictID,rec.m_nRegionID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("update %s set machine_contractor_num = :1 ")
						_T("where machine_num = :2 and machine_region_num = :3 and machine_district_num = :4"),TBL_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString() = rec.m_sContractorName;
			m_saCommand.Param(2).setAsShort() = rec.m_nMachineID;
			m_saCommand.Param(3).setAsShort() = rec.m_nRegionID;
			m_saCommand.Param(4).setAsShort() = rec.m_nDistrictID;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CDiCupDB::delMachine(MACHINE_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where machine_num = %d and machine_district_num = %d and machine_region_num = %d"),
					TBL_MACHINE,rec.m_nMachineID,rec.m_nDistrictID,rec.m_nRegionID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where machine_num = :1 and machine_region_num = :2 and machine_district_num = :3"),TBL_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nMachineID;
			m_saCommand.Param(2).setAsShort() = rec.m_nRegionID;
			m_saCommand.Param(3).setAsShort() = rec.m_nDistrictID;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// TRAKT

// Handle data in trakt_table; 061005 p�d

BOOL CDiCupDB::isThereAnyTraktData(void)
{
	int	nCnt = 0;
	BOOL bReturn;
	TCHAR szBuffer[1024];
	try
	{
		getSQLTraktQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{	
			nCnt++;
			if (nCnt > 0)
				break;
		}		

		m_saConnection.Commit();

		bReturn = (nCnt > 0);

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CDiCupDB::getTrakts(vecTraktData &vec)
{
	TCHAR szBuffer[1024];
	try
	{
		vec.clear();
		getSQLTraktQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(_trakt_data(
				m_saCommand.Field("typ").asShort(),
				m_saCommand.Field("region").asShort(),
				m_saCommand.Field("distrikt").asShort(),
				m_saCommand.Field("ursprung").asShort(),
				m_saCommand.Field("traktnr").asLong(),
				m_saCommand.Field("maskinlagnr").asShort(),
				m_saCommand.Field("traktnamn").asString(),
				m_saCommand.Field("avvEnlRus").asLong(),
				m_saCommand.Field("finnsMiljorapport").asLong(),
				m_saCommand.Field("traktareal").asDouble(),
				m_saCommand.Field("avverkadVolym").asDouble(),						
				m_saCommand.Field("maskinlagnamn").asString(),
				m_saCommand.Field("datumInv").asString(),
				m_saCommand.Field("datumSlutfordAvv").asString(),
				m_saCommand.Field("utfortGrotsk").asLong(),
				m_saCommand.Field("datumSlutfordGrotsk").asString(),
				m_saCommand.Field("naturvardestradSum").asLong(),
				m_saCommand.Field("framtidstradSum").asLong(),
				m_saCommand.Field("farskaHogstubbarSum").asLong(),
				m_saCommand.Field("sparadArealSum").asDouble(),
				m_saCommand.Field("sparadVolymSum").asLong(),
				m_saCommand.Field("kvarglomtVirkeSum").asLong(),
				m_saCommand.Field("risadeBasstrak").asLong(),
				m_saCommand.Field("hansynskrBiotoper").asLong(),
				m_saCommand.Field("mindreAllvKorskada").asLong(),
				m_saCommand.Field("mindreAllvKorskadaTyp").asLong(),
				m_saCommand.Field("allvarligKorskada").asLong(),
				m_saCommand.Field("allvarligKorskadaTyp").asLong(),
				m_saCommand.Field("forebyggtKorskador").asLong(),
				m_saCommand.Field("forebyggtKorskadorKommentar").asString(),
				m_saCommand.Field("basvagUtanforTrakt").asLong(),
				m_saCommand.Field("kommentarAvKorskador").asString(),
				m_saCommand.Field("skadadeVagdiken").asLong(),
				m_saCommand.Field("parallellitet").asLong(),
				m_saCommand.Field("hogarnasBeskaffenhet").asLong(),
				m_saCommand.Field("placeringAvHogar").asLong(),
				m_saCommand.Field("innehallHogar").asLong(),
				m_saCommand.Field("kvalitetPaValtor").asLong(),
				m_saCommand.Field("kommentarPaGrottillredning").asString(),
				m_saCommand.Field("myr").asLong(),
				m_saCommand.Field("hallmark").asLong(),
				m_saCommand.Field("omrSomLamnOrorda").asLong(),
				m_saCommand.Field("objektSomSkallSkotas").asLong(),
				m_saCommand.Field("kulturminnen").asLong(),
				m_saCommand.Field("fornminnen").asLong(),
				m_saCommand.Field("skyddszoner").asLong(),
				m_saCommand.Field("naturvardestrad").asLong(),
				m_saCommand.Field("framtidstrad").asLong(),
				m_saCommand.Field("farskaHogstubbar").asLong(),
				m_saCommand.Field("dodaTrad").asLong(),
				m_saCommand.Field("kalytebegransning").asLong(),
				m_saCommand.Field("nedskrapning").asLong(),
				m_saCommand.Field("avverkningstidpunkt").asLong(),
				m_saCommand.Field("upplevelsehansyn").asLong(),
				m_saCommand.Field("teknEkonimp").asLong(),
				m_saCommand.Field("ovrigHansyn").asString(),
				m_saCommand.Field("ovrigaKommentarerNaturOchMiljo").asString(),
				m_saCommand.Field("baskrTraktplOK").asLong(),
				m_saCommand.Field("baskrTraktplBrister").asString(),
				m_saCommand.Field("avvikandeArbUtifranTraktdirektivet").asString(),
				m_saCommand.Field("hansynTillEfterkommandeAtgarder").asString(),
				m_saCommand.Field("stubbhojd").asLong(),
				m_saCommand.Field("klaratStrategiskaMal").asLong(),
				m_saCommand.Field("forbattringsforslag").asString(),
				m_saCommand.Field("EnterType").asLong()
				));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CDiCupDB::addTrakt(TRAKT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{

		sSQL.Format(_T("select * from %s where maskinlagnr = %d and distrikt = %d and region = %d and ")
					_T("traktnr = %d and typ =%d and ursprung = %d"),
					TBL_TRAKT,rec.m_n_maskinlagnr,rec.m_n_di,rec.m_n_rg,rec.m_n_traktnr,
					rec.m_n_typAvUppfoljning,rec.m_n_ursprung);
		if (!exists(sSQL))
		{

			sSQL.Format(
				_T("insert into %s ")
				_T("(typ,region,distrikt,ursprung,traktnr,maskinlagnr, ")
				_T("traktnamn,avvEnlRus,finnsMiljorapport,traktareal,avverkadVolym,maskinlagnamn,datumInv, ")
				_T("datumSlutfordAvv,utfortGrotsk,datumSlutfordGrotsk,naturvardestradSum,framtidstradSum,farskaHogstubbarSum, ")
				_T("sparadArealSum,sparadVolymSum,kvarglomtVirkeSum,risadeBasstrak,hansynskrBiotoper,mindreAllvKorskada, ")
				_T("mindreAllvKorskadaTyp,allvarligKorskada,allvarligKorskadaTyp,forebyggtKorskador,forebyggtKorskadorKommentar, ")
				_T("basvagUtanforTrakt,kommentarAvKorskador,skadadeVagdiken,parallellitet,hogarnasBeskaffenhet,placeringAvHogar, ")
				_T("innehallHogar,kvalitetPaValtor,kommentarPaGrottillredning,myr,hallmark,omrSomLamnOrorda,objektSomSkallSkotas, ")
				_T("kulturminnen,fornminnen,skyddszoner,naturvardestrad,framtidstrad,farskaHogstubbar,dodaTrad,kalytebegransning, ")
				_T("nedskrapning,avverkningstidpunkt,upplevelsehansyn,teknEkonimp,ovrigHansyn,ovrigaKommentarerNaturOchMiljo, ")
				_T("baskrTraktplOK,baskrTraktplBrister,avvikandeArbUtifranTraktdirektivet,hansynTillEfterkommandeAtgarder, ")
				_T("stubbhojd,klaratStrategiskaMal,forbattringsforslag,EnterType) ")
				_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,")
				_T(":38,:39,:40,:41,:42,:43,:44,:45,:46,:47,:48,:49,:50,:51,:52,:53,:54,:55,:56,:57,:58,:59,:60,:61,:62,:63,:64,:65)"),TBL_TRAKT);

			m_saCommand.setCommandText((SAString)sSQL);


			m_saCommand.Param(1).setAsShort()	=rec.m_n_typAvUppfoljning;
			m_saCommand.Param(2).setAsShort()	=rec.m_n_rg;
			m_saCommand.Param(3).setAsShort()	=rec.m_n_di;
			m_saCommand.Param(4).setAsShort()	=rec.m_n_ursprung;
			m_saCommand.Param(5).setAsLong()	=rec.m_n_traktnr;
			m_saCommand.Param(6).setAsShort()	=rec.m_n_maskinlagnr;

			m_saCommand.Param(7).setAsString()	=rec.m_s_traktnamn;
			m_saCommand.Param(8).setAsLong()	=rec.m_n_avvEnlRus;
			m_saCommand.Param(9).setAsLong()	=rec.m_n_finnsMiljorapport;
			m_saCommand.Param(10).setAsDouble()	=rec.m_f_traktareal;
			m_saCommand.Param(11).setAsDouble()	=rec.m_f_avverkadVolym;	
			m_saCommand.Param(12).setAsString()	=rec.m_s_maskinlagnamn;
			m_saCommand.Param(13).setAsString()	=rec.m_s_datumInv;
			m_saCommand.Param(14).setAsString()	=rec.m_s_datumSlutfordAvv;
			m_saCommand.Param(15).setAsLong()	=rec.m_n_utfortGrotsk;
			m_saCommand.Param(16).setAsString()	=rec.m_s_datumSlutfordGrotsk;
			m_saCommand.Param(17).setAsLong()	=rec.m_n_naturvardestradSum;
			m_saCommand.Param(18).setAsLong()	=rec.m_n_framtidstradSum;
			m_saCommand.Param(19).setAsLong()	=rec.m_n_farskaHogstubbarSum;
			m_saCommand.Param(20).setAsDouble()	=rec.m_f_sparadArealSum;
			m_saCommand.Param(21).setAsLong()	=rec.m_n_sparadVolymSum;
			m_saCommand.Param(22).setAsLong()	=rec.m_n_kvarglomtVirkeSum;
			m_saCommand.Param(23).setAsLong()	=rec.m_n_risadeBasstrak;
			m_saCommand.Param(24).setAsLong()	=rec.m_n_hansynskrBiotoper;
			m_saCommand.Param(25).setAsLong()	=rec.m_n_mindreAllvKorskada;
			m_saCommand.Param(26).setAsLong()	=rec.m_n_mindreAllvKorskadaTyp;
			m_saCommand.Param(27).setAsLong()	=rec.m_n_allvarligKorskada;
			m_saCommand.Param(28).setAsLong()	=rec.m_n_allvarligKorskadaTyp;
			m_saCommand.Param(29).setAsLong()	=rec.m_n_forebyggtKorskador;
			m_saCommand.Param(30).setAsString()	=rec.m_s_forebyggtKorskadorKommentar;
			m_saCommand.Param(31).setAsLong()	=rec.m_n_basvagUtanforTrakt;
			m_saCommand.Param(32).setAsString()	=rec.m_s_kommentarAvKorskador;
			m_saCommand.Param(33).setAsLong()	=rec.m_n_skadadeVagdiken;
			m_saCommand.Param(34).setAsLong()	=rec.m_n_parallellitet;
			m_saCommand.Param(35).setAsLong()	=rec.m_n_hogarnasBeskaffenhet;
			m_saCommand.Param(36).setAsLong()	=rec.m_n_placeringAvHogar;
			m_saCommand.Param(37).setAsLong()	=rec.m_n_innehallHogar;
			m_saCommand.Param(38).setAsLong()	=rec.m_n_kvalitetPaValtor;
			m_saCommand.Param(39).setAsString()	=rec.m_s_kommentarPaGrottillredning;
			m_saCommand.Param(40).setAsLong()	=rec.m_n_myr;
			m_saCommand.Param(41).setAsLong()	=rec.m_n_hallmark;
			m_saCommand.Param(42).setAsLong()	=rec.m_n_omrSomLamnOrorda;
			m_saCommand.Param(43).setAsLong()	=rec.m_n_objektSomSkallSkotas;
			m_saCommand.Param(44).setAsLong()	=rec.m_n_kulturminnen;
			m_saCommand.Param(45).setAsLong()	=rec.m_n_fornminnen;
			m_saCommand.Param(46).setAsLong()	=rec.m_n_skyddszoner;
			m_saCommand.Param(47).setAsLong()	=rec.m_n_naturvardestrad;
			m_saCommand.Param(48).setAsLong()	=rec.m_n_framtidstrad;
			m_saCommand.Param(49).setAsLong()	=rec.m_n_farskaHogstubbar;
			m_saCommand.Param(50).setAsLong()	=rec.m_n_dodaTrad;
			m_saCommand.Param(51).setAsLong()	=rec.m_n_kalytebegransning;
			m_saCommand.Param(52).setAsLong()	=rec.m_n_nedskrapning;
			m_saCommand.Param(53).setAsLong()	=rec.m_n_avverkningstidpunkt;
			m_saCommand.Param(54).setAsLong()	=rec.m_n_upplevelsehansyn;
			m_saCommand.Param(55).setAsLong()	=rec.m_n_teknEkonimp;
			m_saCommand.Param(56).setAsString()	=rec.m_s_ovrigHansyn;
			m_saCommand.Param(57).setAsString()	=rec.m_s_ovrigaKommentarerNaturOchMiljo;
			m_saCommand.Param(58).setAsLong()	=rec.m_n_baskrTraktplOK;
			m_saCommand.Param(59).setAsString()	=rec.m_s_baskrTraktplBrister;
			m_saCommand.Param(60).setAsString()	=rec.m_s_avvikandeArbUtifranTraktdirektivet;
			m_saCommand.Param(61).setAsString()	=rec.m_s_hansynTillEfterkommandeAtgarder;
			m_saCommand.Param(62).setAsLong()	=rec.m_n_stubbhojd;
			m_saCommand.Param(63).setAsLong()	=rec.m_n_klaratStrategiskaMal;
			m_saCommand.Param(64).setAsString()	=rec.m_s_forbattringsforslag;
			m_saCommand.Param(65).setAsLong()  =rec.m_n_EnterType;


			m_saCommand.Prepare();

			m_saCommand.Execute();

			m_saConnection.Commit();

			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
    	AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CDiCupDB::updTrakt(TRAKT_DATA &rec,TRAKT_IDENTIFER &trakt_ident)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where maskinlagnr = %d and distrikt = %d and region = %d and ")
			_T("traktnr = %d and typ =%d and ursprung = %d"),
			TBL_TRAKT,trakt_ident.nMachineID,trakt_ident.nDistrictID,trakt_ident.nRegionID,trakt_ident.nTraktNum,trakt_ident.nType,trakt_ident.nOrigin);
		if (exists(sSQL))
		{
			sSQL.Format(_T("update %s set ")
     			_T("traktnamn=:1,")
				_T("avvEnlRus=:2,")
				_T("finnsMiljorapport=:3,")
				_T("traktareal=:4,")
				_T("avverkadVolym=:5,")
				_T("maskinlagnamn=:6,")
				_T("datumInv=:7,")
				_T("datumSlutfordAvv=:8,")
				_T("utfortGrotsk=:9,")
				_T("datumSlutfordGrotsk=:10,")
				_T("naturvardestradSum=:11,")
				_T("framtidstradSum=:12,")
				_T("farskaHogstubbarSum=:13,")
				_T("sparadArealSum=:14,")
				_T("sparadVolymSum=:15,")
				_T("kvarglomtVirkeSum=:16,")
				_T("risadeBasstrak=:17,")
				_T("hansynskrBiotoper=:18,")
				_T("mindreAllvKorskada=:19,")
				_T("mindreAllvKorskadaTyp=:20,")
				_T("allvarligKorskada=:21,")
				_T("allvarligKorskadaTyp=:22,")
				_T("forebyggtKorskador=:23,")
				_T("forebyggtKorskadorKommentar=:24,")
				_T("basvagUtanforTrakt=:25,")
				_T("kommentarAvKorskador=:26,")
				_T("skadadeVagdiken=:27,")
				_T("parallellitet=:28,")
				_T("hogarnasBeskaffenhet=:29,")
				_T("placeringAvHogar=:30,")
				_T("innehallHogar=:31,")
				_T("kvalitetPaValtor=:32,")
				_T("kommentarPaGrottillredning=:33,")
				_T("myr=:34,")
				_T("hallmark=:35,")
				_T("omrSomLamnOrorda=:36,")
				_T("objektSomSkallSkotas=:37,")
				_T("kulturminnen=:38,")
				_T("fornminnen=:39,")
				_T("skyddszoner=:40,")
				_T("naturvardestrad=:41,")
				_T("framtidstrad=:42,")
				_T("farskaHogstubbar=:43,")
				_T("dodaTrad=:44,")
				_T("kalytebegransning=:45,")
				_T("nedskrapning=:46,")
				_T("avverkningstidpunkt=:47,")
				_T("upplevelsehansyn=:48,")
				_T("teknEkonimp=:49,")
				_T("ovrigHansyn=:50,")
				_T("ovrigaKommentarerNaturOchMiljo=:51,")
				_T("baskrTraktplOK=:52,")
				_T("baskrTraktplBrister=:53,")
				_T("avvikandeArbUtifranTraktdirektivet=:54,")
				_T("hansynTillEfterkommandeAtgarder=:55,")
				_T("stubbhojd=:56,")
				_T("klaratStrategiskaMal=:57,")
				_T("forbattringsforslag=:58,")
				_T("EnterType=:59, ")
				//Uppdatera �ven nycklarna
				_T("typ=:60,")
				_T("region=:61,")
				_T("distrikt=:62,")
				_T("ursprung=:63,")
				_T("traktnr=:64,")
				_T("maskinlagnr=:65 ")
				_T("where typ=:66 and region=:67 and distrikt=:68 and ")
				_T("ursprung=:69 and traktnr=:70 and maskinlagnr=:71"),TBL_TRAKT);
			m_saCommand.setCommandText((SAString)sSQL);


			m_saCommand.Param(1).setAsString()	=rec.m_s_traktnamn;
			m_saCommand.Param(2).setAsLong()	=rec.m_n_avvEnlRus;
			m_saCommand.Param(3).setAsLong()	=rec.m_n_finnsMiljorapport;
			m_saCommand.Param(4).setAsDouble()	=rec.m_f_traktareal;
			m_saCommand.Param(5).setAsDouble()	=rec.m_f_avverkadVolym;	
			m_saCommand.Param(6).setAsString()	=rec.m_s_maskinlagnamn;
			m_saCommand.Param(7).setAsString()	=rec.m_s_datumInv;
			m_saCommand.Param(8).setAsString()	=rec.m_s_datumSlutfordAvv;
			m_saCommand.Param(9).setAsLong()	=rec.m_n_utfortGrotsk;
			m_saCommand.Param(10).setAsString()	=rec.m_s_datumSlutfordGrotsk;
			m_saCommand.Param(11).setAsLong()	=rec.m_n_naturvardestradSum;
			m_saCommand.Param(12).setAsLong()	=rec.m_n_framtidstradSum;
			m_saCommand.Param(13).setAsLong()	=rec.m_n_farskaHogstubbarSum;
			m_saCommand.Param(14).setAsDouble()	=rec.m_f_sparadArealSum;
			m_saCommand.Param(15).setAsLong()	=rec.m_n_sparadVolymSum;
			m_saCommand.Param(16).setAsLong()	=rec.m_n_kvarglomtVirkeSum;
			m_saCommand.Param(17).setAsLong()	=rec.m_n_risadeBasstrak;
			m_saCommand.Param(18).setAsLong()	=rec.m_n_hansynskrBiotoper;
			m_saCommand.Param(19).setAsLong()	=rec.m_n_mindreAllvKorskada;
			m_saCommand.Param(20).setAsLong()	=rec.m_n_mindreAllvKorskadaTyp;
			m_saCommand.Param(21).setAsLong()	=rec.m_n_allvarligKorskada;
			m_saCommand.Param(22).setAsLong()	=rec.m_n_allvarligKorskadaTyp;
			m_saCommand.Param(23).setAsLong()	=rec.m_n_forebyggtKorskador;
			m_saCommand.Param(24).setAsString()	=rec.m_s_forebyggtKorskadorKommentar;
			m_saCommand.Param(25).setAsLong()	=rec.m_n_basvagUtanforTrakt;
			m_saCommand.Param(26).setAsString()	=rec.m_s_kommentarAvKorskador;
			m_saCommand.Param(27).setAsLong()	=rec.m_n_skadadeVagdiken;
			m_saCommand.Param(28).setAsLong()	=rec.m_n_parallellitet;
			m_saCommand.Param(29).setAsLong()	=rec.m_n_hogarnasBeskaffenhet;
			m_saCommand.Param(30).setAsLong()	=rec.m_n_placeringAvHogar;
			m_saCommand.Param(31).setAsLong()	=rec.m_n_innehallHogar;
			m_saCommand.Param(32).setAsLong()	=rec.m_n_kvalitetPaValtor;
			m_saCommand.Param(33).setAsString()	=rec.m_s_kommentarPaGrottillredning;
			m_saCommand.Param(34).setAsLong()	=rec.m_n_myr;
			m_saCommand.Param(35).setAsLong()	=rec.m_n_hallmark;
			m_saCommand.Param(36).setAsLong()	=rec.m_n_omrSomLamnOrorda;
			m_saCommand.Param(37).setAsLong()	=rec.m_n_objektSomSkallSkotas;
			m_saCommand.Param(38).setAsLong()	=rec.m_n_kulturminnen;
			m_saCommand.Param(39).setAsLong()	=rec.m_n_fornminnen;
			m_saCommand.Param(40).setAsLong()	=rec.m_n_skyddszoner;
			m_saCommand.Param(41).setAsLong()	=rec.m_n_naturvardestrad;
			m_saCommand.Param(42).setAsLong()	=rec.m_n_framtidstrad;
			m_saCommand.Param(43).setAsLong()	=rec.m_n_farskaHogstubbar;
			m_saCommand.Param(44).setAsLong()	=rec.m_n_dodaTrad;
			m_saCommand.Param(45).setAsLong()	=rec.m_n_kalytebegransning;
			m_saCommand.Param(46).setAsLong()	=rec.m_n_nedskrapning;
			m_saCommand.Param(47).setAsLong()	=rec.m_n_avverkningstidpunkt;
			m_saCommand.Param(48).setAsLong()	=rec.m_n_upplevelsehansyn;
			m_saCommand.Param(49).setAsLong()	=rec.m_n_teknEkonimp;
			m_saCommand.Param(50).setAsString()	=rec.m_s_ovrigHansyn;
			m_saCommand.Param(51).setAsString()	=rec.m_s_ovrigaKommentarerNaturOchMiljo;
			m_saCommand.Param(52).setAsLong()	=rec.m_n_baskrTraktplOK;
			m_saCommand.Param(53).setAsString()	=rec.m_s_baskrTraktplBrister;
			m_saCommand.Param(54).setAsString()	=rec.m_s_avvikandeArbUtifranTraktdirektivet;
			m_saCommand.Param(55).setAsString()	=rec.m_s_hansynTillEfterkommandeAtgarder;
			m_saCommand.Param(56).setAsLong()	=rec.m_n_stubbhojd;
			m_saCommand.Param(57).setAsLong()	=rec.m_n_klaratStrategiskaMal;
			m_saCommand.Param(58).setAsString()	=rec.m_s_forbattringsforslag;
			//0 from file 1 maual
			m_saCommand.Param(59).setAsLong()  =rec.m_n_EnterType;
			//Uppdaterade nycklar
			m_saCommand.Param(60).setAsShort()	=rec.m_n_typAvUppfoljning;
			m_saCommand.Param(61).setAsShort()	=rec.m_n_rg;
			m_saCommand.Param(62).setAsShort()	=rec.m_n_di;
			m_saCommand.Param(63).setAsShort()	=rec.m_n_ursprung;
			m_saCommand.Param(64).setAsLong()	=rec.m_n_traktnr;
			m_saCommand.Param(65).setAsShort()	=rec.m_n_maskinlagnr;

			//Gamla traktens nycklar
			m_saCommand.Param(66).setAsShort()	=trakt_ident.nType;
			m_saCommand.Param(67).setAsShort()	=trakt_ident.nRegionID;
			m_saCommand.Param(68).setAsShort()	=trakt_ident.nDistrictID;
			m_saCommand.Param(69).setAsShort()	=trakt_ident.nOrigin;
			m_saCommand.Param(70).setAsLong()	=trakt_ident.nTraktNum;
			m_saCommand.Param(71).setAsShort()	=trakt_ident.nMachineID;

			m_saCommand.Prepare();

			m_saCommand.Execute();

			m_saConnection.Commit();

			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		// print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CDiCupDB::existTrakt(TRAKT_IDENTIFER rec)
{
CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where maskinlagnr = %d and distrikt = %d and region = %d and ")
					_T("traktnr = %d and typ =%d and ursprung = %d"),
					TBL_TRAKT,rec.nMachineID,rec.nDistrictID,rec.nRegionID,rec.nTraktNum,rec.nType,rec.nOrigin);
		if (exists(sSQL))
		{
			bReturn=TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}
	return bReturn;
}

// Delete trakt associated to a specific machine,district and region
BOOL CDiCupDB::delTrakt(TRAKT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where maskinlagnr = %d and distrikt = %d and region = %d and ")
					_T("traktnr = %d and typ =%d and ursprung = %d"),
					TBL_TRAKT,rec.m_n_maskinlagnr,rec.m_n_di,rec.m_n_rg,rec.m_n_traktnr,rec.m_n_typAvUppfoljning,rec.m_n_ursprung);
		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where distrikt=:1 and region=:2 and maskinlagnr=:3 and typ=:4 and ursprung=:5 and traktnr=:6"),TBL_TRAKT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_n_di;
			m_saCommand.Param(2).setAsShort() = rec.m_n_rg;
			m_saCommand.Param(3).setAsShort() = rec.m_n_maskinlagnr;
			m_saCommand.Param(4).setAsShort() = rec.m_n_typAvUppfoljning;
			m_saCommand.Param(5).setAsShort() = rec.m_n_ursprung;
			m_saCommand.Param(6).setAsLong() = rec.m_n_traktnr;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}


	return bReturn;
}



// MACHINE REGISTER

// Handle data in machine_table; 061005 p�d

BOOL CDiCupDB::isThereAnyMachineRegData(void)
{
	int	nCnt = 0;
	BOOL bReturn;
	TCHAR szBuffer[1024];
	try
	{
		// Setup SQL question; 061110 p�d
		_stprintf(szBuffer,_T("select * from %s"),TBL_ACTIVE_MACHINE);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{	
			nCnt++;
			if (nCnt > 0)
				break;
		}		

		m_saConnection.Commit();

		bReturn = (nCnt > 0);

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CDiCupDB::getMachineReg(vecMachineRegData &vec)
{

	TCHAR szBuffer[1024];
	try
	{
		vec.clear();

		// Setup SQL question; 061110 p�d
		_stprintf(szBuffer,_T("select * from %s order by active_machine_num"),TBL_ACTIVE_MACHINE);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(_machine_reg_data(m_saCommand.Field(1).asShort(), m_saCommand.Field(2).asString() ) );
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CDiCupDB::getMachineInRegByNumber(MACHINE_REG_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
			sSQL.Format(_T("select * from %s where active_machine_num = %d"),TBL_ACTIVE_MACHINE,rec.m_nMachineRegID);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				rec = MACHINE_REG_DATA(m_saCommand.Field(1).asShort(), m_saCommand.Field(2).asString());
			}
	
			m_saConnection.Commit();
			
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CDiCupDB::addMachineReg(MACHINE_REG_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where active_machine_num = %d"),TBL_ACTIVE_MACHINE,rec.m_nMachineRegID);
		if (!exists(sSQL))
		{
			sSQL.Format(_T("insert into %s (active_machine_num,active_machine_name) values(:1,:2)"),TBL_ACTIVE_MACHINE);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nMachineRegID;
			m_saCommand.Param(2).setAsString() = rec.m_sContractorName;

			m_saCommand.Execute();
			
			m_saConnection.Commit();
			
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// Update contractor, based on Region,District and Machine; 061010 p�d
BOOL CDiCupDB::updMachineReg(MACHINE_REG_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		sSQL.Format(_T("select * from %s where active_machine_num = %d"),TBL_ACTIVE_MACHINE,rec.m_nMachineRegID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("update %s set active_machine_name = :1 where active_machine_num = :2"),TBL_ACTIVE_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString() = rec.m_sContractorName;
			m_saCommand.Param(2).setAsShort() = rec.m_nMachineRegID;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CDiCupDB::delMachineReg(MACHINE_REG_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where active_machine_num = %d"),TBL_ACTIVE_MACHINE,rec.m_nMachineRegID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where active_machine_num = :1"),TBL_ACTIVE_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nMachineRegID;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

