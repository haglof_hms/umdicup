#include "stdafx.h"
#include "CTraktTabTwo_Korskador.h"

#include "ResLangFileReader.h"
#include "UMDiCupGenerics.h"
//#include ".\dicuptraktpages.h"

// -------------------------------------------------------------------------
//	Trakt Page 2 Körskador
// -------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CTraktPageTwo_Korskador, CXTResizeFormView)
BEGIN_MESSAGE_MAP(CTraktPageTwo_Korskador, CXTResizeFormView)
	ON_CBN_SELCHANGE(IDC_COMBO_TRAKT_PAGE2_MINDREALLVKORSKADA, &CTraktPageTwo_Korskador::OnCbnSelchangeComboTraktPage2Mindreallvkorskada)
	ON_CBN_SELCHANGE(IDC_COMBO_TRAKT_PAGE2_ALLVKORSKADA, &CTraktPageTwo_Korskador::OnCbnSelchangeComboTraktPage2Allvkorskada)
END_MESSAGE_MAP()

CTraktPageTwo_Korskador::CTraktPageTwo_Korskador()
	: CXTResizeFormView(CTraktPageTwo_Korskador::IDD)
{
	m_bInitialized=FALSE;
}
CTraktPageTwo_Korskador::~CTraktPageTwo_Korskador()
{
}

void CTraktPageTwo_Korskador::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)

	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE2_RISADE, m_wndCBox_Trakt_Page2_Risade);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE2_HANSYNKR, m_wndCBox_Trakt_Page2_Hansynskr);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE2_MINDREALLVKORSKADA, m_wndCBox_Trakt_Page2_MindreAllvKorskada);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE2_MINDREALLVKORSKADATYP, m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE2_ALLVKORSKADA, m_wndCBox_Trakt_Page2_AllvKorskada);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE2_ALLVKORSKADATYP, m_wndCBox_Trakt_Page2_AllvKorskadaTyp);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE2_FOREBKORSKADOR, m_wndCBox_Trakt_Page2_ForebKorskador);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE2_BASVAG, m_wndCBox_Trakt_Page2_Basvag);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE2_VAGDIKEN, m_wndCBox_Trakt_Page2_Vagdiken);
					 
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE2_FOREBKORSKADORKOMMENTAR, m_wndEdit_Trakt_Page2_ForebKorskador);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE2_KOMMENTARKORSKADOR,m_wndEdit_Trakt_Page2_KommentarKorskador);
}

void CTraktPageTwo_Korskador::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
		m_wndCBox_Trakt_Page2_Risade.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page2_Risade.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page2_Hansynskr.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page2_Hansynskr.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page2_MindreAllvKorskada.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page2_MindreAllvKorskada.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page2_AllvKorskada.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page2_AllvKorskada.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page2_ForebKorskador.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page2_ForebKorskador.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page2_Basvag.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page2_Basvag.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page2_Vagdiken.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page2_Vagdiken.SetDisabledColor(BLACK,INFOBK);


		m_wndEdit_Trakt_Page2_ForebKorskador.SetEnabledColor(BLACK, WHITE );
		m_wndEdit_Trakt_Page2_ForebKorskador.SetDisabledColor(BLACK, INFOBK );
		m_wndEdit_Trakt_Page2_ForebKorskador.SetReadOnly( TRUE );

		m_wndEdit_Trakt_Page2_KommentarKorskador.SetEnabledColor(BLACK, WHITE );
		m_wndEdit_Trakt_Page2_KommentarKorskador.SetDisabledColor(BLACK, INFOBK );
		m_wndEdit_Trakt_Page2_KommentarKorskador.SetReadOnly( TRUE );

		clearAll();

		setTraktValArrays();
		resetIsDirty();
		m_bInitialized = TRUE;
	}
}

void CTraktPageTwo_Korskador::clearAll(void)
{
	m_wndEdit_Trakt_Page2_ForebKorskador.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page2_KommentarKorskador.SetWindowText(_T(""));

	m_wndCBox_Trakt_Page2_Risade.SetCurSel(-1);
	m_wndCBox_Trakt_Page2_Hansynskr.SetCurSel(-1);
	m_wndCBox_Trakt_Page2_MindreAllvKorskada.SetCurSel(-1);
	m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.SetCurSel(-1);
	m_wndCBox_Trakt_Page2_AllvKorskada.SetCurSel(-1);
	m_wndCBox_Trakt_Page2_AllvKorskadaTyp.SetCurSel(-1);
	m_wndCBox_Trakt_Page2_ForebKorskador.SetCurSel(-1);
	m_wndCBox_Trakt_Page2_Basvag.SetCurSel(-1);
	m_wndCBox_Trakt_Page2_Vagdiken.SetCurSel(-1);
}

void CTraktPageTwo_Korskador::setAllReadOnly(BOOL ro1)
{
	m_wndEdit_Trakt_Page2_ForebKorskador.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page2_KommentarKorskador.SetReadOnly( ro1 );

	m_wndCBox_Trakt_Page2_Risade.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page2_Hansynskr.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page2_MindreAllvKorskada.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page2_AllvKorskada.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page2_AllvKorskadaTyp.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page2_ForebKorskador.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page2_Basvag.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page2_Vagdiken.SetReadOnly( ro1 );
}

void CTraktPageTwo_Korskador::setEnabled(BOOL enabled)
{
	m_wndEdit_Trakt_Page2_ForebKorskador.EnableWindow( enabled );
	m_wndEdit_Trakt_Page2_KommentarKorskador.EnableWindow( enabled );

	m_wndCBox_Trakt_Page2_Risade.EnableWindow( enabled );
	m_wndCBox_Trakt_Page2_Hansynskr.EnableWindow( enabled );
	m_wndCBox_Trakt_Page2_MindreAllvKorskada.EnableWindow( enabled );
	m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.EnableWindow( enabled );
	m_wndCBox_Trakt_Page2_AllvKorskada.EnableWindow( enabled );
	m_wndCBox_Trakt_Page2_AllvKorskadaTyp.EnableWindow( enabled );
	m_wndCBox_Trakt_Page2_ForebKorskador.EnableWindow( enabled );
	m_wndCBox_Trakt_Page2_Basvag.EnableWindow( enabled );
	m_wndCBox_Trakt_Page2_Vagdiken.EnableWindow( enabled );

	setAllReadOnly(!enabled);	
}


void CTraktPageTwo_Korskador::populateData(TRAKT_DATA data)
{
	CString s_Data=_T("");

	s_Data.Format(_T("%d"),data.m_n_risadeBasstrak);
	if(data.m_n_risadeBasstrak==-1)
		m_wndCBox_Trakt_Page2_Risade.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page2_Risade.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_hansynskrBiotoper);
	if(data.m_n_hansynskrBiotoper==-1)
		m_wndCBox_Trakt_Page2_Hansynskr.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page2_Hansynskr.SelectString(0,s_Data);


	s_Data.Format(_T("%d"),data.m_n_mindreAllvKorskada);
	if(data.m_n_mindreAllvKorskada==-1)
		m_wndCBox_Trakt_Page2_MindreAllvKorskada.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page2_MindreAllvKorskada.SelectString(0,s_Data);


	if(data.m_n_mindreAllvKorskadaTyp==0 || data.m_n_mindreAllvKorskadaTyp==-1)
	{
		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.SetCurSel(0);		
	}
	else
	{
		s_Data.Format(_T("%d"),data.m_n_mindreAllvKorskadaTyp);
		if(data.m_n_mindreAllvKorskadaTyp==-1)
			m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.SetCurSel(0);
		else
			m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.SelectString(0,s_Data);
	}

	if(data.m_n_mindreAllvKorskada==1 || data.m_n_mindreAllvKorskada==-1)
		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.EnableWindow(FALSE);
	else
		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.EnableWindow(TRUE);

	s_Data.Format(_T("%d"),data.m_n_allvarligKorskada);
	if(data.m_n_allvarligKorskada==-1)
		m_wndCBox_Trakt_Page2_AllvKorskada.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page2_AllvKorskada.SelectString(0,s_Data);

	if(data.m_n_allvarligKorskadaTyp==0 || data.m_n_allvarligKorskadaTyp==-1)
	{
		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.SetCurSel(0);
	}
	else
	{
		s_Data.Format(_T("%d"),data.m_n_allvarligKorskadaTyp);
		if(data.m_n_allvarligKorskadaTyp==-1)
			m_wndCBox_Trakt_Page2_AllvKorskadaTyp.SetCurSel(0);
		else
			m_wndCBox_Trakt_Page2_AllvKorskadaTyp.SelectString(0,s_Data);
	}

	if(data.m_n_allvarligKorskada==1 || data.m_n_allvarligKorskada==-1)
		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.EnableWindow(FALSE);
	else
		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.EnableWindow(TRUE);

	s_Data.Format(_T("%d"),data.m_n_forebyggtKorskador);
	if(data.m_n_forebyggtKorskador==-1)
		m_wndCBox_Trakt_Page2_ForebKorskador.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page2_ForebKorskador.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_basvagUtanforTrakt);
	if(data.m_n_basvagUtanforTrakt==-1)
		m_wndCBox_Trakt_Page2_Basvag.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page2_Basvag.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_skadadeVagdiken);
	if(data.m_n_skadadeVagdiken==-1)
		m_wndCBox_Trakt_Page2_Vagdiken.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page2_Vagdiken.SelectString(0,s_Data);

	m_wndEdit_Trakt_Page2_ForebKorskador.SetWindowText(data.m_s_forebyggtKorskadorKommentar);
	m_wndEdit_Trakt_Page2_KommentarKorskador.SetWindowText(data.m_s_kommentarAvKorskador);
	setAllReadOnly(FALSE);
	resetIsDirty();
}

void CTraktPageTwo_Korskador::getEnteredData(TRAKT_DATA *trakt)
{	
	trakt->m_s_forebyggtKorskadorKommentar= m_wndEdit_Trakt_Page2_ForebKorskador.getText();
	trakt->m_s_kommentarAvKorskador=m_wndEdit_Trakt_Page2_KommentarKorskador.getText();
	trakt->m_n_risadeBasstrak=m_wndCBox_Trakt_Page2_Risade.GetItemData(m_wndCBox_Trakt_Page2_Risade.GetCurSel());
	trakt->m_n_hansynskrBiotoper =m_wndCBox_Trakt_Page2_Hansynskr.GetItemData(m_wndCBox_Trakt_Page2_Hansynskr.GetCurSel());
	trakt->m_n_mindreAllvKorskada =m_wndCBox_Trakt_Page2_MindreAllvKorskada.GetItemData(m_wndCBox_Trakt_Page2_MindreAllvKorskada.GetCurSel());
	trakt->m_n_mindreAllvKorskadaTyp =m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.GetItemData(m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.GetCurSel());
	trakt->m_n_allvarligKorskada =m_wndCBox_Trakt_Page2_AllvKorskada.GetItemData(m_wndCBox_Trakt_Page2_AllvKorskada.GetCurSel());
	trakt->m_n_allvarligKorskadaTyp =m_wndCBox_Trakt_Page2_AllvKorskadaTyp.GetItemData(m_wndCBox_Trakt_Page2_AllvKorskadaTyp.GetCurSel());
	trakt->m_n_forebyggtKorskador =m_wndCBox_Trakt_Page2_ForebKorskador.GetItemData(m_wndCBox_Trakt_Page2_ForebKorskador.GetCurSel());
	trakt->m_n_basvagUtanforTrakt =m_wndCBox_Trakt_Page2_Basvag.GetItemData(m_wndCBox_Trakt_Page2_Basvag.GetCurSel());
	trakt->m_n_skadadeVagdiken =m_wndCBox_Trakt_Page2_Vagdiken.GetItemData(m_wndCBox_Trakt_Page2_Vagdiken.GetCurSel());	
}

BOOL CTraktPageTwo_Korskador::getIsDirty(void)
{
	if (m_wndEdit_Trakt_Page2_ForebKorskador.isDirty() ||
			m_wndEdit_Trakt_Page2_KommentarKorskador.isDirty() ||
			m_wndCBox_Trakt_Page2_Risade.isDirty() ||
			m_wndCBox_Trakt_Page2_Hansynskr.isDirty() ||
			m_wndCBox_Trakt_Page2_MindreAllvKorskada.isDirty() ||
			m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.isDirty() ||
			m_wndCBox_Trakt_Page2_AllvKorskada.isDirty() || 
			m_wndCBox_Trakt_Page2_AllvKorskadaTyp.isDirty() ||
			m_wndCBox_Trakt_Page2_ForebKorskador.isDirty() ||
			m_wndCBox_Trakt_Page2_Basvag.isDirty() ||
			m_wndCBox_Trakt_Page2_Vagdiken.isDirty())
	{
		return TRUE;
	}
	return FALSE;
}

void CTraktPageTwo_Korskador::resetIsDirty(void)
{
	m_wndEdit_Trakt_Page2_ForebKorskador.resetIsDirty();
	m_wndEdit_Trakt_Page2_KommentarKorskador.resetIsDirty();

	m_wndCBox_Trakt_Page2_Risade.resetIsDirty();
	m_wndCBox_Trakt_Page2_Hansynskr.resetIsDirty();
	m_wndCBox_Trakt_Page2_MindreAllvKorskada.resetIsDirty();
	m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.resetIsDirty();
	m_wndCBox_Trakt_Page2_AllvKorskada.resetIsDirty(); 
	m_wndCBox_Trakt_Page2_AllvKorskadaTyp.resetIsDirty();
	m_wndCBox_Trakt_Page2_ForebKorskador.resetIsDirty();
	m_wndCBox_Trakt_Page2_Basvag.resetIsDirty();
	m_wndCBox_Trakt_Page2_Vagdiken.resetIsDirty();
}

void CTraktPageTwo_Korskador::setTraktValArrays()
{	
	TRAKT_VAL_DATA data;
	CString s_Data=_T("");
	UINT i=0;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			vec_Trakt_Risade.clear();
			vec_Trakt_Risade.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Risade.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7021)),xml->str(IDS_STRING7020)));
			vec_Trakt_Risade.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7023)),xml->str(IDS_STRING7022)));

			vec_Trakt_Hansynskr.clear();
			vec_Trakt_Hansynskr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Hansynskr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7025)),xml->str(IDS_STRING7024)));
			vec_Trakt_Hansynskr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7027)),xml->str(IDS_STRING7026)));
			vec_Trakt_Hansynskr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7029)),xml->str(IDS_STRING7028)));

			vec_Trakt_MindreAllvKorskada.clear();
			vec_Trakt_MindreAllvKorskada.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_MindreAllvKorskada.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7031)),xml->str(IDS_STRING7030)));
			vec_Trakt_MindreAllvKorskada.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7033)),xml->str(IDS_STRING7032)));

			vec_Trakt_MindreAllvKorskadaTyp.clear();
			vec_Trakt_MindreAllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_MindreAllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7035)),xml->str(IDS_STRING7034)));
			vec_Trakt_MindreAllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7037)),xml->str(IDS_STRING7036)));
			
			vec_Trakt_AllvKorskada.clear();
			vec_Trakt_AllvKorskada.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_AllvKorskada.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7039)),xml->str(IDS_STRING7038)));
			vec_Trakt_AllvKorskada.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7041)),xml->str(IDS_STRING7040)));

			vec_Trakt_AllvKorskadaTyp.clear();
			vec_Trakt_AllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_AllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7043)),xml->str(IDS_STRING7042)));
			vec_Trakt_AllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7045)),xml->str(IDS_STRING7044)));	
			vec_Trakt_AllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7047)),xml->str(IDS_STRING7046)));
			vec_Trakt_AllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7049)),xml->str(IDS_STRING7048)));
			vec_Trakt_AllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7051)),xml->str(IDS_STRING7050)));
			vec_Trakt_AllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7053)),xml->str(IDS_STRING7052)));
			vec_Trakt_AllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7055)),xml->str(IDS_STRING7054)));
			vec_Trakt_AllvKorskadaTyp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7057)),xml->str(IDS_STRING7056)));
			
			vec_Trakt_ForebKorskador.clear();
			vec_Trakt_ForebKorskador.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_ForebKorskador.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7059)),xml->str(IDS_STRING7058)));
			vec_Trakt_ForebKorskador.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7061)),xml->str(IDS_STRING7060)));

			vec_Trakt_Basvag.clear();
			vec_Trakt_Basvag.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Basvag.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7063)),xml->str(IDS_STRING7062)));
			vec_Trakt_Basvag.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7065)),xml->str(IDS_STRING7064)));
			vec_Trakt_Basvag.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7067)),xml->str(IDS_STRING7066)));
			
			vec_Trakt_Vagdiken.clear();
			vec_Trakt_Vagdiken.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Vagdiken.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7069)),xml->str(IDS_STRING7068)));
			vec_Trakt_Vagdiken.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7071)),xml->str(IDS_STRING7070)));
		}
	}

	m_wndCBox_Trakt_Page2_Risade.Clear();
	for (i = 0;i < vec_Trakt_Risade.size();i++)
	{
		data = vec_Trakt_Risade[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page2_Risade.AddString(s_Data);
		m_wndCBox_Trakt_Page2_Risade.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Hansynskr.size();i++)
	{
		data = vec_Trakt_Hansynskr[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page2_Hansynskr.AddString(s_Data);
		m_wndCBox_Trakt_Page2_Hansynskr.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_MindreAllvKorskada.size();i++)
	{
		data = vec_Trakt_MindreAllvKorskada[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page2_MindreAllvKorskada.AddString(s_Data);
		m_wndCBox_Trakt_Page2_MindreAllvKorskada.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_MindreAllvKorskadaTyp.size();i++)
	{
		data = vec_Trakt_MindreAllvKorskadaTyp[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.AddString(s_Data);
		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_AllvKorskada.size();i++)
	{
		data = vec_Trakt_AllvKorskada[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page2_AllvKorskada.AddString(s_Data);
		m_wndCBox_Trakt_Page2_AllvKorskada.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_AllvKorskadaTyp.size();i++)
	{
		data = vec_Trakt_AllvKorskadaTyp[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.AddString(s_Data);
		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_ForebKorskador.size();i++)
	{
		data = vec_Trakt_ForebKorskador[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page2_ForebKorskador.AddString(s_Data);
		m_wndCBox_Trakt_Page2_ForebKorskador.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Basvag.size();i++)
	{
		data = vec_Trakt_Basvag[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page2_Basvag.AddString(s_Data);
		m_wndCBox_Trakt_Page2_Basvag.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Vagdiken.size();i++)
	{
		data = vec_Trakt_Vagdiken[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page2_Vagdiken.AddString(s_Data);
		m_wndCBox_Trakt_Page2_Vagdiken.SetItemData(i, data.m_nID);		
	}

}

void CTraktPageTwo_Korskador::OnCbnSelchangeComboTraktPage2Mindreallvkorskada()
{

	if(m_wndCBox_Trakt_Page2_MindreAllvKorskada.GetItemData(m_wndCBox_Trakt_Page2_MindreAllvKorskada.GetCurSel())==1)
	{		
		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.SetCurSel(0);
		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.EnableWindow( FALSE );
	}
	else
		m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp.EnableWindow( TRUE );
}

void CTraktPageTwo_Korskador::OnCbnSelchangeComboTraktPage2Allvkorskada()
{

	if(m_wndCBox_Trakt_Page2_AllvKorskada.GetItemData(m_wndCBox_Trakt_Page2_AllvKorskada.GetCurSel())==1)
	{
		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.SetCurSel(0);
		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.EnableWindow( FALSE );
	}
	else
		m_wndCBox_Trakt_Page2_AllvKorskadaTyp.EnableWindow( TRUE );
}
