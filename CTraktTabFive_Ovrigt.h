#pragma once

#include "UMDiCupDB.h"
#include "Resource.h"

class CTraktPageFive_Ovrigt : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTraktPageFive_Ovrigt)

    BOOL m_bInitialized;
	CString m_sLangFN;

	CMyExtEdit m_wndEdit_Trakt_Page5_KvarVirke;
	CMyExtEdit m_wndEdit_Trakt_Page5_Brister;
	CMyExtEdit m_wndEdit_Trakt_Page5_Avvikande;
	CMyExtEdit m_wndEdit_Trakt_Page5_Hansyn;
	CMyExtEdit m_wndEdit_Trakt_Page5_HogstProc;
	CMyExtEdit m_wndEdit_Trakt_Page5_Forbattr;

	CMyComboBox m_wndCBox_Trakt_Page5_Baskr;
	CMyComboBox m_wndCBox_Trakt_Page5_Strateg;

	vector<TRAKT_VAL_DATA> vec_Trakt_Page5_Baskr;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page5_Strateg;
private:
	void setTraktValArrays();
	void setAllReadOnly(BOOL ro1);
	
public:
	void setEnabled(BOOL enabled);
	void clearAll(void);
	void resetIsDirty(void);
	void populateData(TRAKT_DATA data);
	BOOL getIsDirty(void);
	void getEnteredData(TRAKT_DATA *trakt);
	virtual void OnInitialUpdate();

	enum { IDD = IDD_FORMVIEW_UMDICUP_TRAKT_PAGE5 };
	CTraktPageFive_Ovrigt();           // protected constructor used by dynamic creation
	virtual ~CTraktPageFive_Ovrigt();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()	
};