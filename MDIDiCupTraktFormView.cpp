// MDIDiCupTraktFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIDiCupTraktFormView.h"

#include "ResLangFileReader.h"

#include "SelectRegionDistDlg.h"

#include "UMDiCupGenerics.h"
#include ".\mdidicuptraktformview.h"

#include "CTraktTabTwo_Korskador.h"
#include "CTraktTabThree_Grottillredning.h"
#include "CTraktTabOne_Addering.h"
#include "CTraktTabFour_NaturMiljo.h"
#include "CTraktTabFive_Ovrigt.h"
#include "CTraktPageTest.h"


// CMDIDiCupTraktFormView

IMPLEMENT_DYNCREATE(CMDIDiCupTraktFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIDiCupTraktFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_BUTTON_TRAKT_MASKIN, OnBnClicked_Trakt_Maskin)
	ON_CBN_SELCHANGE(IDC_COMBO_TRAKT_REGION, OnCbnSelchangeTraktRegion)
	ON_WM_COPYDATA()	
END_MESSAGE_MAP()

CMDIDiCupTraktFormView::CMDIDiCupTraktFormView()
	: CXTResizeFormView(CMDIDiCupTraktFormView::IDD)
	, m_Text(_T(""))
{
}

CMDIDiCupTraktFormView::~CMDIDiCupTraktFormView()
{
}

BOOL CMDIDiCupTraktFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIDiCupTraktFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL_TRAKT_REGION, m_wndLb_Trakt_Region);
	DDX_Control(pDX, IDC_LBL_TRAKT_DISTRIKT, m_wndLb_Trakt_Distrikt);
	DDX_Control(pDX, IDC_LBL_TRAKT_MASKINNR, m_wndLb_Trakt_Maskinnr);
	DDX_Control(pDX, IDC_LBL_TRAKT_ENTR, m_wndLb_Trakt_Entr);
	DDX_Control(pDX, IDC_LBL_TRAKT_INVTYP, m_wndLb_Trakt_InvTyp);
	DDX_Control(pDX, IDC_LBL_TRAKT_URSPRUNG, m_wndLb_Trakt_Ursprung);
	DDX_Control(pDX, IDC_LBL_TRAKT_NR, m_wndLb_Trakt_Nr);
	DDX_Control(pDX, IDC_LBL_TRAKT_NAMN, m_wndLb_Trakt_Namn);
	DDX_Control(pDX, IDC_LBL_TRAKT_AREAL, m_wndLb_Trakt_Areal);
	DDX_Control(pDX, IDC_LBL_TRAKT_DATUM_INV, m_wndLb_Trakt_Datum_Inv);
	DDX_Control(pDX, IDC_LBL_TRAKT_DATUM_SLUTFORD_AVV, m_wndLb_Trakt_Datum_Slutford_Avv);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVV_ENL_RUS, m_wndLb_Trakt_Avv_Enl_Rus);
	DDX_Control(pDX, IDC_LBL_TRAKT_FINNS_MILJORAPP, m_wndLb_Trakt_Finns_Miljorapp);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVV_VOL, m_wndLb_Trakt_Avv_Vol);
	DDX_Control(pDX, IDC_LBL_TRAKT_UTFORD_GROT, m_wndLb_Trakt_Utford_Grot);
	DDX_Control(pDX, IDC_LBL_TRAKT_DATUM_SLUTFORD_GROT, m_wndLb_Trakt_Datum_Slutford_Grot);
	DDX_Control(pDX, IDC_LBL_TRAKT_DATUM_SLUTFORD_AVV, m_wndLb_Trakt_Datum_Slutford_Avv);

	DDX_Control(pDX, IDC_EDIT_TRAKT_MASKIN, m_wndEdit_Trakt_Maskin);
	DDX_Control(pDX, IDC_EDIT_TRAKT_ENTR, m_wndEdit_Trakt_Entr);
	DDX_Control(pDX, IDC_EDIT_TRAKT_TRAKTNR, m_wndEdit_Trakt_Nr);
	DDX_Control(pDX, IDC_EDIT_TRAKT_TRAKTNAMN, m_wndEdit_Trakt_Namn);
	DDX_Control(pDX, IDC_EDIT_TRAKT_AREAL, m_wndEdit_Trakt_Areal);
	DDX_Control(pDX, IDC_EDIT_TRAKT_AVV_VOL, m_wndEdit_Trakt_Avv_Vol);
	
	DDX_Control(pDX, IDC_COMBO_TRAKT_INVTYP, m_wndCBox_Trakt_InvTyp);
	DDX_Control(pDX, IDC_COMBO_TRAKT_URSPRUNG, m_wndCBox_Trakt_Ursprung);
	DDX_Control(pDX, IDC_COMBO_TRAKT_REGION, m_wndCBox_Trakt_Region);
	DDX_Control(pDX, IDC_COMBO_TRAKT_DISTRIKT, m_wndCBox_Trakt_District);
	DDX_Control(pDX, IDC_COMBO_TRAKT_AVV_ENL_RUS, m_wndCBox_Trakt_Avv_Enl_Rus);
	DDX_Control(pDX, IDC_COMBO_TRAKT_FINNS_MILJORAPP, m_wndCBox_Trakt_Finns_Miljorapp);
	DDX_Control(pDX, IDC_COMBO_TRAKT_UTFORT_GROT, m_wndCBox_Trakt_Utford_Grot);




	DDX_Control(pDX, IDC_DATETIMEPICKER_INV, m_wndDatePicker_Trakt_Datum_Inv);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_INV, strDatePicker_Trakt_Datum_Inv);

	DDX_Control(pDX, IDC_DATETIMEPICKER_SLUTF_AVV, m_wndDatePicker_Trakt_Datum_Slutford_Avv);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_SLUTF_AVV, strDatePicker_Trakt_Datum_Slutford_Avv);
	
	DDX_Control(pDX, IDC_DATETIMEPICKER_SLUTF_GROTSK, m_wndDatePicker_Trakt_Datum_Slutford_Grot);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER_SLUTF_GROTSK, strDatePicker_Trakt_Datum_Slutford_Grot);



	DDX_Control(pDX, IDC_GRP1, m_wndIDGroup);
	DDX_Control(pDX, IDC_GRP_TRAKT, m_wndTraktGroup);

	DDX_Control(pDX, IDC_BUTTON_TRAKT_MASKIN, m_wndListMachineBtn);
}

// CMDIDiCupTraktFormView diagnostics

#ifdef _DEBUG
void CMDIDiCupTraktFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIDiCupTraktFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

void CMDIDiCupTraktFormView::OnSize(UINT nType,int cx,int cy)
{
	CView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
		setResize(&m_wndTabControl, 1, rect.top+190, rect.right - 2, rect.bottom-(rect.top+195));
}

void CMDIDiCupTraktFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	m_wndEdit_Trakt_Maskin.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Trakt_Maskin.SetDisabledColor(BLACK, INFOBK );
	

	m_wndEdit_Trakt_Entr.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Trakt_Entr.SetDisabledColor(BLACK, INFOBK );
	

	m_wndEdit_Trakt_Nr.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Trakt_Nr.SetDisabledColor(BLACK, INFOBK );
	

	m_wndEdit_Trakt_Namn.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Trakt_Namn.SetDisabledColor(BLACK, INFOBK );
	

	m_wndEdit_Trakt_Areal.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Trakt_Areal.SetDisabledColor(BLACK, INFOBK );
	

	m_wndEdit_Trakt_Avv_Vol.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Trakt_Avv_Vol.SetDisabledColor(BLACK, INFOBK );
	


	m_wndCBox_Trakt_Region.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Region.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Region.SetReadOnlyEx();

	m_wndCBox_Trakt_District.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_District.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_District.SetReadOnlyEx();

	m_wndCBox_Trakt_InvTyp.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_InvTyp.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_InvTyp.SetReadOnlyEx();
	
	m_wndCBox_Trakt_Ursprung.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Ursprung.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Ursprung.SetReadOnlyEx();

	m_wndCBox_Trakt_Avv_Enl_Rus.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avv_Enl_Rus.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avv_Enl_Rus.SetReadOnlyEx();
	
	m_wndCBox_Trakt_Finns_Miljorapp.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Finns_Miljorapp.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Finns_Miljorapp.SetReadOnlyEx();

	m_wndCBox_Trakt_Utford_Grot.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Utford_Grot.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Utford_Grot.SetReadOnlyEx();

	/*COleDateTime dt; 
	dt.SetStatus(COleDateTime::null); 
	m_wndDatePicker_Trakt_Datum_Inv.SetTime(dt); 
	m_wndDatePicker_Trakt_Datum_Slutford_Avv.SetTime(dt); 
	m_wndDatePicker_Trakt_Datum_Slutford_Grot.SetTime(dt); */


	setEnabled(FALSE);

	m_wndListMachineBtn.SetBitmap(CSize(18,14),IDB_BITMAP1);
	m_wndListMachineBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Get regions; 061204 p�d
	getRegionsFromDB();
	addRegionsToCBox();
	// Get districts (and region info); 061011 p�d
	getDistrictFromDB();
	// Get machine(s); 061011 p�d
	getMachineFromDB();
	// Get data from database; 061010 p�d
	getTraktsFromDB();
	//
	setupTraktTabs();
	


	setTraktValArrays();

	// Setup what the user's done when adding data; 061201 p�d
	// MANUALLY = Adding data by hand
	// FROM_FILE = Adding from a datafile
	m_enumAction = NOTHING;
		
	setLanguage();

	// Set Default values for Type and Origin; 061010 p�d
	setDefaultTypeAndOrigin();

	m_nDBIndex = 0;
	populateData(m_nDBIndex);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
		setResize(&m_wndTabControl, 1, rect.top+190, rect.right - 2, rect.bottom-(rect.top+195));

}

BOOL CMDIDiCupTraktFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIDiCupTraktFormView::OnSetFocus(CWnd*)
{
	if (!m_vecTraktData.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktData.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
}


void CMDIDiCupTraktFormView::getEnteredDataTabPages(TRAKT_DATA *trakt)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();

	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CTraktPageOne_Addering *wnd_Page1;
				wnd_Page1 = DYNAMIC_DOWNCAST(CTraktPageOne_Addering,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page1->getEnteredData(trakt);
				break;
			case 1:
				CTraktPageTwo_Korskador *wnd_Page2;
				wnd_Page2 = DYNAMIC_DOWNCAST(CTraktPageTwo_Korskador,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page2->getEnteredData(trakt);
				break;
			case 2:
				CTraktPageThree_Grottillredning *wnd_Page3;
				wnd_Page3 = DYNAMIC_DOWNCAST(CTraktPageThree_Grottillredning,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page3->getEnteredData(trakt);
				break;
			case 3:
				CTraktPageTest *wnd_Page0;
				wnd_Page0 = DYNAMIC_DOWNCAST(CTraktPageTest,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page0->getEnteredData(trakt);
				break;
			case 4:
				CTraktPageFive_Ovrigt *wnd_Page5;
				wnd_Page5 = DYNAMIC_DOWNCAST(CTraktPageFive_Ovrigt,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page5->getEnteredData(trakt);
				break;
			}
		}
	}	
}

BOOL CMDIDiCupTraktFormView::getTabPagesIsDirty(void)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();
	int nRet=0;

	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{			
			case 0:
				CTraktPageOne_Addering *wnd_Page1;
				wnd_Page1 = DYNAMIC_DOWNCAST(CTraktPageOne_Addering,CWnd::FromHandle(pItem->GetHandle()));
				if(wnd_Page1->getIsDirty())
					nRet=1;
				break;
			case 1:
				CTraktPageTwo_Korskador *wnd_Page2;
				wnd_Page2 = DYNAMIC_DOWNCAST(CTraktPageTwo_Korskador,CWnd::FromHandle(pItem->GetHandle()));
				if(wnd_Page2->getIsDirty())
					nRet=1;
				break;
			case 2:
				CTraktPageThree_Grottillredning *wnd_Page3;
				wnd_Page3 = DYNAMIC_DOWNCAST(CTraktPageThree_Grottillredning,CWnd::FromHandle(pItem->GetHandle()));
				if(wnd_Page3->getIsDirty())
					nRet=1;
				break;
			case 3:
				CTraktPageTest *wnd_Page0;
				wnd_Page0 = DYNAMIC_DOWNCAST(CTraktPageTest,CWnd::FromHandle(pItem->GetHandle()));
				if(wnd_Page0->getIsDirty())
					nRet=1;
				break;
			case 4:
				CTraktPageFive_Ovrigt *wnd_Page5;
				wnd_Page5 = DYNAMIC_DOWNCAST(CTraktPageFive_Ovrigt,CWnd::FromHandle(pItem->GetHandle()));
				if(wnd_Page5->getIsDirty())
					nRet=1;
				break;
			}
		}
	}	
	return nRet;
}

void CMDIDiCupTraktFormView::resetTabPagesIsDirty(void)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();

	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CTraktPageOne_Addering *wnd_Page1;
				wnd_Page1 = DYNAMIC_DOWNCAST(CTraktPageOne_Addering,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page1->resetIsDirty();
				break;
			case 1:
				CTraktPageTwo_Korskador *wnd_Page2;
				wnd_Page2 = DYNAMIC_DOWNCAST(CTraktPageTwo_Korskador,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page2->resetIsDirty();
				break;
			case 2:
				CTraktPageThree_Grottillredning *wnd_Page3;
				wnd_Page3 = DYNAMIC_DOWNCAST(CTraktPageThree_Grottillredning,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page3->resetIsDirty();
				break;
			case 3:
				CTraktPageTest *wnd_Page0;
				wnd_Page0 = DYNAMIC_DOWNCAST(CTraktPageTest,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page0->resetIsDirty();
				break;
			case 4:
				CTraktPageFive_Ovrigt *wnd_Page5;
				wnd_Page5 = DYNAMIC_DOWNCAST(CTraktPageFive_Ovrigt,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page5->resetIsDirty();
				break;
			}
		}
	}	
}

BOOL CMDIDiCupTraktFormView::getDatesIsDirty()
{
	CString sDate=_T("");
	CString sYearFrom = _T(""),sMonthFrom = _T(""),sDayFrom=_T("");
	COleDateTime ctTemp;
	m_wndDatePicker_Trakt_Datum_Inv.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayFrom.Format(_T("%02d"), ctTemp.GetDay());
		sMonthFrom.Format(_T("%02d"), ctTemp.GetMonth());
		sYearFrom.Format(_T("%04d"), ctTemp.GetYear());
		sDate.Format(_T("%s-%s-%s"),sYearFrom,sMonthFrom,sDayFrom);

		if(m_recActiveTrakt.m_s_datumInv != sDate)
			return TRUE;
	}


	m_wndDatePicker_Trakt_Datum_Slutford_Avv.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayFrom.Format(_T("%02d"), ctTemp.GetDay());
		sMonthFrom.Format(_T("%02d"), ctTemp.GetMonth());
		sYearFrom.Format(_T("%04d"), ctTemp.GetYear());
		sDate.Format(_T("%s-%s-%s"),sYearFrom,sMonthFrom,sDayFrom);
		if(m_recActiveTrakt.m_s_datumSlutfordAvv != sDate)
			return TRUE;
	}

	//m_wndDatePicker_Trakt_Datum_Slutford_Avv.GetWindowText(sDate);
	//m_recNewTrakt.m_s_datumSlutfordAvv		= sDate;

	m_wndDatePicker_Trakt_Datum_Slutford_Grot.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayFrom.Format(_T("%02d"), ctTemp.GetDay());
		sMonthFrom.Format(_T("%02d"), ctTemp.GetMonth());
		sYearFrom.Format(_T("%04d"), ctTemp.GetYear());
		sDate.Format(_T("%s-%s-%s"),sYearFrom,sMonthFrom,sDayFrom);

		if(m_recActiveTrakt.m_s_datumSlutfordGrotsk != sDate)
			return TRUE;

	}
	return FALSE;
}

BOOL CMDIDiCupTraktFormView::getIsDirty(void)
{
	if (m_wndEdit_Trakt_Maskin.isDirty() ||
	m_wndEdit_Trakt_Entr.isDirty() ||
	m_wndEdit_Trakt_Nr.isDirty() ||
	m_wndEdit_Trakt_Namn.isDirty() ||
	m_wndEdit_Trakt_Areal.isDirty() ||
	m_wndEdit_Trakt_Avv_Vol.isDirty() ||

	m_wndCBox_Trakt_Region.isDirty() ||
	m_wndCBox_Trakt_District.isDirty() ||
	m_wndCBox_Trakt_InvTyp.isDirty() ||
	m_wndCBox_Trakt_Ursprung.isDirty() ||
	m_wndCBox_Trakt_Avv_Enl_Rus.isDirty() ||
	m_wndCBox_Trakt_Finns_Miljorapp.isDirty() ||
	m_wndCBox_Trakt_Utford_Grot.isDirty() || getDatesIsDirty() ||
	getTabPagesIsDirty()) 
	{
		return TRUE;
	}

	return FALSE;
}

void CMDIDiCupTraktFormView::resetIsDirty(void)
{

	m_wndEdit_Trakt_Maskin.resetIsDirty();
	m_wndEdit_Trakt_Entr.resetIsDirty();
	m_wndEdit_Trakt_Nr.resetIsDirty();
	m_wndEdit_Trakt_Namn.resetIsDirty();
	m_wndEdit_Trakt_Areal.resetIsDirty();
	m_wndEdit_Trakt_Avv_Vol.resetIsDirty();

	m_wndCBox_Trakt_Region.resetIsDirty();
	m_wndCBox_Trakt_District.resetIsDirty();
	m_wndCBox_Trakt_InvTyp.resetIsDirty();
	m_wndCBox_Trakt_Ursprung.resetIsDirty();
	m_wndCBox_Trakt_Avv_Enl_Rus.resetIsDirty();
	m_wndCBox_Trakt_Finns_Miljorapp.resetIsDirty();
	m_wndCBox_Trakt_Utford_Grot.resetIsDirty();
	resetTabPagesIsDirty();
}


void CMDIDiCupTraktFormView::setTraktValArrays()
{	
	TRAKT_VAL_DATA data;
	CString s_Data=_T("");
	UINT i=0;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			vec_Trakt_Avv_Enl_Rus.clear();
			vec_Trakt_Avv_Enl_Rus.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Avv_Enl_Rus.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6201)),xml->str(IDS_STRING6200)));
			vec_Trakt_Avv_Enl_Rus.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6203)),xml->str(IDS_STRING6202)));

			vec_Trakt_Finns_Miljorapp.clear();
			vec_Trakt_Finns_Miljorapp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Finns_Miljorapp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6211)),xml->str(IDS_STRING6210)));
			vec_Trakt_Finns_Miljorapp.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6213)),xml->str(IDS_STRING6212)));
			
			vec_Trakt_Utford_Grot.clear();
			vec_Trakt_Utford_Grot.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Utford_Grot.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6221)),xml->str(IDS_STRING6220)));
			vec_Trakt_Utford_Grot.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6223)),xml->str(IDS_STRING6222)));

		}
	}

	for (i = 0;i < vec_Trakt_Avv_Enl_Rus.size();i++)
	{
		data = vec_Trakt_Avv_Enl_Rus[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avv_Enl_Rus.AddString(s_Data);
		m_wndCBox_Trakt_Avv_Enl_Rus.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Finns_Miljorapp.size();i++)
	{
		data = vec_Trakt_Finns_Miljorapp[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Finns_Miljorapp.AddString(s_Data);
		m_wndCBox_Trakt_Finns_Miljorapp.SetItemData(i, data.m_nID);
	}



	for (i = 0;i < vec_Trakt_Utford_Grot.size();i++)
	{
		data = vec_Trakt_Utford_Grot[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Utford_Grot.AddString(s_Data);
		m_wndCBox_Trakt_Utford_Grot.SetItemData(i, data.m_nID);
	}

}

// CMDIDiCupTraktFormView message handlers

void CMDIDiCupTraktFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Trakt information
			m_wndTraktGroup.SetWindowText(xml->str(IDS_STRING247));
			m_wndLb_Trakt_Region.SetWindowText(xml->str(IDS_STRING2040));
			m_wndLb_Trakt_Distrikt.SetWindowText(xml->str(IDS_STRING2041));
			m_wndLb_Trakt_Maskinnr.SetWindowText(xml->str(IDS_STRING205));
			m_wndLb_Trakt_Entr.SetWindowText(xml->str(IDS_STRING108));
			m_wndLb_Trakt_InvTyp.SetWindowText(xml->str(IDS_STRING210));
			m_wndLb_Trakt_Ursprung.SetWindowText(xml->str(IDS_STRING211));
			m_wndLb_Trakt_Nr.SetWindowText(xml->str(IDS_STRING248));
			m_wndLb_Trakt_Namn.SetWindowText(xml->str(IDS_STRING249));
			m_wndLb_Trakt_Areal.SetWindowText(xml->str(IDS_STRING254));
			m_wndLb_Trakt_Datum_Inv.SetWindowText(xml->str(IDS_STRING255));
			m_wndLb_Trakt_Datum_Slutford_Avv.SetWindowText(xml->str(IDS_STRING256));
			m_wndLb_Trakt_Avv_Enl_Rus.SetWindowText(xml->str(IDS_STRING257));
			m_wndLb_Trakt_Finns_Miljorapp.SetWindowText(xml->str(IDS_STRING258));
			m_wndLb_Trakt_Avv_Vol.SetWindowText(xml->str(IDS_STRING259));
			m_wndLb_Trakt_Utford_Grot.SetWindowText(xml->str(IDS_STRING260));
			m_wndLb_Trakt_Datum_Slutford_Grot.SetWindowText(xml->str(IDS_STRING261));
			m_wndLb_Trakt_Datum_Slutford_Avv.SetWindowText(xml->str(IDS_STRING262));

			m_sErrCap = xml->str(IDS_STRING109);
			m_sErrMsg = xml->str(IDS_STRING510);
			m_sNotOkToSave.Format(_T("%s\n\n%s\n\n%s"),
				xml->str(IDS_STRING5161),
				xml->str(IDS_STRING5162),
				xml->str(IDS_STRING5163));
			m_sSaveData.Format(_T("%s\n\n%s"),
				xml->str(IDS_STRING5171),
				xml->str(IDS_STRING5172));
			m_sUpdateTraktMsg.Format(_T("%s\n\n%s\n%s\n\n%s"),
				xml->str(IDS_STRING5220),
				xml->str(IDS_STRING5221),
				xml->str(IDS_STRING5222),
				xml->str(IDS_STRING5223));

			m_sTraktExists.Format(_T("%s"),xml->str(IDS_STRING5224));

			m_sCantSaveTraktMsg.Format(_T("%s\n"),xml->str(IDS_STRING5161));

		}
		delete xml;
	}

}

void CMDIDiCupTraktFormView::setupTraktTabs(void)
{
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP,CRect(0,0,0,0),this, IDC_TABCONTROL_TRAKT);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Create tabpages
			AddView(RUNTIME_CLASS(CTraktPageOne_Addering),xml->str(IDS_STRING7007),-1);				// Adderingssnurror
			AddView(RUNTIME_CLASS(CTraktPageTwo_Korskador),xml->str(IDS_STRING7008),-1);			// K�rskador
			AddView(RUNTIME_CLASS(CTraktPageThree_Grottillredning),xml->str(IDS_STRING7009),-1);	// Grottillrednint
			AddView(RUNTIME_CLASS(CTraktPageTest),xml->str(IDS_STRING7010),-1);			// Natur och milj�
			AddView(RUNTIME_CLASS(CTraktPageFive_Ovrigt),xml->str(IDS_STRING7011),-1);				// �vrigt		
		}
		delete xml;
	}

}

void CMDIDiCupTraktFormView::populateData(UINT idx)
{
	DISTRICT_DATA data;
	CString s_Data=_T("");
	if (!m_vecTraktData.empty() && 
		  idx >= 0 && 
			idx < m_vecTraktData.size())
	{
		m_recActiveTrakt = m_vecTraktData[idx];

		//Region
		//m_wndCBox_Trakt_Region.SetWindowText(getRegionSelected(m_recActiveTrakt.m_n_rg));
		//m_wndCBox_Trakt_Region.SetItemData(0,m_recActiveTrakt.m_n_rg);
		s_Data.Format(_T("%d"),m_recActiveTrakt.m_n_rg);
		m_wndCBox_Trakt_Region.SelectString(0,s_Data);
		//Distrikt
		//m_wndCBox_Trakt_District.SetWindowText(getDistrictSelected(m_recActiveTrakt.m_n_rg,m_recActiveTrakt.m_n_di));
		//m_wndCBox_Trakt_District.SetItemData(0,m_recActiveTrakt.m_n_di);
		addDistrictsToCBox();
		s_Data.Format(_T("%d"),m_recActiveTrakt.m_n_di);
		m_wndCBox_Trakt_District.SelectString(0,s_Data);

		//Maskin
		m_wndEdit_Trakt_Maskin.setInt(m_recActiveTrakt.m_n_maskinlagnr);
		//Entrepren�r
		m_wndEdit_Trakt_Entr.SetWindowText(getMachineEntr(m_recActiveTrakt.m_n_rg,m_recActiveTrakt.m_n_di,m_recActiveTrakt.m_n_maskinlagnr));
		//InvTyp
		m_wndCBox_Trakt_InvTyp.SetCurSel(m_recActiveTrakt.m_n_typAvUppfoljning);
		//Ursprung
		m_wndCBox_Trakt_Ursprung.SetCurSel(m_recActiveTrakt.m_n_ursprung);
		//Traktnr
		m_wndEdit_Trakt_Nr.setInt(m_recActiveTrakt.m_n_traktnr);
		//Traktnamn
		m_wndEdit_Trakt_Namn.SetWindowText(m_recActiveTrakt.m_s_traktnamn);
		//Areal
		m_wndEdit_Trakt_Areal.setFloat(m_recActiveTrakt.m_f_traktareal,1);
		
		//Avv enl RUS
		s_Data.Format(_T("%d"),m_recActiveTrakt.m_n_avvEnlRus);
		m_wndCBox_Trakt_Avv_Enl_Rus.SelectString(0,s_Data);
		//Finns milj�rapport
		s_Data.Format(_T("%d"),m_recActiveTrakt.m_n_finnsMiljorapport);
		m_wndCBox_Trakt_Finns_Miljorapp.SelectString(0,s_Data);
		//Avv Volym
		m_wndEdit_Trakt_Avv_Vol.setFloat(m_recActiveTrakt.m_f_avverkadVolym,1);
		//Utf�rd grotskotning
		s_Data.Format(_T("%d"),m_recActiveTrakt.m_n_utfortGrotsk);
		m_wndCBox_Trakt_Utford_Grot.SelectString(0,s_Data);
		
		//Datum Inv
		strDatePicker_Trakt_Datum_Inv=m_recActiveTrakt.m_s_datumInv;
		//m_wndDatePicker_Trakt_Datum_Inv.SetSetWindowText(m_recActiveTrakt.m_s_datumInv);
		
		//Datum Slutf�rd grotskotning
		//m_wndDatePicker_Trakt_Datum_Slutford_Grot.SetWindowText(m_recActiveTrakt.m_s_datumSlutfordGrotsk);		
		strDatePicker_Trakt_Datum_Slutford_Grot=m_recActiveTrakt.m_s_datumSlutfordGrotsk;
		
		//Datum Slutf�rd Avv
		//m_wndDatePicker_Trakt_Datum_Slutford_Avv.SetWindowText(m_recActiveTrakt.m_s_datumSlutfordAvv);	
		strDatePicker_Trakt_Datum_Slutford_Avv=m_recActiveTrakt.m_s_datumSlutfordAvv;


     	// This method set editboxes read or read only, depending on 
		// if there's Compartments and Plots; 061010 p�d
		setEnabled(TRUE);
		showTabData(m_recActiveTrakt);
		resetIsDirty();
	}
	else
	{
		// This method set editboxes read or read only, depending on 
		// if there's Compartments and Plots; 061010 p�d
		setEnabled(FALSE);
		clearAll();
	}
	UpdateData(FALSE);

}

void CMDIDiCupTraktFormView::setEnabledTabData(BOOL enabled)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();

	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CTraktPageOne_Addering *wnd_Page1;
				wnd_Page1 = DYNAMIC_DOWNCAST(CTraktPageOne_Addering,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page1->setEnabled(enabled);
				break;
			case 1:
				CTraktPageTwo_Korskador *wnd_Page2;
				wnd_Page2 = DYNAMIC_DOWNCAST(CTraktPageTwo_Korskador,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page2->setEnabled(enabled);
				break;		
			case 2:
				CTraktPageThree_Grottillredning *wnd_Page3;
				wnd_Page3 = DYNAMIC_DOWNCAST(CTraktPageThree_Grottillredning,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page3->setEnabled(enabled);
				break;
			case 3:
				CTraktPageTest *wnd_Page0;
				wnd_Page0 = DYNAMIC_DOWNCAST(CTraktPageTest,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page0->setEnabled(enabled);
				break;
			case 4:
				CTraktPageFive_Ovrigt *wnd_Page5;
				wnd_Page5 = DYNAMIC_DOWNCAST(CTraktPageFive_Ovrigt,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page5->setEnabled(enabled);
				break;
			}
		}
	}

}

void CMDIDiCupTraktFormView::clearAllTabData(void)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();

	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CTraktPageOne_Addering *wnd_Page1;
				wnd_Page1 = DYNAMIC_DOWNCAST(CTraktPageOne_Addering,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page1->clearAll();
				break;
			case 1:
				CTraktPageTwo_Korskador *wnd_Page2;
				wnd_Page2 = DYNAMIC_DOWNCAST(CTraktPageTwo_Korskador,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page2->clearAll();
				break;		
			case 2:
				CTraktPageThree_Grottillredning *wnd_Page3;
				wnd_Page3 = DYNAMIC_DOWNCAST(CTraktPageThree_Grottillredning,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page3->clearAll();
				break;
			case 3:
				CTraktPageTest *wnd_Page0;
				wnd_Page0 = DYNAMIC_DOWNCAST(CTraktPageTest,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page0->clearAll();
				break;
			case 4:
				CTraktPageFive_Ovrigt *wnd_Page5;
				wnd_Page5 = DYNAMIC_DOWNCAST(CTraktPageFive_Ovrigt,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page5->clearAll();
				break;
			}
		}
	}
}



void CMDIDiCupTraktFormView::showTabData(TRAKT_DATA data)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();

	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CTraktPageOne_Addering *wnd_Page1;
				wnd_Page1 = DYNAMIC_DOWNCAST(CTraktPageOne_Addering,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page1->populateData(data);
				break;
			case 1:
				CTraktPageTwo_Korskador *wnd_Page2;
				wnd_Page2 = DYNAMIC_DOWNCAST(CTraktPageTwo_Korskador,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page2->populateData(data);
				break;		
			case 2:
				CTraktPageThree_Grottillredning *wnd_Page3;
				wnd_Page3 = DYNAMIC_DOWNCAST(CTraktPageThree_Grottillredning,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page3->populateData(data);
				break;
			case 3:
				CTraktPageTest *wnd_Page0;
				wnd_Page0 = DYNAMIC_DOWNCAST(CTraktPageTest,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page0->populateData(data);
				break;
			case 4:
				CTraktPageFive_Ovrigt *wnd_Page5;
				wnd_Page5 = DYNAMIC_DOWNCAST(CTraktPageFive_Ovrigt,CWnd::FromHandle(pItem->GetHandle()));
				wnd_Page5->populateData(data);
				break;
			}
		}
	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 061010 p�d
LRESULT CMDIDiCupTraktFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
	case ID_NEW_ITEM :
		{
			m_enumAction = MANUALLY;
			addTraktManually();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
	case ID_OPEN_ITEM :
		{
			m_enumAction = FROM_FILE;
			addTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
	case ID_SAVE_ITEM :
		{
			saveTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
	case ID_DELETE_ITEM :
		{
			deleteTrakt();
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
	case ID_UPDATE_ITEM :
		{
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
	case ID_DBNAVIG_START :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			isDataChanged();
			m_nDBIndex = 0;
			setNavigationButtons(FALSE,TRUE);
			populateData(m_nDBIndex);
			break;
		}
	case ID_DBNAVIG_PREV :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			isDataChanged();

			if(m_nDBIndex>=1)
				m_nDBIndex--;
			if (m_nDBIndex == 0)
			{
				setNavigationButtons(FALSE,TRUE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
	case ID_DBNAVIG_NEXT :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			isDataChanged();
			m_nDBIndex++;
			if (m_nDBIndex > ((UINT)m_vecTraktData.size() - 1))
				m_nDBIndex = (UINT)m_vecTraktData.size() - 1;

			if (m_nDBIndex == (UINT)m_vecTraktData.size() - 1)
			{
				setNavigationButtons(TRUE,FALSE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
	case ID_DBNAVIG_END :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			isDataChanged();
			m_nDBIndex = (UINT)m_vecTraktData.size()-1;
			setNavigationButtons(TRUE,FALSE);	
			populateData(m_nDBIndex);
			break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

void CMDIDiCupTraktFormView::getTraktsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CDiCupDB *pDB = new CDiCupDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakts(m_vecTraktData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIDiCupTraktFormView::getRegionsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CDiCupDB *pDB = new CDiCupDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getRegions(m_vecRegionData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIDiCupTraktFormView::getDistrictFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CDiCupDB *pDB = new CDiCupDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistricts(m_vecDistrictData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIDiCupTraktFormView::getMachineFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CDiCupDB *pDB = new CDiCupDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getMachines(m_vecMachineData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIDiCupTraktFormView::addTraktManually(void)
{
	setEnabled( TRUE );	// Open up fields for editing; 061011 p�d

	clearAll();	
	m_wndListMachineBtn.EnableWindow( TRUE );
}

void CMDIDiCupTraktFormView::addTrakt(void)
{
	if (createFromFile(m_dbConnectionData,m_sLangFN,&m_structTraktIdentifer))
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
}

void CMDIDiCupTraktFormView::deleteTrakt(void)
{
	DISTRICT_DATA dataDistrict;
	TRAKT_DATA dataTrakt;
	CString sMsg;
	CString sText1;
	CString sText2;
	//CString sText3;
	CString sText4;
	CString sRegion;
	CString sDistrict;
	CString sMachine;
	CString sUrsprung;
	CString sTrakt;
	CString sCaption;
	CString sOKBtn;
	CString sCancelBtn;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING5001);
			sText2 = xml->str(IDS_STRING5011);
			//sText3 = xml->str(IDS_STRING5021);
			sText4 = xml->str(IDS_STRING5031);
			sRegion = xml->str(IDS_STRING504);
			sDistrict = xml->str(IDS_STRING505);
			sMachine = xml->str(IDS_STRING506);
			sUrsprung = xml->str(IDS_STRING211);
			sTrakt = xml->str(IDS_STRING507);

			sCaption = xml->str(IDS_STRING109);
			sOKBtn = xml->str(IDS_STRING110);
			sCancelBtn = xml->str(IDS_STRING111);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecTraktData.size() > 0)
	{
		if (m_bConnected)
		{
			// Get Region information from Database server; 070122 p�d
			CDiCupDB *pDB = new CDiCupDB(m_dbConnectionData);
			if (pDB != NULL)
			{
					// Get Regions in database; 061002 p�d	
					dataTrakt = m_vecTraktData[m_nDBIndex];
					// Setup a message for user upon deleting machine; 061010 p�d

					getRegionAndDistrictData(dataTrakt.m_n_rg,dataTrakt.m_n_di,dataDistrict);
					sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%d</b>   (%s)<br>%s : <b>%d</b>  (%s)<br>%s : <b>%d</b><br>%s : <b>%s</b><br>%s : <b>%d</b><br><hr><br>%s<br><font size=\"+4\" color=\"#ff0000\"><center>%s</center></font>"),
											sText1,
											sRegion,
											dataDistrict.m_nRegionID,
											dataDistrict.m_sRegionName,
											sDistrict,
											dataDistrict.m_nDistrictID,
											dataDistrict.m_sDistrictName,
											sMachine,
											dataTrakt.m_n_maskinlagnr,
											sUrsprung,
											getOrigin_Name(dataTrakt.m_n_ursprung),
											sTrakt,
											dataTrakt.m_n_traktnr,
											sText2,
											sText4);

					if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
					{
						// Delete Machine and ALL underlying 
						//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
						pDB->delTrakt(dataTrakt);
					}

				delete pDB;
			}	// if (pDB != NULL)
		} // 	if (m_bConnected)
	}	// if (m_vecMachineData.size() > 0)
	resetTrakt(RESET_TO_LAST_SET_NB);
}

// PROTECTED
void CMDIDiCupTraktFormView::setDefaultTypeAndOrigin(void)
{
	CString sTmp;
	for (int i = 0;i < sizeof(INVENTORY_TYPE_ARRAY)/sizeof(INVENTORY_TYPE_ARRAY[0]);i++)
	{
		m_wndCBox_Trakt_InvTyp.AddString(INVENTORY_TYPE_ARRAY[i].sInvTypeName);
	}

	for (int i = 0;i < sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0]);i++)
	{
		m_wndCBox_Trakt_Ursprung.AddString(ORIGIN_ARRAY[i].sOriginName);
	}
}

int CMDIDiCupTraktFormView::getType_setByUser(void)
{
	int nIndex = m_wndCBox_Trakt_InvTyp.GetCurSel();
	if (nIndex != CB_ERR)
		return INVENTORY_TYPE_ARRAY[nIndex].sInvTypeNum;
	else
		-1;
}

CString CMDIDiCupTraktFormView::getTypeName_setByUser(void)
{
	int nIndex = m_wndCBox_Trakt_InvTyp.GetCurSel();
	if (nIndex != CB_ERR)
		return INVENTORY_TYPE_ARRAY[nIndex].sInvTypeName;
	else
		return _T("");
}

int CMDIDiCupTraktFormView::getOrigin_setByUser(void)
{
	int nIndex = m_wndCBox_Trakt_Ursprung.GetCurSel();
	if (nIndex != CB_ERR)
		return ORIGIN_ARRAY[nIndex].nOriginNum;
	else
		return -1;
}

CString CMDIDiCupTraktFormView::getOriginName_setByUser(void)
{
	int nIndex = m_wndCBox_Trakt_Ursprung.GetCurSel();
	if (nIndex != CB_ERR)
		return ORIGIN_ARRAY[nIndex].sOriginName;
	else
		return _T("");
}

int CMDIDiCupTraktFormView::get_TraktAvvEnlRus_setByUser(void)
{
	int nIndex = m_wndCBox_Trakt_Avv_Enl_Rus.GetCurSel();
	if (nIndex != CB_ERR)
		return vec_Trakt_Avv_Enl_Rus[nIndex].m_nID;
	else
		return -1;
}

int CMDIDiCupTraktFormView::get_TraktFinnsMiljorapp_setByUser(void)
{
	int nIndex = m_wndCBox_Trakt_Finns_Miljorapp.GetCurSel();
	if (nIndex != CB_ERR)
		return vec_Trakt_Finns_Miljorapp[nIndex].m_nID;
	else
		return -1;
}

int CMDIDiCupTraktFormView::get_TraktUtfordGrot_setByUser(void)
{
	int nIndex = m_wndCBox_Trakt_Utford_Grot.GetCurSel();
	if (nIndex != CB_ERR)
		return vec_Trakt_Utford_Grot[nIndex].m_nID;
	else
		return -1;
}

// Get manually entered data; 061011 p�d
void CMDIDiCupTraktFormView::getEnteredData(void)
{
	CString sDate=_T("");
	CString sYearFrom = _T(""),sMonthFrom = _T(""),sDayFrom=_T("");

	// Setup data for new entry of Machine; 061011 p�d
	m_recNewMachine.m_nRegionID				= getRegionID();
	m_recNewMachine.m_nDistrictID			= getDistrictID();
	m_recNewMachine.m_nMachineID			= m_wndEdit_Trakt_Maskin.getInt();
	m_recNewMachine.m_sContractorName		= m_wndEdit_Trakt_Entr.getText();

	m_recNewTrakt=TRAKT_DATA();
	// Identification information
	m_recNewTrakt.m_n_typAvUppfoljning		= getType_setByUser();
	m_recNewTrakt.m_n_rg					= getRegionID();
	m_recNewTrakt.m_n_di					= getDistrictID();
	m_recNewTrakt.m_n_ursprung				= getOrigin_setByUser();
	m_recNewTrakt.m_n_traktnr				= m_wndEdit_Trakt_Nr.getInt();
	m_recNewTrakt.m_n_maskinlagnr			= m_wndEdit_Trakt_Maskin.getInt();

	// Trakt information & Data

	//m_wndDatePicker_Trakt_Datum_Inv.GetWindowText(sDate);
	COleDateTime ctTemp;
	m_wndDatePicker_Trakt_Datum_Inv.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayFrom.Format(_T("%02d"), ctTemp.GetDay());
		sMonthFrom.Format(_T("%02d"), ctTemp.GetMonth());
		sYearFrom.Format(_T("%04d"), ctTemp.GetYear());
		sDate.Format(_T("%s-%s-%s"),sYearFrom,sMonthFrom,sDayFrom);
		m_recNewTrakt.m_s_datumInv				= sDate;
	}


	m_wndDatePicker_Trakt_Datum_Slutford_Avv.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayFrom.Format(_T("%02d"), ctTemp.GetDay());
		sMonthFrom.Format(_T("%02d"), ctTemp.GetMonth());
		sYearFrom.Format(_T("%04d"), ctTemp.GetYear());
		sDate.Format(_T("%s-%s-%s"),sYearFrom,sMonthFrom,sDayFrom);
		m_recNewTrakt.m_s_datumSlutfordAvv				= sDate;
	}

	//m_wndDatePicker_Trakt_Datum_Slutford_Avv.GetWindowText(sDate);
	//m_recNewTrakt.m_s_datumSlutfordAvv		= sDate;

	m_wndDatePicker_Trakt_Datum_Slutford_Grot.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayFrom.Format(_T("%02d"), ctTemp.GetDay());
		sMonthFrom.Format(_T("%02d"), ctTemp.GetMonth());
		sYearFrom.Format(_T("%04d"), ctTemp.GetYear());
		sDate.Format(_T("%s-%s-%s"),sYearFrom,sMonthFrom,sDayFrom);
		m_recNewTrakt.m_s_datumSlutfordGrotsk				= sDate;
	}
	//m_wndDatePicker_Trakt_Datum_Slutford_Grot.GetWindowText(sDate);
	//m_recNewTrakt.m_s_datumSlutfordGrotsk	= sDate;
	

	m_recNewTrakt.m_n_avvEnlRus			=get_TraktAvvEnlRus_setByUser();
	m_recNewTrakt.m_n_finnsMiljorapport	=get_TraktFinnsMiljorapp_setByUser();
	m_recNewTrakt.m_n_utfortGrotsk		=get_TraktUtfordGrot_setByUser();
	
	m_recNewTrakt.m_s_maskinlagnamn		=	m_wndEdit_Trakt_Entr.getText();
	m_recNewTrakt.m_s_traktnamn			=	m_wndEdit_Trakt_Namn.getText();
	m_recNewTrakt.m_f_traktareal		=	m_wndEdit_Trakt_Areal.getFloat();
	m_recNewTrakt.m_f_avverkadVolym		=	m_wndEdit_Trakt_Avv_Vol.getFloat();

	if (m_enumAction == MANUALLY)
		m_recNewTrakt.m_n_EnterType = 1;

	
	getEnteredDataTabPages(&m_recNewTrakt);


	// Check RegionID and DistrictID. If they eq. -1 then
	// Set to m_recActiveTrakt values; 061011 p�d
	if (m_recNewMachine.m_nRegionID == -1 && m_recNewMachine.m_nDistrictID == -1)
	{
		m_recNewMachine.m_nRegionID = m_recActiveTrakt.m_n_rg;
		m_recNewMachine.m_nDistrictID = m_recActiveTrakt.m_n_di;
	}

	// Setup who data's entered, Maually or by file; 061201 p�d
	if (m_enumAction == MANUALLY)
		m_recNewTrakt.m_n_EnterType = 1;
}

// Method used on createing a new Trakt manually.
// Clears ALL fileds for entering data. OBS! Region/District is entered
// using a Dialog; 061011 p�d
void CMDIDiCupTraktFormView::clearAll(void)
{

	m_wndCBox_Trakt_Region.SetCurSel(-1);
	m_wndCBox_Trakt_District.SetCurSel(-1);
	m_wndCBox_Trakt_InvTyp.SetCurSel(-1);
	m_wndCBox_Trakt_Ursprung.SetCurSel(-1);
	m_wndCBox_Trakt_Avv_Enl_Rus.SetCurSel(-1);
	m_wndCBox_Trakt_Finns_Miljorapp.SetCurSel(-1);
	m_wndCBox_Trakt_Utford_Grot.SetCurSel(-1);

	m_wndEdit_Trakt_Maskin.SetWindowText(_T(""));
	m_wndEdit_Trakt_Entr.SetWindowText(_T(""));
	m_wndEdit_Trakt_Nr.SetWindowText(_T(""));
	m_wndEdit_Trakt_Namn.SetWindowText(_T(""));
	m_wndEdit_Trakt_Areal.SetWindowText(_T(""));
	m_wndEdit_Trakt_Avv_Vol.SetWindowText(_T(""));


	strDatePicker_Trakt_Datum_Inv=_T("");
	strDatePicker_Trakt_Datum_Slutford_Avv=_T("");
	strDatePicker_Trakt_Datum_Slutford_Grot=_T("");

	/*COleDateTime dt; 
	dt.SetStatus(COleDateTime::null); 
	m_wndDatePicker_Trakt_Datum_Inv.SetTime(dt); 
	m_wndDatePicker_Trakt_Datum_Slutford_Avv.SetTime(dt); 
	m_wndDatePicker_Trakt_Datum_Slutford_Grot.SetTime(dt); */

	

	UpdateData(FALSE);

	//m_wndDatePicker_Trakt_Datum_Inv.SetWindowText(_T(""));
	//m_wndDatePicker_Trakt_Datum_Slutford_Avv.SetWindowText(_T(""));
	//m_wndDatePicker_Trakt_Datum_Slutford_Grot.SetWindowText(_T(""));


	// Set m_recNewTrakt key-values to -1
	m_recNewTrakt.m_n_typAvUppfoljning	= -1;
	m_recNewTrakt.m_n_rg				= -1;
	m_recNewTrakt.m_n_di				= -1;
	m_recNewTrakt.m_n_ursprung			= -1;
	m_recNewTrakt.m_n_traktnr			= -1;
	m_recNewTrakt.m_n_maskinlagnr		= -1;

	m_recActiveTrakt.m_n_typAvUppfoljning	= -1;
	m_recActiveTrakt.m_n_rg				= -1;
	m_recActiveTrakt.m_n_di				= -1;
	m_recActiveTrakt.m_n_ursprung			= -1;
	m_recActiveTrakt.m_n_traktnr			= -1;
	m_recActiveTrakt.m_n_maskinlagnr		= -1;

	clearAllTabData();
}

// Set all Editboxes to read or read only, depending on
// if data comes from database and holds Compartmant(s) and Plot(s); 061010 p�d
void CMDIDiCupTraktFormView::setEnabled(BOOL enabled)
{
	m_wndListMachineBtn.EnableWindow( enabled );

	m_wndCBox_Trakt_InvTyp.EnableWindow( enabled );
	m_wndCBox_Trakt_Region.EnableWindow( enabled );
	m_wndCBox_Trakt_District.EnableWindow( enabled );
	m_wndCBox_Trakt_Ursprung.EnableWindow( enabled );
	m_wndCBox_Trakt_Avv_Enl_Rus.EnableWindow( enabled );
	m_wndCBox_Trakt_Finns_Miljorapp.EnableWindow( enabled );
	m_wndCBox_Trakt_Utford_Grot.EnableWindow( enabled );

	m_wndEdit_Trakt_Nr.SetReadOnly( !enabled );
	m_wndEdit_Trakt_Maskin.SetReadOnly( !enabled );
	m_wndEdit_Trakt_Entr.SetReadOnly( !enabled );
	m_wndEdit_Trakt_Namn.SetReadOnly( !enabled );
	m_wndEdit_Trakt_Areal.SetReadOnly( !enabled );
	m_wndEdit_Trakt_Avv_Vol.SetReadOnly( !enabled );

	m_wndDatePicker_Trakt_Datum_Inv.EnableWindow(enabled);
	m_wndDatePicker_Trakt_Datum_Slutford_Avv.EnableWindow(enabled);
	m_wndDatePicker_Trakt_Datum_Slutford_Grot.EnableWindow(enabled);

	setEnabledTabData(enabled);

}

// Create a string containing Region and District in the same form as displayed in
// dialog for "Bekr�fta inl�sning"; 061011 p�d
CString CMDIDiCupTraktFormView::getRegionAndDistrictData(int region,int dist,DISTRICT_DATA &rec)
{
	CString sRetStr;
	DISTRICT_DATA data;
	if (m_vecDistrictData.empty())
	{
		return _T("");
	}

	for (UINT i = 0;i < m_vecDistrictData.size();i++)
	{
		data = m_vecDistrictData[i];
		if (data.m_nRegionID == region && data.m_nDistrictID == dist)
		{
			sRetStr.Format(_T("(%d) %s  -  (%d) %s"),data.m_nRegionID,
																					data.m_sRegionName,
																					data.m_nDistrictID,
																					data.m_sDistrictName);
			rec = data;
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return sRetStr;
}

// Create a string containing Region and District in the same form as displayed in
// dialog for "Bekr�fta inl�sning"; 061011 p�d
CString CMDIDiCupTraktFormView::getMachineEntr(int region,int dist,int machine)
{
	CString sRetStr;
	MACHINE_DATA data;
	if (m_vecMachineData.empty())
		return _T("");

	for (UINT i = 0;i < m_vecMachineData.size();i++)
	{
		data = m_vecMachineData[i];
		if (data.m_nRegionID == region && data.m_nDistrictID == dist && data.m_nMachineID == machine)
		{
			sRetStr = data.m_sContractorName;
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return sRetStr;
}

// Add regions in registry, into ComboBox3; 061204 p�d
void CMDIDiCupTraktFormView::addRegionsToCBox(void)
{
	CString sText;
	if (m_vecRegionData.size() > 0)
	{
		m_wndCBox_Trakt_Region.ResetContent();
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			sText.Format(_T("%d - %s"),data.m_nRegionID,data.m_sRegionName);
			m_wndCBox_Trakt_Region.AddString(sText);
			m_wndCBox_Trakt_Region.SetItemData(i, data.m_nRegionID);
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

// Try to find region, based on index; 061204 p�d
CString CMDIDiCupTraktFormView::getRegionSelected(int region_num)
{
	CString sText;
	if (m_vecRegionData.size() > 0)
	{
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			if (data.m_nRegionID == region_num)
			{
				sText.Format(_T("%d - %s"),data.m_nRegionID,data.m_sRegionName);
				return sText;
			}
		}
	}	// if (m_vecRegionData.size() > 0)
	return _T("");
}

// Try to find index of regionnumber in m_vecRegionData; 061204 p�d
int CMDIDiCupTraktFormView::getRegionID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected Region is used on New item; 061205 p�d
	//if (m_vecTraktData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktData.size() && m_enumAction == NOTHING)
	//{
	//	return m_vecTraktData[m_nDBIndex].m_n_rg;
	//}
	//else
	//{
		int nIdx = m_wndCBox_Trakt_Region.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return m_wndCBox_Trakt_Region.GetItemData(nIdx);
		}
	//}
	return -1;
}

// Add Districts into Combobox4, based on selected region; 061204 p�d
void CMDIDiCupTraktFormView::addDistrictsToCBox(void)
{
	int nRegionIndex = m_wndCBox_Trakt_Region.GetCurSel();
	CString sText;
	int nCnt = 0;
	if (nRegionIndex != CB_ERR && m_vecDistrictData.size() > 0)
	{
		REGION_DATA dataRegion = m_vecRegionData[nRegionIndex];
		m_wndCBox_Trakt_District.ResetContent();
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA dataDist = m_vecDistrictData[i];
			if (dataDist.m_nRegionID == dataRegion.m_nRegionID)
			{
				sText.Format(_T("%d - %s"),dataDist.m_nDistrictID,dataDist.m_sDistrictName);
				m_wndCBox_Trakt_District.AddString(sText);
				m_wndCBox_Trakt_District.SetItemData(nCnt,dataDist.m_nDistrictID);
				nCnt++;
			}	// if (dataDist.m_nRegionID == dataRegion.m_nRegionID)
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (nRegionIndex != CB_ERR && m_vecDistrictData.size() > 0)
}

// Try to find district, based on index; 061204 p�d
CString CMDIDiCupTraktFormView::getDistrictSelected(int region_num,int district_num)
{
	CString sText;
	if (m_vecDistrictData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nRegionID == region_num && data.m_nDistrictID == district_num)
			{
				sText.Format(_T("%d - %s"),data.m_nDistrictID,data.m_sDistrictName);
				return sText;
			}	// if (data.m_nRegionID == region_num && data.m_nDistrictID == district_num)
		}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
	
		return _T("");
}

int CMDIDiCupTraktFormView::getDistrictID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected District is used on New item; 061205 p�d
	//if (m_vecTraktData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktData.size() && m_enumAction == NOTHING)
	//{
	//	return m_vecTraktData[m_nDBIndex].m_n_di;
	//}
	//else
	//{
		int nIdx = m_wndCBox_Trakt_District.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return m_wndCBox_Trakt_District.GetItemData(nIdx);
		}
	//}
	return -1;
}


// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIDiCupTraktFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

void CMDIDiCupTraktFormView::OnBnClicked_Trakt_Maskin()
{
	MACHINE_REG_DATA recMachineReg;
	CSelectRegionDistDlg *dlg = new CSelectRegionDistDlg(WTL_MACHINE_REG);

	if (dlg->DoModal())
	{
		dlg->getMachineReg(recMachineReg);
		m_recNewMachine.m_nMachineID = recMachineReg.m_nMachineRegID;
		m_recNewMachine.m_sContractorName = recMachineReg.m_sContractorName;

		m_wndEdit_Trakt_Maskin.setInt(recMachineReg.m_nMachineRegID);
		m_wndEdit_Trakt_Entr.SetWindowText(recMachineReg.m_sContractorName);
	}

	delete dlg;
}

// PUBLIC
void CMDIDiCupTraktFormView::saveTrakt(void)
{
	TRAKT_IDENTIFER old_trakt_id;
	TRAKT_IDENTIFER new_trakt_ident;
	BOOL didSave=FALSE;
	if (isOKToSave(&old_trakt_id) && m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CDiCupDB *pDB = new CDiCupDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			new_trakt_ident.nType = getType_setByUser();
			new_trakt_ident.nRegionID =getRegionID();
			new_trakt_ident.nDistrictID = getDistrictID();
			new_trakt_ident.nOrigin = getOrigin_setByUser();
			new_trakt_ident.nTraktNum = m_wndEdit_Trakt_Nr.getInt();
			new_trakt_ident.nMachineID = m_wndEdit_Trakt_Maskin.getInt();
			//Kolla om identitet �ndras i s� fall kolla om trakten redan existerar i DB annars uppdatera redan existerande trakt.
			if(	!((old_trakt_id.nType==new_trakt_ident.nType) &&
				(old_trakt_id.nRegionID==new_trakt_ident.nRegionID) &&
				(old_trakt_id.nDistrictID==new_trakt_ident.nDistrictID) &&
				(old_trakt_id.nMachineID==new_trakt_ident.nMachineID) &&
				(old_trakt_id.nTraktNum==new_trakt_ident.nTraktNum) &&
				(old_trakt_id.nOrigin==new_trakt_ident.nOrigin))
				&& 
				(												//Denna koll k�rs i s� fall �r det en ny trakt
				old_trakt_id.nType!=-1 &&
				old_trakt_id.nRegionID!=-1 &&
				old_trakt_id.nDistrictID!=-1 &&
				old_trakt_id.nMachineID!=-1 &&
				old_trakt_id.nTraktNum!=-1 &&
				old_trakt_id.nOrigin!=-1)
				)
			{
					if(pDB->existTrakt(new_trakt_ident))
					{
						::MessageBox(0,m_sNotOkToSave,m_sTraktExists,MB_ICONSTOP | MB_OK);
						didSave=FALSE;
					}
					else
					{
						getEnteredData();
						// Setup index information; 061207 p�d
						m_structTraktIdentifer.nRegionID	= m_recNewTrakt.m_n_rg;
						m_structTraktIdentifer.nDistrictID	= m_recNewTrakt.m_n_di;
						m_structTraktIdentifer.nMachineID	= m_recNewTrakt.m_n_maskinlagnr;
						m_structTraktIdentifer.nTraktNum	= m_recNewTrakt.m_n_traktnr;
						m_structTraktIdentifer.nType		= m_recNewTrakt.m_n_typAvUppfoljning;
						m_structTraktIdentifer.nOrigin		= m_recNewTrakt.m_n_ursprung;


						// Add or Update Machine, if not already in register; 061204 p�d
						if (!pDB->addMachine( m_recNewMachine ))
							pDB->updMachine( m_recNewMachine );

						//Om man inte lyckas uppdatera befintlig trakt s� �r det en helt ny trakt i s� fall spara den
						if(!pDB->updTrakt(m_recNewTrakt,old_trakt_id))
						{
							if (!pDB->addTrakt( m_recNewTrakt ))
							{
								::MessageBox(0,m_sCantSaveTraktMsg,m_sErrCap,MB_ICONSTOP | MB_OK);					

							}		
							else
								didSave=TRUE;
						}
						else
							didSave=TRUE;
					}
			}
			else
			{

				getEnteredData();
				// Setup index information; 061207 p�d
				m_structTraktIdentifer.nRegionID	= m_recNewTrakt.m_n_rg;
				m_structTraktIdentifer.nDistrictID	= m_recNewTrakt.m_n_di;
				m_structTraktIdentifer.nMachineID	= m_recNewTrakt.m_n_maskinlagnr;
				m_structTraktIdentifer.nTraktNum	= m_recNewTrakt.m_n_traktnr;
				m_structTraktIdentifer.nType		= m_recNewTrakt.m_n_typAvUppfoljning;
				m_structTraktIdentifer.nOrigin		= m_recNewTrakt.m_n_ursprung;


				// Add or Update Machine, if not already in register; 061204 p�d
				if (!pDB->addMachine( m_recNewMachine ))
					pDB->updMachine( m_recNewMachine );

				if (!pDB->addTrakt( m_recNewTrakt ))
				{
					if(!pDB->updTrakt(m_recNewTrakt,old_trakt_id))
					{
						::MessageBox(0,m_sCantSaveTraktMsg,m_sErrCap,MB_ICONSTOP | MB_OK);					
					}
					else
						didSave=TRUE;
				}		
				else
					didSave=TRUE;

			}
			delete pDB;
		}	// if (pDB != NULL)

		if(didSave)
		{
			resetIsDirty();
			resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
			m_enumAction = NOTHING;
		}

	}	// if (isOKToSave())
}

int CMDIDiCupTraktFormView::checkOtherVariablesToSave()
{
	int nErr=1;
	CString sDate=_T("");
	TRAKT_DATA trakt;
	CString csErrorMsg=_T("");
	getEnteredDataTabPages(&trakt);

	if(get_TraktAvvEnlRus_setByUser()==-1)
	{
		csErrorMsg+=_T("Avv enl RUS m�ste anges!\n");
		nErr=0;
	}

	if(get_TraktFinnsMiljorapp_setByUser()==-1)
	{
		csErrorMsg+=_T("Milj�rapport m�ste anges!\n");
		nErr=0;
	}

	if(m_wndEdit_Trakt_Areal.getFloat()<=0.0)
	{
		csErrorMsg += _T("Traktareal m�ste anges!\n");
		nErr=0;
	}

	if(get_TraktUtfordGrot_setByUser()==-1)
	{
		csErrorMsg += _T("Utf�rt grotskotning m�ste anges!\n");
		nErr=0;
	}

	if( trakt.m_n_mindreAllvKorskada == 0 &&
		(trakt.m_n_mindreAllvKorskadaTyp == -1 || trakt.m_n_mindreAllvKorskadaTyp == 0))
	{
		csErrorMsg += _T("Typ av mindre allvarlig k�rskada m�ste anges!\n");
		nErr=0;
	}
	if( trakt.m_n_allvarligKorskada == 0 &&
		(trakt.m_n_allvarligKorskadaTyp == -1 || trakt.m_n_allvarligKorskadaTyp == 0))
	{
		csErrorMsg += _T("Typ av allvarlig k�rskada m�ste anges!\n");
		nErr=0;
	}
	if( trakt.m_n_forebyggtKorskador == 1 && trakt.m_s_forebyggtKorskadorKommentar.GetLength() == 0 ) // Mandatory comment on "Nej"
	{
		csErrorMsg += _T("Kommentar p� f�rebyggt k�rskador m�ste anges!\n");
		nErr=0;
	}

	if( trakt.m_n_baskrTraktplOK < 0 )
	{
		csErrorMsg += _T("Baskr. traktpl. OK m�ste anges!\n");
		nErr=0;
	}
	else if( trakt.m_n_baskrTraktplOK == 1 && trakt.m_s_baskrTraktplBrister.GetLength() == 0 ) // Mandatory comment on "Nej"
	{
		csErrorMsg += _T("Vilka var bristerna m�ste anges!\n");
		nErr=0;
	}

	if( trakt.m_n_klaratStrategiskaMal < 0 )
	{
		csErrorMsg += _T("Klarat strategiska m�l m�ste anges!\n");
		nErr=0;
	}

	if( trakt.m_n_stubbhojd < 0 || trakt.m_n_stubbhojd > 100 )
	{
		csErrorMsg += _T("Andel h�ga stubbar m�ste vara mellan 0-100!\n");
		nErr=0;
	}

	if(nErr==0)
	{
	AfxMessageBox(csErrorMsg);
	}
	return nErr;
}

// Check that information needed to be abel to save Trakt, is enterd
// by user; 061113 p�d
BOOL CMDIDiCupTraktFormView::isOKToSave(TRAKT_IDENTIFER *old_trakt_ident)
{
	TRAKT_IDENTIFER new_trakt_ident;

	if (_tcscmp(m_wndCBox_Trakt_Region.getText().Trim(),_T("")) == 0 ||
		_tcscmp(m_wndCBox_Trakt_District.getText().Trim(),_T("")) == 0 ||
		m_wndEdit_Trakt_Nr.getInt() <= 0 ||
		m_wndEdit_Trakt_Maskin.getInt() <= 0 ||
		getType_setByUser() == -1 ||
		getOrigin_setByUser() == -1) 
	{
		// Also tell user that there's insufficent data; 061113 p�d
		::MessageBox(0,m_sNotOkToSave,m_sErrCap,MB_ICONSTOP | MB_OK);
		return FALSE;
	}
	else
	{

		if(checkOtherVariablesToSave()==0)
			return FALSE;

		//Load old trakt identification in order to find if the keys has been altered and needs to be updated
		old_trakt_ident->nType=m_recActiveTrakt.m_n_typAvUppfoljning;
		old_trakt_ident->nRegionID=m_recActiveTrakt.m_n_rg;
		old_trakt_ident->nDistrictID=m_recActiveTrakt.m_n_di;
		old_trakt_ident->nMachineID=m_recActiveTrakt.m_n_maskinlagnr;
		old_trakt_ident->nTraktNum=m_recActiveTrakt.m_n_traktnr;
		old_trakt_ident->nOrigin=m_recActiveTrakt.m_n_ursprung;
	}

	return TRUE;
}

// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
BOOL CMDIDiCupTraktFormView::isDataChanged(void)
{
	if (getIsDirty())
	{
		saveTrakt();
		resetIsDirty();
		return TRUE;
	}
	return FALSE;
}

void CMDIDiCupTraktFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
											 m_nDBIndex < (m_vecTraktData.size()-1));
}

/*
	RESET_TO_FIRST_SET_NB,				// Set to first item and set Navigationbar
  RESET_TO_FIRST_NO_NB,					// Set to first item and no Navigationbar (disable all)
	RESET_TO_LAST_SET_NB,					// Set to last item and set Navigationbar
	RESET_TO_LAST_NO_NB,					// Set to last item and no Navigationbar (disable all)
	RESET_TO_JUST_ENTERED_SET_NB,	// Set to just entered data (from file or manually) and set Navigationbar
	RESET_TO_JUST_ENTERED_NO_NB		// Set to just entered data (from file or manually) and no Navigationbar
*/
// Reset and get data from database; 061010 p�d
void CMDIDiCupTraktFormView::resetTrakt(enumRESET reset)
{
	// Get updated data from Database
	getMachineFromDB();
	// Get updated data from Database
	getTraktsFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecTraktData.size() > 0)
	{

		if (reset == RESET_TO_FIRST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_FIRST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_LAST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_LAST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_JUST_ENTERED_SET_NB || reset == RESET_TO_JUST_ENTERED_NO_NB)
		{

			// Find itemindex for last entry, from file, and set on populateData; 061207 p�d
			for (UINT i = 0;i < m_vecTraktData.size();i++)
			{
				TRAKT_DATA data = m_vecTraktData[i];
				if (data.m_n_rg == m_structTraktIdentifer.nRegionID &&
					  data.m_n_di == m_structTraktIdentifer.nDistrictID &&
						data.m_n_maskinlagnr == m_structTraktIdentifer.nMachineID &&
						data.m_n_traktnr == m_structTraktIdentifer.nTraktNum &&
						data.m_n_typAvUppfoljning == m_structTraktIdentifer.nType &&
						data.m_n_ursprung == m_structTraktIdentifer.nOrigin)
				{
					// Reset to last item in m_vecTraktData
					m_nDBIndex = i;
					break;
				}	// if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
			}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
			// Populate data on User interface
			populateData(m_nDBIndex);
			if (reset == RESET_TO_JUST_ENTERED_SET_NB)
			{
				setNavigationButtons(m_nDBIndex > 0,
					   								m_nDBIndex < (m_vecTraktData.size()-1));
			}
			else if (reset == RESET_TO_JUST_ENTERED_NO_NB)
			{
				setNavigationButtons(FALSE,FALSE);
			}
		}	// if (reset == RESET_TO_JUST_ENTERED_SET_NB)
	}	// if (m_vecTraktData.size() > 0)
	else 
	{
		setNavigationButtons(FALSE,FALSE);
		populateData(-1);	// Clear data
	}
}



// Event handles for data changed in Edit boxes; 061113 p�d
void CMDIDiCupTraktFormView::OnCbnSelchangeTraktRegion()
{
	addDistrictsToCBox();
}

BOOL CMDIDiCupTraktFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;
	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
				 rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
				 TRACE0( "Warning: couldn't create client tab for view.\n" );
				 // pWnd will be cleaned up by PostNcDestroy
				 return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);
	return TRUE;
}
