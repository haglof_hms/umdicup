#pragma once

#include "UMDiCupDB.h"
#include "Resource.h"

class CTraktPageFour_NaturMiljo : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTraktPageFour_NaturMiljo)

	BOOL m_bInitialized;
	CString m_sLangFN;

	CMyComboBox m_wndCBox_Trakt_Page4_Myr;
	CMyComboBox m_wndCBox_Trakt_Page4_Hallmark;
	CMyComboBox m_wndCBox_Trakt_Page4_OmrOrorda;
	CMyComboBox m_wndCBox_Trakt_Page4_ObjSkrotas;
	CMyComboBox m_wndCBox_Trakt_Page4_Kulturminnen;
	CMyComboBox m_wndCBox_Trakt_Page4_Fornminnen;
	CMyComboBox m_wndCBox_Trakt_Page4_Skyddszoner;
	CMyComboBox m_wndCBox_Trakt_Page4_Naturtrad;
	CMyComboBox m_wndCBox_Trakt_Page4_Framtidstrad;
	CMyComboBox m_wndCBox_Trakt_Page4_Hogstubbar;
	CMyComboBox m_wndCBox_Trakt_Page4_Dodatrad;
	CMyComboBox m_wndCBox_Trakt_Page4_Kalytebegr;
	CMyComboBox m_wndCBox_Trakt_Page4_Nedskrap;
	CMyComboBox m_wndCBox_Trakt_Page4_AvvTidp;
	CMyComboBox m_wndCBox_Trakt_Page4_Upplevelse;
	CMyComboBox m_wndCBox_Trakt_Page4_Tekn;

	CMyExtEdit m_wndEdit_Trakt_Page4_Natur;				//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_Framtid;			//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_Hogst;				//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_OvrighHansyn;

	CMyExtEdit m_wndEdit_Trakt_Page4_SparadArealHa;		//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_SparadArealProc;	//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_SparadVolymHa;		//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_SparadVolymProc;	//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_HogstubbSt;		//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_HogstubbStHa;		//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_FramNaturSt;		//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_FramNaturStHa;		//Only for show no editing
	CMyExtEdit m_wndEdit_Trakt_Page4_OvrigaKomment;

	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Myr;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Hallmark;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_OmrOrorda;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_ObjSkrotas;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Kulturminnen;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Fornminnen;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Skyddszoner;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Naturtrad;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Framtidstrad;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Hogstubbar;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Dodatrad;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Kalytebegr;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Nedskrap;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_AvvTidp;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Upplevelse;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page4_Tekn;

private:
	void setTraktValArrays();
	void clearAll(void);
	void setAllReadOnly(BOOL ro1);	
public:
	void setEnabled(BOOL enabled);
	void resetIsDirty(void);
	void populateData(TRAKT_DATA data);
	BOOL getIsDirty(void);
	void getEnteredData(TRAKT_DATA *trakt);
	virtual void OnInitialUpdate();
				 
	enum { IDD = IDD_FORMVIEW_UMDICUP_TRAKT_PAGE4 };
	CTraktPageFour_NaturMiljo();           // protected constructor used by dynamic creation
	virtual ~CTraktPageFour_NaturMiljo();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()	
};