#if !defined(AFX_UMDICUPDB_H)
#define AFX_UMDICUPDB_H

#include "StdAfx.h"

#include "AddDataToDataBase.h"

/*  TRAKT TABELL
	typ int NOT NULL,
	region int NOT NULL,
	distrikt int NOT NULL,
	ursprung int NOT NULL,
	traktnr int NOT NULL,
	maskinlagnr int NOT NULL,

	traktnamn varchar(50),
	avvEnlRus int,
	finnsMiljorapport int,
	traktareal float,
	avverkadVolym float,
	maskinlagnamn varchar(50),
	datumInv varchar(50),
	datumSlutfordAvv varchar(50),
	utfortGrotsk int,
	datumSlutfordGrotsk varchar(50),
	naturvardestradSum int,
	framtidstradSum int,
	farskaHogstubbarSum int,
	sparadArealSum float,
	sparadVolymSum int,
	kvarglomtVirkeSum int,
	risadeBasstrak int,
	hansynskrBiotoper int,
	mindreAllvKorskada int,
	mindreAllvKorskadaTyp int,
	allvarligKorskada int,
	allvarligKorskadaTyp int,
	forebyggtKorskador int,
	forebyggtKorskadorKommentar varchar(50),
	basvagUtanforTrakt int,
	kommentarAvKorskador varchar(50),
	skadadeVagdiken int,
	parallellitet int,
	hogarnasBeskaffenhet int,
	placeringAvHogar int,
	innehallHogar int,
	kvalitetPaValtor int,
	kommentarPaGrottillredning varchar(50),
	myr int,
	hallmark int,
	omrSomLamnOrorda int,
	objektSomSkallSkotas int,
	kulturminnen int,
	fornminnen int,
	skyddszoner int,
	naturvardestrad int,
	framtidstrad int,
	farskaHogstubbar int,
	dodaTrad int,
	kalytebegransning int,
	nedskrapning int,
	avverkningstidpunkt int,
	upplevelsehansyn int,
	teknEkonimp int,
	ovrigHansyn varchar(50),
	ovrigaKommentarerNaturOchMiljo varchar(50),
	baskrTraktplOK int,
	baskrTraktplBrister varchar(50),
	avvikandeArbUtifranTraktdirektivet varchar(50),
	hansynTillEfterkommandeAtgarder varchar(50),
	stubbhojd int,
	klaratStrategiskaMal int,
	forbattringsforslag varchar(50),
	EnterType int
	*/

//////////////////////////////////////////////////////////////////////////////////
// CDiCupDBDB; Handle ALL transactions for DiCup

// REGION DATA STRUCTURES
typedef struct _region_data
{
	int m_nRegionID;
	CString m_sRegionName;

	_region_data(void)
	{
		m_nRegionID = -1;
		m_sRegionName = "";
	}

	_region_data(int id,LPCTSTR name)
	{
		m_nRegionID = id;
		m_sRegionName = name;
	}
} REGION_DATA;
typedef std::vector<REGION_DATA> vecRegionData;


// DISTRICT DATA STRUCTURES
typedef struct _district_data
{
	int m_nRegionID;
	CString m_sRegionName;
	int m_nDistrictID;
	CString m_sDistrictName;

	_district_data(void)
	{
		m_nRegionID = -1;
		m_sRegionName = "";
		m_nDistrictID = -1;
		m_sDistrictName = "";
	}

	_district_data(int region_id,LPCTSTR region_name,int district_id,LPCTSTR district_name)
	{
		m_nRegionID = region_id;
		m_sRegionName = region_name;
		m_nDistrictID = district_id;
		m_sDistrictName = district_name;
	}
} DISTRICT_DATA;
typedef std::vector<DISTRICT_DATA> vecDistrictData;

// MACHINE DATA STRUCTURES
typedef struct _machine_data
{
	int m_nMachineID;
	CString m_sContractorName;
	int m_nRegionID;
	CString m_sRegionName;
	int m_nDistrictID;
	CString m_sDistrictName;

	_machine_data(void)
	{
		m_nMachineID = -1;
		m_nRegionID = -1;
		m_nDistrictID = -1;
		m_sContractorName = "";
	}

	_machine_data(int machine_id,
							  LPCTSTR contractor_name,
								int region_id,
								LPCTSTR region_name,
								int district_id,
								LPCTSTR district_name)
	{
		m_nMachineID = machine_id;
		m_sContractorName = contractor_name;
		m_nRegionID = region_id;
		m_sRegionName = region_name;
		m_nDistrictID = district_id;
		m_sDistrictName = district_name;
	}
} MACHINE_DATA;
typedef std::vector<MACHINE_DATA> vecMachineData;

// MACHINE REGISTRY DATA STRUCTURES
typedef struct _machine_reg_data
{
	int m_nMachineRegID;
	CString m_sContractorName;

	_machine_reg_data(void)
	{
		m_nMachineRegID = -1;
		m_sContractorName = "";
	}

	_machine_reg_data(int machine_reg_id,
							  LPCTSTR contractor_name)
	{
		m_nMachineRegID = machine_reg_id;
		m_sContractorName = contractor_name;
	}
} MACHINE_REG_DATA;
typedef std::vector<MACHINE_REG_DATA> vecMachineRegData;


typedef struct _sql_machine_question
{
	int nRegionID;
	int nDistrictID;
	TCHAR szSQL[1024];	// Buffer to hold actual question; 061010 p�d

} SQL_MACHINE_QUESTION;

typedef struct _sql_trakt_question
{
	int nRegionID;
	int nDistrictID;
	int nMachineID;
	int nTraktID;
	int nType;
	int nOrigin;
	TCHAR szSQL[1024];	// Buffer to hold actual question; 061010 p�d

} SQL_TRAKT_QUESTION;


// TRAKT DATA STRUCTURES
typedef struct _trakt_data
{
	int m_n_typAvUppfoljning;
	int m_n_rg;
	int m_n_di;
	int m_n_ursprung;
	int m_n_traktnr;
	int m_n_maskinlagnr;

	CString m_s_traktnamn;
	int m_n_avvEnlRus;
	int m_n_finnsMiljorapport;
	float m_f_traktareal;
	float m_f_avverkadVolym;	
	CString m_s_maskinlagnamn;
	CString m_s_datumInv;
	CString m_s_datumSlutfordAvv;
	int m_n_utfortGrotsk;
	CString m_s_datumSlutfordGrotsk;
	int m_n_naturvardestradSum;
	int m_n_framtidstradSum;
	int m_n_farskaHogstubbarSum;
	float m_f_sparadArealSum;
	int m_n_sparadVolymSum;
	int m_n_kvarglomtVirkeSum;
	int m_n_risadeBasstrak;
	int m_n_hansynskrBiotoper;
	int m_n_mindreAllvKorskada;
	int m_n_mindreAllvKorskadaTyp;
	int m_n_allvarligKorskada;
	int m_n_allvarligKorskadaTyp;
	int m_n_forebyggtKorskador;
	CString m_s_forebyggtKorskadorKommentar;
	int m_n_basvagUtanforTrakt;
	CString m_s_kommentarAvKorskador;
	int m_n_skadadeVagdiken;
	int m_n_parallellitet;
	int m_n_hogarnasBeskaffenhet;
	int m_n_placeringAvHogar;
	int m_n_innehallHogar;
	int m_n_kvalitetPaValtor;
	CString m_s_kommentarPaGrottillredning;
	int m_n_myr;
	int m_n_hallmark;
	int m_n_omrSomLamnOrorda;
	int m_n_objektSomSkallSkotas;
	int m_n_kulturminnen;
	int m_n_fornminnen;
	int m_n_skyddszoner;
	int m_n_naturvardestrad;
	int m_n_framtidstrad;
	int m_n_farskaHogstubbar;
	int m_n_dodaTrad;
	int m_n_kalytebegransning;
	int m_n_nedskrapning;
	int m_n_avverkningstidpunkt;
	int m_n_upplevelsehansyn;
	int m_n_teknEkonimp;
	CString m_s_ovrigHansyn;
	CString m_s_ovrigaKommentarerNaturOchMiljo;
	int m_n_baskrTraktplOK;
	CString m_s_baskrTraktplBrister;
	CString m_s_avvikandeArbUtifranTraktdirektivet;
	CString m_s_hansynTillEfterkommandeAtgarder;
	int m_n_stubbhojd;
	int m_n_klaratStrategiskaMal;
	CString m_s_forbattringsforslag;
	//0 from file 1 manual
	int m_n_EnterType;

	_trakt_data(void)
	{

		m_n_typAvUppfoljning		= -1;
		m_n_rg						= -1;
		m_n_di						= -1;
		m_n_ursprung				= -1;
		m_n_traktnr					= -1;
		m_n_maskinlagnr				= -1;

		m_s_traktnamn							=_T("");
		m_n_avvEnlRus				= -1;
		m_n_finnsMiljorapport		= -1;
		m_f_traktareal				= 0.0;
		m_f_avverkadVolym			= 0.0;
		m_s_maskinlagnamn						=_T("");
		m_s_datumInv							=_T("");
		m_s_datumSlutfordAvv					=_T("");
		m_n_utfortGrotsk			= -1;
		m_s_datumSlutfordGrotsk					=_T("");
		m_n_naturvardestradSum		= 0;
		m_n_framtidstradSum			= 0;
		m_n_farskaHogstubbarSum		= 0;
		m_f_sparadArealSum			= 0.0;
		m_n_sparadVolymSum			= 0;
		m_n_kvarglomtVirkeSum		= 0;
		m_n_risadeBasstrak			= -1;
		m_n_hansynskrBiotoper		= -1;
		m_n_mindreAllvKorskada		= -1;
		m_n_mindreAllvKorskadaTyp	= -1;
		m_n_allvarligKorskada		= -1;
		m_n_allvarligKorskadaTyp	= -1;
		m_n_forebyggtKorskador		= -1;
		m_s_forebyggtKorskadorKommentar			=_T("");
		m_n_basvagUtanforTrakt		= -1;
		m_s_kommentarAvKorskador				=_T("");
		m_n_skadadeVagdiken			= -1;
		m_n_parallellitet			= -1;
		m_n_hogarnasBeskaffenhet	= -1;
		m_n_placeringAvHogar		= -1;
		m_n_innehallHogar			= -1;
		m_n_kvalitetPaValtor		= -1;
		m_s_kommentarPaGrottillredning			=_T("");
		m_n_myr						= -1;
		m_n_hallmark				= -1;
		m_n_omrSomLamnOrorda		= -1;
		m_n_objektSomSkallSkotas	= -1;
		m_n_kulturminnen			= -1;
		m_n_fornminnen				= -1;
		m_n_skyddszoner				= -1,
		m_n_dodaTrad				= -1,
		m_n_kalytebegransning		= -1;
		m_n_nedskrapning			= -1;
		m_n_avverkningstidpunkt		= -1;
		m_n_upplevelsehansyn		= -1;
		m_n_teknEkonimp				= -1;
		m_s_ovrigHansyn							=_T("");
		m_s_ovrigaKommentarerNaturOchMiljo		=_T("");
		m_n_baskrTraktplOK			= -1;
		m_s_baskrTraktplBrister					=_T("");
		m_s_avvikandeArbUtifranTraktdirektivet	=_T("");
		m_s_hansynTillEfterkommandeAtgarder		=_T("");
		m_n_stubbhojd				= 0;
		m_n_klaratStrategiskaMal	= -1;
		m_s_forbattringsforslag					=_T("");
		m_n_EnterType				= 1;

	}

	_trakt_data(	int n_typAvUppfoljning,
	int n_rg,
	int n_di,
	int n_ursprung,
	int n_traktnr,
	int n_maskinlagnr,
	LPCTSTR s_traktnamn,
	int n_avvEnlRus,
	int n_finnsMiljorapport,
	float f_traktareal,
	float f_avverkadVolym,	
	LPCTSTR s_maskinlagnamn,
	LPCTSTR s_datumInv,
	LPCTSTR s_datumSlutfordAvv,
	int n_utfortGrotsk,
	LPCTSTR s_datumSlutfordGrotsk,
	int n_naturvardestradSum,
	int n_framtidstradSum,
	int n_farskaHogstubbarSum,
	float f_sparadArealSum,
	int n_sparadVolymSum,
	int n_kvarglomtVirkeSum,
	int n_risadeBasstrak,
	int n_hansynskrBiotoper,
	int n_mindreAllvKorskada,
	int n_mindreAllvKorskadaTyp,
	int n_allvarligKorskada,
	int n_allvarligKorskadaTyp,
	int n_forebyggtKorskador,
	LPCTSTR s_forebyggtKorskadorKommentar,
	int n_basvagUtanforTrakt,
	LPCTSTR s_kommentarAvKorskador,
	int n_skadadeVagdiken,
	int n_parallellitet,
	int n_hogarnasBeskaffenhet,
	int n_placeringAvHogar,
	int n_innehallHogar,
	int n_kvalitetPaValtor,
	LPCTSTR s_kommentarPaGrottillredning,
	int n_myr,
	int n_hallmark,
	int n_omrSomLamnOrorda,
	int n_objektSomSkallSkotas,
	int n_kulturminnen,
	int n_fornminnen,
	int n_skyddszoner,
	int n_naturvardestrad,
	int n_framtidstrad,
	int n_farskaHogstubbar,
	int n_dodaTrad,
	int n_kalytebegransning,
	int n_nedskrapning,
	int n_avverkningstidpunkt,
	int n_upplevelsehansyn,
	int n_teknEkonimp,
	LPCTSTR s_ovrigHansyn,
	LPCTSTR s_ovrigaKommentarerNaturOchMiljo,
	int n_baskrTraktplOK,
	LPCTSTR s_baskrTraktplBrister,
	LPCTSTR s_avvikandeArbUtifranTraktdirektivet,
	LPCTSTR s_hansynTillEfterkommandeAtgarder,
	int n_stubbhojd,
	int n_klaratStrategiskaMal,
	LPCTSTR s_forbattringsforslag,
	int n_EnterType)
	{
	m_n_typAvUppfoljning=n_typAvUppfoljning;
	m_n_rg=n_rg;
	m_n_di=n_di;
	m_n_ursprung=n_ursprung;
	m_n_traktnr=n_traktnr;
	m_s_traktnamn=s_traktnamn;
	m_n_avvEnlRus=n_avvEnlRus;
	m_n_finnsMiljorapport=n_finnsMiljorapport;
	m_f_traktareal=f_traktareal;
	m_f_avverkadVolym=f_avverkadVolym;
	m_n_maskinlagnr=n_maskinlagnr;
	m_s_maskinlagnamn=s_maskinlagnamn;
	m_s_datumInv=s_datumInv;
	m_s_datumSlutfordAvv=s_datumSlutfordAvv;
	m_n_utfortGrotsk=n_utfortGrotsk;
	m_s_datumSlutfordGrotsk=s_datumSlutfordGrotsk;
	m_n_naturvardestradSum=n_naturvardestradSum;
	m_n_framtidstradSum=n_framtidstradSum;
	m_n_farskaHogstubbarSum=n_farskaHogstubbarSum;
	m_f_sparadArealSum=f_sparadArealSum;
	m_n_sparadVolymSum=n_sparadVolymSum;
	m_n_kvarglomtVirkeSum=n_kvarglomtVirkeSum;
	m_n_risadeBasstrak=n_risadeBasstrak;
	m_n_hansynskrBiotoper=n_hansynskrBiotoper;
	m_n_mindreAllvKorskada=n_mindreAllvKorskada;
	m_n_mindreAllvKorskadaTyp=n_mindreAllvKorskadaTyp;
	m_n_allvarligKorskada=n_allvarligKorskada;
	m_n_allvarligKorskadaTyp=n_allvarligKorskadaTyp;
	m_n_forebyggtKorskador=n_forebyggtKorskador;
	m_s_forebyggtKorskadorKommentar=s_forebyggtKorskadorKommentar;
	m_n_basvagUtanforTrakt=n_basvagUtanforTrakt;
	m_s_kommentarAvKorskador=s_kommentarAvKorskador;
	m_n_skadadeVagdiken=n_skadadeVagdiken;
	m_n_parallellitet=n_parallellitet;
	m_n_hogarnasBeskaffenhet=n_hogarnasBeskaffenhet;
	m_n_placeringAvHogar=n_placeringAvHogar;
	m_n_innehallHogar=n_innehallHogar;
	m_n_kvalitetPaValtor=n_kvalitetPaValtor;
	m_s_kommentarPaGrottillredning=s_kommentarPaGrottillredning;
	m_n_myr=n_myr;
	m_n_hallmark=n_hallmark;
	m_n_omrSomLamnOrorda=n_omrSomLamnOrorda;
	m_n_objektSomSkallSkotas=n_objektSomSkallSkotas;
	m_n_kulturminnen=n_kulturminnen;
	m_n_fornminnen=n_fornminnen;
	m_n_skyddszoner=n_skyddszoner;
	m_n_naturvardestrad=n_naturvardestrad;
	m_n_framtidstrad=n_framtidstrad;
	m_n_farskaHogstubbar=n_farskaHogstubbar;
	m_n_dodaTrad=n_dodaTrad;
	m_n_kalytebegransning=n_kalytebegransning;
	m_n_nedskrapning=n_nedskrapning;
	m_n_avverkningstidpunkt=n_avverkningstidpunkt;
	m_n_upplevelsehansyn=n_upplevelsehansyn;
	m_n_teknEkonimp=n_teknEkonimp;
	m_s_ovrigHansyn=s_ovrigHansyn;
	m_s_ovrigaKommentarerNaturOchMiljo=s_ovrigaKommentarerNaturOchMiljo;
	m_n_baskrTraktplOK=n_baskrTraktplOK;
	m_s_baskrTraktplBrister=s_baskrTraktplBrister;
	m_s_avvikandeArbUtifranTraktdirektivet=s_avvikandeArbUtifranTraktdirektivet;
	m_s_hansynTillEfterkommandeAtgarder=s_hansynTillEfterkommandeAtgarder;
	m_n_stubbhojd=n_stubbhojd;
	m_n_klaratStrategiskaMal=n_klaratStrategiskaMal;
	m_s_forbattringsforslag=s_forbattringsforslag;
	m_n_EnterType=n_EnterType;
	}
} TRAKT_DATA;
typedef std::vector<TRAKT_DATA> vecTraktData;




////////////////////////////////////////////////////////////////////////////////
// These classes is used in CXTPReportControls for region and district; 061005 p�d

// CRegionReportDataRec

class CRegionReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CRegionReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CRegionReportDataRec(UINT index,int id,LPCTSTR name)
	{
		m_nIndex = index;
		AddItem(new CIntItem(id));
		AddItem(new CTextItem(name));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

// CDistrictReportDataRec

class CDistrictReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CDistrictReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CDistrictReportDataRec(UINT index,int region_id,LPCTSTR region_name,int district_id,LPCTSTR district_name)
	{
		m_nIndex = index;
		AddItem(new CIntItem(region_id));
		AddItem(new CTextItem(region_name));
		AddItem(new CIntItem(district_id));
		AddItem(new CTextItem(district_name));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


// CMachineReportDataRec

class CMachineReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CMachineReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CMachineReportDataRec(UINT index,int region_id,LPCTSTR region_name,int district_id,LPCTSTR district_name,int machine_id,LPCTSTR contractor)
	{
		m_nIndex = index;
		AddItem(new CIntItem(region_id));
		AddItem(new CTextItem(region_name));
		AddItem(new CIntItem(district_id));
		AddItem(new CTextItem(district_name));
		AddItem(new CIntItem(machine_id));
		AddItem(new CTextItem(contractor));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

// CMachineRegReportDataRec

class CMachineRegReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CMachineRegReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
	}

	CMachineRegReportDataRec(UINT index)
	{
		m_nIndex = index;
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
	}

	CMachineRegReportDataRec(UINT index,int machine_id,LPCTSTR contractor)
	{
		m_nIndex = index;
		AddItem(new CIntItem(machine_id));
		AddItem(new CTextItem(contractor));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


// CTraktReportDataRec

class CTraktReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CTraktReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CTraktReportDataRec(UINT index,int region_id,int district_id,int machine_id,int trakt_num,
											LPCTSTR type,LPCTSTR origin,LPCTSTR date,LPCTSTR entered_as)
	{
		m_nIndex = index;
		AddItem(new CIntItem(region_id));
		AddItem(new CIntItem(district_id));
		AddItem(new CIntItem(machine_id));
		AddItem(new CIntItem(trakt_num));
		AddItem(new CTextItem(type));
		AddItem(new CTextItem(origin));
		AddItem(new CTextItem(date));
		AddItem(new CTextItem(entered_as));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


// CCompartReportDataRec

class CCompartReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CCompartReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CCompartReportDataRec(UINT index,int region_id,int district_id,int machine_id,LPCTSTR trakt_num,LPCTSTR type,LPCTSTR origin,int compart)
	{
		m_nIndex = index;
		AddItem(new CIntItem(region_id));
		AddItem(new CIntItem(district_id));
		AddItem(new CIntItem(machine_id));
		AddItem(new CTextItem(trakt_num));
		AddItem(new CTextItem(type));
		AddItem(new CTextItem(origin));
		AddItem(new CIntItem(compart));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};





class CDiCupDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
	vecRegionData m_vecRegionData;
	vecDistrictData m_vecDistrictData;
	vecMachineData m_vecMachineData;
protected:
	void getSQLMachineQuestion(LPTSTR);
	void getSQLTraktQuestion(LPTSTR);
public:
	CDiCupDB();
	CDiCupDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CDiCupDB(DB_CONNECTION_DATA &db_connection);

	BOOL getRegions(vecRegionData&);
	
	BOOL getDistricts(vecDistrictData&);
	
	BOOL getMachines(vecMachineData&);
	BOOL isThereAnyMachineData(void);
	BOOL addMachine(MACHINE_DATA &);
	BOOL updMachine(MACHINE_DATA &);
	// Delete machine and ALL trakt(s) associated
	BOOL delMachine(MACHINE_DATA &);

	BOOL getTrakts(vecTraktData&);
	BOOL isThereAnyTraktData(void);
	BOOL addTrakt(TRAKT_DATA &);
	BOOL updTrakt(TRAKT_DATA &,TRAKT_IDENTIFER &);
	// Delete trakt and ALL associated Copmpartments and Plots
	BOOL delTrakt(TRAKT_DATA &);
	BOOL existTrakt(TRAKT_IDENTIFER rec);

	// Machine regiter
	BOOL getMachineReg(vecMachineRegData&);
	BOOL getMachineInRegByNumber(MACHINE_REG_DATA &);
	BOOL isThereAnyMachineRegData(void);
	BOOL addMachineReg(MACHINE_REG_DATA &);
	BOOL updMachineReg(MACHINE_REG_DATA &);
	// Delete machine and ALL trakt(s) associated
	BOOL delMachineReg(MACHINE_REG_DATA &);

};


#endif

