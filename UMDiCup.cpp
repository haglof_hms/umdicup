// UMDataBase.cpp : Defines the initialization routines for the DLL.
//
#include "StdAfx.h"
#include "UMDiCup.h"
#include "Resource.h"

//#include "MDIDiCupRegionFrame.h"
//#include "MDIDiCupDistrictFrame.h"
//#include "MDIDiCupMachineFrame.h"
#include "MDIDiCupTraktFrame.h"
//#include "MDIDiCupActiveMachineFrame.h"

//#include "MDIDiCupRegionFormView.h"
//#include "MDIDiCupDistrictFormView.h"
//#include "MDIDiCupMachinesFormView.h"
#include "MDIDiCupTraktFormView.h"
//#include "MDIDiCupActiveMachineFormView.h"

//#include "DiCupRegionSelectList.h"
//#include "DiCupDistrictSelectList.h"
//#include "DiCupMachineSelectList.h"
#include "DiCupTraktSelectList.h"


#include "UMDiCupGenerics.h"
#include "AlterTables.h"

static AFX_EXTENSION_MODULE UMDiCupDLL = { NULL, NULL };

HINSTANCE hInst = NULL;

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);
extern "C" void AFX_EXT_API DoAlterTables(LPCTSTR);
extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMDiCup.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMDiCupDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMEstimate.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMDiCupDLL);
	}
	hInst = hInstance;
	return 1;   // ok
}

// Initialize the DLL, register the classes etc
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &info)
{
	new CDynLinkLibrary(UMDiCupDLL);
	CString sReportSettingsFN;
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	CString sPath;
	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function; 051214 p�d
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	// Form view to enter data

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW_UMDICUP_TRAKT,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIDiCupTraktFrame),
			RUNTIME_CLASS(CMDIDiCupTraktFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW_UMDICUP_TRAKT,suite,sLangFN,TRUE));

/*	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW12,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIDiCupActiveMachineFrame),
			RUNTIME_CLASS(CMDIDiCupActiveMachineFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW12,suite,sLangFN,TRUE));*/

	/*app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW6,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CDiCupRegionsSelectionListFrame),
			RUNTIME_CLASS(CDiCupRegionSelectList)));*/
	
	/*app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW7,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CDiCupDistrictSelectionListFrame),
			RUNTIME_CLASS(CDiCupDistrictSelectList)));*/

	/*app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW8,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CDiCupMachineSelectListFrame),
			RUNTIME_CLASS(CDiCupMachineSelectList)));*/

	app->AddDocTemplate(new CMultiDocTemplate(IDD_TRAKT_SELECT_LIST,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CDiCupTraktSelectListFrame),
			RUNTIME_CLASS(CDiCupTraktSelectList)));

	// Get version information; 060803 p�d
	LPCTSTR VER_NUMBER		= _T("FileVersion");
	LPCTSTR VER_COMPANY		= _T("CompanyName");
	LPCTSTR VER_COPYRIGHT	= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);

	info.push_back(INFO_TABLE(-999,2 /* User module */,
									sLangFN.GetString(),
									sVersion.GetString(),
									sCopyright.GetString(),
									sCompany.GetString()));

	// Check if there's a connection set; 070329 p�d
	if (getIsDBConSet() == 1)
	{

		// Check if exists/create report settings file; 061023 p�d
		sReportSettingsFN.Format(_T("%s%s"),getProgDir(),REPORT_SETTINGS_FILE);
		if (!fileExists(sReportSettingsFN))
		{
			FILE *fp;
			if ((fp = _tfopen(sReportSettingsFN,_T("wt"))) != NULL)
			{
				fclose(fp);
			}
		}

		// Check if exists/create report settings file; 061023 p�d
		sReportSettingsFN.Format(_T("%s%s"),getProgDir(),REPORT_SETTINGS2_FILE);
		if (!fileExists(sReportSettingsFN))
		{
			FILE *fp;
			if ((fp = _tfopen(sReportSettingsFN,_T("wt"))) != NULL)
			{
				fclose(fp);
			}
		}

		// Check if exists/create report settings file; 070925 p�d
		sReportSettingsFN.Format(_T("%s%s"),getProgDir(),REPORT_SETTINGS3_FILE);
		if (!fileExists(sReportSettingsFN))
		{
			FILE *fp;
			if ((fp = _tfopen(sReportSettingsFN,_T("wt"))) != NULL)
			{
				fclose(fp);
			}
		}

		sPath.Format(_T("%s\\%s\\%s"),getProgDir(),SUBDIR_SCRIPTS,DICUP_TABLES);
		runSQLScriptFile(sPath,TBL_REGION);
		
		sPath.Format(_T("%s\\%s\\%s"),getProgDir(),SUBDIR_SCRIPTS,DICUP_MACHINES_TABLE);
		runSQLScriptFile(sPath,TBL_ACTIVE_MACHINE);
	}

	DoAlterTables(_T(""));	// Empty arg = use default database;

}

void AFX_EXT_API DoAlterTables(LPCTSTR db_name)
{
	vecScriptFiles vec;
	if (getIsDBConSet() == 1)
	{
		// Alter tables
		//vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable2,db_name));
		//runSQLScriptFileEx1(vec,Scripts::TBL_ALTER);
		//vec.clear();
	}	// if (getIsDBConSet() == 1)
}	


