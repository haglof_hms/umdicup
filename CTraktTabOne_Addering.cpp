#include "stdafx.h"
#include "CTraktTabOne_Addering.h"

#include "ResLangFileReader.h"
#include "UMDiCupGenerics.h"
//#include ".\dicuptraktpages.h"

IMPLEMENT_DYNCREATE(CTraktPageOne_Addering, CXTResizeFormView)
BEGIN_MESSAGE_MAP(CTraktPageOne_Addering, CXTResizeFormView)
END_MESSAGE_MAP()

CTraktPageOne_Addering::CTraktPageOne_Addering()
	: CXTResizeFormView(CTraktPageOne_Addering::IDD)
{
	m_bInitialized=FALSE;
}
CTraktPageOne_Addering::~CTraktPageOne_Addering()
{
}

void CTraktPageOne_Addering::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE1_NATURTRAD,m_wndEdit_Trakt_Page1_NaturTrad);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE1_FRAMTIDSTRAD,m_wndEdit_Trakt_Page1_FramtidsTrad);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE1_FARSKAHOGSTUBBAR,m_wndEdit_Trakt_Page1_Hogstubbar);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE1_SPARADAREAL,m_wndEdit_Trakt_Page1_SparadAreal);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE1_SPARADVOLYM,m_wndEdit_Trakt_Page1_SparadVolym);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE1_KVARGLOMTVIRKE,m_wndEdit_Trakt_Page1_KvarVirke);
					 
	
}

void CTraktPageOne_Addering::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

		m_wndEdit_Trakt_Page1_NaturTrad.SetEnabledColor(BLACK, WHITE );
		m_wndEdit_Trakt_Page1_NaturTrad.SetDisabledColor(BLACK, INFOBK );
		m_wndEdit_Trakt_Page1_NaturTrad.SetAsNumeric();
		m_wndEdit_Trakt_Page1_NaturTrad.SetReadOnly( TRUE );

		m_wndEdit_Trakt_Page1_FramtidsTrad.SetEnabledColor(BLACK, WHITE );
		m_wndEdit_Trakt_Page1_FramtidsTrad.SetDisabledColor(BLACK, INFOBK );
		m_wndEdit_Trakt_Page1_FramtidsTrad.SetReadOnly( TRUE );
		m_wndEdit_Trakt_Page1_FramtidsTrad.SetAsNumeric();

		m_wndEdit_Trakt_Page1_Hogstubbar.SetEnabledColor(BLACK, WHITE );
		m_wndEdit_Trakt_Page1_Hogstubbar.SetDisabledColor(BLACK, INFOBK );
		m_wndEdit_Trakt_Page1_Hogstubbar.SetReadOnly( TRUE );
		m_wndEdit_Trakt_Page1_Hogstubbar.SetAsNumeric();

		m_wndEdit_Trakt_Page1_SparadAreal.SetEnabledColor(BLACK, WHITE );
		m_wndEdit_Trakt_Page1_SparadAreal.SetDisabledColor(BLACK, INFOBK );
		m_wndEdit_Trakt_Page1_SparadAreal.SetReadOnly( TRUE );
		m_wndEdit_Trakt_Page1_SparadAreal.SetAsNumeric();

		m_wndEdit_Trakt_Page1_SparadVolym.SetEnabledColor(BLACK, WHITE );
		m_wndEdit_Trakt_Page1_SparadVolym.SetDisabledColor(BLACK, INFOBK );
		m_wndEdit_Trakt_Page1_SparadVolym.SetReadOnly( TRUE );
		m_wndEdit_Trakt_Page1_SparadVolym.SetAsNumeric();

		m_wndEdit_Trakt_Page1_KvarVirke.SetEnabledColor(BLACK, WHITE );
		m_wndEdit_Trakt_Page1_KvarVirke.SetDisabledColor(BLACK, INFOBK );
		m_wndEdit_Trakt_Page1_KvarVirke.SetReadOnly( TRUE );
		m_wndEdit_Trakt_Page1_KvarVirke.SetAsNumeric();

		clearAll();		
		resetIsDirty();
		m_bInitialized = TRUE;
	}
}

void CTraktPageOne_Addering::clearAll(void)
{
	m_wndEdit_Trakt_Page1_NaturTrad.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page1_FramtidsTrad.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page1_Hogstubbar.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page1_SparadAreal.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page1_SparadVolym.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page1_KvarVirke.SetWindowText(_T(""));

}

void CTraktPageOne_Addering::setAllReadOnly(BOOL ro1)
{
	m_wndEdit_Trakt_Page1_NaturTrad.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page1_FramtidsTrad.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page1_Hogstubbar.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page1_SparadAreal.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page1_SparadVolym.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page1_KvarVirke.SetReadOnly( ro1 );
}

void CTraktPageOne_Addering::setEnabled(BOOL enabled)
{
	m_wndEdit_Trakt_Page1_NaturTrad.EnableWindow( enabled );
	m_wndEdit_Trakt_Page1_FramtidsTrad.EnableWindow( enabled );
	m_wndEdit_Trakt_Page1_Hogstubbar.EnableWindow( enabled );
	m_wndEdit_Trakt_Page1_SparadAreal.EnableWindow( enabled );
	m_wndEdit_Trakt_Page1_SparadVolym.EnableWindow( enabled );
	m_wndEdit_Trakt_Page1_KvarVirke.EnableWindow( enabled );

	setAllReadOnly(!enabled);	

}

void CTraktPageOne_Addering::populateData(TRAKT_DATA data)
{
	CString s_Data=_T("");
	setAllReadOnly(FALSE);
	m_wndEdit_Trakt_Page1_NaturTrad.setInt(data.m_n_naturvardestradSum);
	m_wndEdit_Trakt_Page1_FramtidsTrad.setInt(data.m_n_framtidstradSum);
	m_wndEdit_Trakt_Page1_Hogstubbar.setInt(data.m_n_farskaHogstubbarSum);
	m_wndEdit_Trakt_Page1_SparadAreal.setFloat(data.m_f_sparadArealSum,1);
	m_wndEdit_Trakt_Page1_SparadVolym.setInt(data.m_n_sparadVolymSum);
	m_wndEdit_Trakt_Page1_KvarVirke.setInt(data.m_n_kvarglomtVirkeSum);	
	resetIsDirty();
}

void CTraktPageOne_Addering::getEnteredData(TRAKT_DATA *trakt)
{	
	trakt->m_n_naturvardestradSum=m_wndEdit_Trakt_Page1_NaturTrad.getInt();
	trakt->m_n_framtidstradSum=m_wndEdit_Trakt_Page1_FramtidsTrad.getInt();
	trakt->m_n_farskaHogstubbarSum=m_wndEdit_Trakt_Page1_Hogstubbar.getInt();
	trakt->m_f_sparadArealSum=m_wndEdit_Trakt_Page1_SparadAreal.getFloat();
	trakt->m_n_sparadVolymSum=m_wndEdit_Trakt_Page1_SparadVolym.getInt();
	trakt->m_n_kvarglomtVirkeSum=m_wndEdit_Trakt_Page1_KvarVirke.getInt();
}

BOOL CTraktPageOne_Addering::getIsDirty(void)
{
	if (m_wndEdit_Trakt_Page1_NaturTrad.isDirty() ||
	m_wndEdit_Trakt_Page1_FramtidsTrad.isDirty() ||
	m_wndEdit_Trakt_Page1_Hogstubbar.isDirty() ||
	m_wndEdit_Trakt_Page1_SparadAreal.isDirty() ||
	m_wndEdit_Trakt_Page1_SparadVolym.isDirty() ||
	m_wndEdit_Trakt_Page1_KvarVirke.isDirty())
	{
		return TRUE;
	}
	return FALSE;
}

void CTraktPageOne_Addering::resetIsDirty(void)
{
	m_wndEdit_Trakt_Page1_NaturTrad.resetIsDirty();
	m_wndEdit_Trakt_Page1_FramtidsTrad.resetIsDirty();
	m_wndEdit_Trakt_Page1_Hogstubbar.resetIsDirty();
	m_wndEdit_Trakt_Page1_SparadAreal.resetIsDirty();
	m_wndEdit_Trakt_Page1_SparadVolym.resetIsDirty();
	m_wndEdit_Trakt_Page1_KvarVirke.resetIsDirty();
}