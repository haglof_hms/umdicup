#pragma once

#include "UMDiCupDB.h"
#include "Resource.h"

class CTraktPageTwo_Korskador : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTraktPageTwo_Korskador)

    BOOL m_bInitialized;
	CString m_sLangFN;

	CMyExtEdit m_wndEdit_Trakt_Page2_ForebKorskador;
	CMyExtEdit m_wndEdit_Trakt_Page2_KommentarKorskador;

	CMyComboBox m_wndCBox_Trakt_Page2_Risade;
	CMyComboBox m_wndCBox_Trakt_Page2_Hansynskr;
	CMyComboBox m_wndCBox_Trakt_Page2_MindreAllvKorskada;
	CMyComboBox m_wndCBox_Trakt_Page2_MindreAllvKorskadaTyp;
	CMyComboBox m_wndCBox_Trakt_Page2_AllvKorskada;
	CMyComboBox m_wndCBox_Trakt_Page2_AllvKorskadaTyp;
	CMyComboBox m_wndCBox_Trakt_Page2_ForebKorskador;
	CMyComboBox m_wndCBox_Trakt_Page2_Basvag;
	CMyComboBox m_wndCBox_Trakt_Page2_Vagdiken;

	vector<TRAKT_VAL_DATA> vec_Trakt_Risade;
	vector<TRAKT_VAL_DATA> vec_Trakt_Hansynskr;
	vector<TRAKT_VAL_DATA> vec_Trakt_MindreAllvKorskada;
	vector<TRAKT_VAL_DATA> vec_Trakt_MindreAllvKorskadaTyp;
	vector<TRAKT_VAL_DATA> vec_Trakt_AllvKorskada;
	vector<TRAKT_VAL_DATA> vec_Trakt_AllvKorskadaTyp;
	vector<TRAKT_VAL_DATA> vec_Trakt_ForebKorskador;
	vector<TRAKT_VAL_DATA> vec_Trakt_Basvag;
	vector<TRAKT_VAL_DATA> vec_Trakt_Vagdiken;
private:
	void setTraktValArrays();
	void setAllReadOnly(BOOL ro1);
public:
	void setEnabled(BOOL enabled);
	void clearAll(void);
	void resetIsDirty(void);
	void populateData(TRAKT_DATA data);
	BOOL getIsDirty(void);
	void getEnteredData(TRAKT_DATA *trakt);
	virtual void OnInitialUpdate();

	enum { IDD = IDD_FORMVIEW_UMDICUP_TRAKT_PAGE2 };
	CTraktPageTwo_Korskador();           // protected constructor used by dynamic creation
	virtual ~CTraktPageTwo_Korskador();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()	
public:
	afx_msg void OnCbnSelchangeComboTraktPage2Mindreallvkorskada();
	afx_msg void OnCbnSelchangeComboTraktPage2Allvkorskada();
};