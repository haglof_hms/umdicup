#pragma once

#include "UMDiCupDB.h"

#include "Resource.h"

// CComfirmationDlg dialog

class CComfirmationDlg : public CXTPDialog
{
	DECLARE_DYNAMIC(CComfirmationDlg)

	CString m_sLangAbbrev;
	CString m_sLangFN;

public:
	CComfirmationDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CComfirmationDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

	void setRegion(int region)
	{
		CComfirmationDlg::m_nRegionID = region;
	}
	void setDistrict(int dist)
	{
		CComfirmationDlg::m_nDistrictID = dist;
	}
	void setMachine(int machine,LPCTSTR contr)
	{
		m_nMachineID = machine;
		m_sContractor = contr;
	}
	void setTrakt(CXmlReader *Trakt)
	{
		m_nTrakt=Trakt->m_n_traktnr;
		m_nType=Trakt->m_n_typAvUppfoljning;
		m_nOrigin=Trakt->m_n_ursprung;

		m_recTraktData=TRAKT_DATA(Trakt->m_n_typAvUppfoljning,
			Trakt->m_n_rg,
			Trakt->m_n_di,
			Trakt->m_n_ursprung,
			Trakt->m_n_traktnr,
			Trakt->m_n_maskinlagnr,

			Trakt->m_s_traktnamn,
			Trakt->m_n_avvEnlRus,
			Trakt->m_n_finnsMiljorapport,
			Trakt->m_f_traktareal,
			Trakt->m_f_avverkadVolym,	
			Trakt->m_s_maskinlagnamn,
			Trakt->m_s_datumInv,
			Trakt->m_s_datumSlutfordAvv,
			Trakt->m_n_utfortGrotsk,
			Trakt->m_s_datumSlutfordGrotsk,
			Trakt->m_n_naturvardestradSum,
			Trakt->m_n_framtidstradSum,
			Trakt->m_n_farskaHogstubbarSum,
			Trakt->m_f_sparadArealSum,
			Trakt->m_n_sparadVolymSum,
			Trakt->m_n_kvarglomtVirkeSum,
			Trakt->m_n_risadeBasstrak,
			Trakt->m_n_hansynskrBiotoper,
			Trakt->m_n_mindreAllvKorskada,
			Trakt->m_n_mindreAllvKorskadaTyp,
			Trakt->m_n_allvarligKorskada,
			Trakt->m_n_allvarligKorskadaTyp,
			Trakt->m_n_forebyggtKorskador,
			Trakt->m_s_forebyggtKorskadorKommentar,
			Trakt->m_n_basvagUtanforTrakt,
			Trakt->m_s_kommentarAvKorskador,
			Trakt->m_n_skadadeVagdiken,
			Trakt->m_n_parallellitet,
			Trakt->m_n_hogarnasBeskaffenhet,
			Trakt->m_n_placeringAvHogar,
			Trakt->m_n_innehallHogar,
			Trakt->m_n_kvalitetPaValtor,
			Trakt->m_s_kommentarPaGrottillredning,
			Trakt->m_n_myr,
			Trakt->m_n_hallmark,
			Trakt->m_n_omrSomLamnOrorda,
			Trakt->m_n_objektSomSkallSkotas,
			Trakt->m_n_kulturminnen,
			Trakt->m_n_fornminnen,
			Trakt->m_n_skyddszoner,
			Trakt->m_n_naturvardestrad,
			Trakt->m_n_framtidstrad,
			Trakt->m_n_farskaHogstubbar,
			Trakt->m_n_dodaTrad,
			Trakt->m_n_kalytebegransning,
			Trakt->m_n_nedskrapning,
			Trakt->m_n_avverkningstidpunkt,
			Trakt->m_n_upplevelsehansyn,
			Trakt->m_n_teknEkonimp,
			Trakt->m_s_ovrigHansyn,
			Trakt->m_s_ovrigaKommentarerNaturOchMiljo,
			Trakt->m_n_baskrTraktplOK,
			Trakt->m_s_baskrTraktplBrister,
			Trakt->m_s_avvikandeArbUtifranTraktdirektivet,
			Trakt->m_s_hansynTillEfterkommandeAtgarder,
			Trakt->m_n_stubbhojd,
			Trakt->m_n_klaratStrategiskaMal,
			Trakt->m_s_forbattringsforslag,0);

	}

	void setDataFileName(LPCTSTR fn)
	{
		m_sDataFileName = fn;
	}

	void getMachineData(MACHINE_DATA &rec)
	{
		rec = m_recMachineData;
	}
	void getTraktData(TRAKT_DATA &rec)
	{
	
		rec = m_recTraktData;
	}

protected:
	CComboBox m_wndCBox1;
	CComboBox m_wndCBox2;

	CButton m_wndBtn1;
	CButton m_wndBtn2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;

	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl10;
	CMyExtStatic m_wndLbl11;
	CMyExtStatic m_wndLbl12;
	CMyExtStatic m_wndLbl13;
	CMyExtStatic m_wndLbl14;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	
	CXTButton m_wndListRegionDistBtn;
	CXTButton m_wndListMachineBtn;
	CXTButton m_wndListTraktBtn;

	CString m_sErrCap;
	CString m_sErrMsg;
	CString	m_sErrMsg1;
	CString m_sErrMsg2;
	CString m_sCaption;
	CString m_sMachineNum;
	CString m_sContractCap;

	vecDistrictData m_vecDistrictData;
	vecMachineData m_vecMachineData;
	vecMachineRegData m_vecMachineRegData;
	vecTraktData m_vecTraktData;

	CString m_sDataFileName;

	// Set STATIC datammembers to hold information on
	// PRIMARY Keys
	static int m_nRegionID;
	static int m_nDistrictID;
	int m_nMachineID;
	CString m_sContractor;
	int m_nTrakt;
	int m_nType;
	CString m_sType;
	int m_nOrigin;
	CString m_sOriginName;


	MACHINE_DATA m_recMachineData;
	TRAKT_DATA m_recTraktData;

	void setLanguage(void);

	void setRegionAndDistrictInfo(void);
	void setMachineInfo(void);
	void setTraktInfo(void);

	int getMachineID_setByUser(void);
	int getTraktID_setByUser(void);
	CString getContractor_setByUser(void);
	int getType_setByUser(void);
	CString getTypeName_setByUser(void);
	int getOrigin_setByUser(void);
	CString getOriginName_setByUser(void);

	BOOL isMachineNumOK(void);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

protected:
	//{{AFX_VIRTUAL(CComfirmationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CComfirmationDlg)
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
};
