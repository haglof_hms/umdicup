#include "StdAfx.h"
#include "XmlReader.h"
#include "xmlParser.h"
//#include "AddDataToDataBase.h"
#include "ResLangFileReader.h"


//#4031 20140429 J� xml parser
CXmlReader::CXmlReader(void)
{
		m_n_typAvUppfoljning		= -1;
		m_n_rg						= -1;
		m_n_di						= -1;
		m_n_ursprung				= -1;
		m_n_traktnr					= -1;
		m_s_traktnamn				=_T("");
		m_n_avvEnlRus				= -1;
		m_n_finnsMiljorapport		= -1;
		m_f_traktareal				= 0.0;
		m_f_avverkadVolym			= 0.0;
		m_n_maskinlagnr				=-1;
		m_s_maskinlagnamn						=_T("");
		m_s_datumInv							=_T("");
		m_s_datumSlutfordAvv					=_T("");
		m_n_utfortGrotsk			= -1;
		m_s_datumSlutfordGrotsk					=_T("");
		m_n_naturvardestradSum		= 0;
		m_n_framtidstradSum			= 0;
		m_n_farskaHogstubbarSum		= 0;
		m_f_sparadArealSum			= 0.0;
		m_n_sparadVolymSum			= 0;
		m_n_kvarglomtVirkeSum		= 0;
		m_n_risadeBasstrak			= -1;
		m_n_hansynskrBiotoper		= -1;
		m_n_mindreAllvKorskada		= -1;
		m_n_mindreAllvKorskadaTyp	= -1;
		m_n_allvarligKorskada		= -1;
		m_n_allvarligKorskadaTyp	= -1;
		m_n_forebyggtKorskador		= -1;
		m_s_forebyggtKorskadorKommentar			=_T("");
		m_n_basvagUtanforTrakt		= -1;
		m_s_kommentarAvKorskador				=_T("");
		m_n_skadadeVagdiken			= -1;
		m_n_parallellitet			= -1;
		m_n_hogarnasBeskaffenhet	= -1;
		m_n_placeringAvHogar		= -1;
		m_n_innehallHogar			= -1;
		m_n_kvalitetPaValtor		= -1;
		m_s_kommentarPaGrottillredning			=_T("");
		m_n_myr						= -1;
		m_n_hallmark				= -1;
		m_n_omrSomLamnOrorda		= -1;
		m_n_objektSomSkallSkotas	= -1;
		m_n_kulturminnen			= -1;
		m_n_fornminnen				= -1;
		m_n_skyddszoner				= -1,
		m_n_dodaTrad				= -1,
		m_n_kalytebegransning		= -1;
		m_n_nedskrapning			= -1;
		m_n_avverkningstidpunkt		= -1;
		m_n_upplevelsehansyn		= -1;
		m_n_teknEkonimp				= -1;
		m_s_ovrigHansyn							=_T("");
		m_s_ovrigaKommentarerNaturOchMiljo		=_T("");
		m_n_baskrTraktplOK			= -1;
		m_s_baskrTraktplBrister					=_T("");
		m_s_avvikandeArbUtifranTraktdirektivet	=_T("");
		m_s_hansynTillEfterkommandeAtgarder		=_T("");
		m_n_stubbhojd				= 0;
		m_n_klaratStrategiskaMal	= -1;
		m_s_forbattringsforslag					=_T("");



	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
}



int CXmlReader::OpenFile(char *filename)
{
	int n_Ret=RETURN_ERR_FILEOPEN;
	XMLNode xNode;
	//n_Ret=HXL_CheckXmlVer(filename);
	//if(n_Ret==1)
	//{
		xNode = XMLNode::openFileHelper(filename,XML_TAG_TRAKT_DICUPXML);
		n_Ret=HXL_HandleHxlFile(xNode);
	//}
	//else
	//{		
	//}
	return n_Ret;
}

int CXmlReader::HXL_HandleHxlFile(XMLNode xNode)
{
	int n_Ret=RETURN_ERR_FILEOPEN;

	if(xNode.isEmpty() != 1 || xNode.nChildNode()>0)
	{
		return HXL_RecTrakt(xNode);
	}
	else
	{

	}
	return n_Ret;
}


CString CXmlReader::formatToDateStr(CString str)
{
	CString strDate=_T("");

	if(str.GetLength()==8)
	{
		//strDate.Format(_T("%4.4s-%2.2s-%2.2s"),str.GetAt(0)+str.GetAt(1)+str.GetAt(2)+str.GetAt(3),str.GetAt(4)+str.GetAt(5),str.GetAt(6)+str.GetAt(7));
		//20150102
		str.Insert(4,_T("-"));
		//2015-0102
		str.Insert(7,_T("-"));
		strDate=str;
	//strDate.Format(_T("%c%c%c%c-%c%c-%c%c"),str.GetAt(0),str.GetAt(1),str.GetAt(2),str.GetAt(3),str.GetAt(4),str.GetAt(5),str.GetAt(6),str.GetAt(7));
	}
	return strDate;
}

//--------------------------------------------------------------
// L�s in traktvariabler
//--------------------------------------------------------------
int CXmlReader::HXL_RecTrakt(XMLNode xNode)
{
	int nDocs=0,numChild=0,n_Ret=-2,n_Keys=0;
	CString strTempDate=_T("");
	XMLNode xDoc,xDoc2;

	nDocs = xNode.nChildNode(XML_TAG_TRAKT_DICUPXML);
	if(nDocs==1)
	{
		xDoc2=xNode.getChildNode(XML_TAG_TRAKT_DICUPXML);
		nDocs = xDoc2.nChildNode(XML_TAG_NODE_TRAKT);
		if(nDocs==1)
		{
			xDoc=xDoc2.getChildNode(XML_TAG_NODE_TRAKT);
		}
		else
			return n_Ret;
	}
	else 
		return n_Ret;


	if(nDocs == 1)										
	{
		//xDoc=xNode.getChildNode(XML_TAG_NODE_TRAKT);


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_TypAvUppfoljning);
		if(numChild==1)
		{
			n_Keys++;
			m_n_typAvUppfoljning		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_TypAvUppfoljning).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Region);
		if(numChild==1)
		{
			n_Keys++;
			m_n_rg						= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Region).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Distrikt);
		if(numChild==1)
		{
			n_Keys++;
			m_n_di						= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Distrikt).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Ursprung);
		if(numChild==1)
		{
			n_Keys++;
			m_n_ursprung				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Ursprung).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Traktnr);
		if(numChild==1)
		{
			n_Keys++;
			m_n_traktnr					=_tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Traktnr).getText());	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Traktnamn);
		if(numChild==1)
		{
			m_s_traktnamn							=xDoc.getChildNode(XML_TAG_TRAKT_Traktnamn).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_AvvEnlRUS);
		if(numChild==1)
		{
			m_n_avvEnlRus				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_AvvEnlRUS).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_FinnsMiljorapport);
		if(numChild==1)
		{
			m_n_finnsMiljorapport		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_FinnsMiljorapport).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Traktareal);
		if(numChild==1)
		{
			m_f_traktareal				= my_atof(xDoc.getChildNode(XML_TAG_TRAKT_Traktareal).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_AvverkadVolym);
		if(numChild==1)
		{
			m_f_avverkadVolym			= my_atof(xDoc.getChildNode(XML_TAG_TRAKT_AvverkadVolym).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Maskinlagnr);
		if(numChild==1)
		{
			n_Keys++;
			m_n_maskinlagnr							=_tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Maskinlagnr).getText());	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Maskinlagnamn);
		if(numChild==1)
		{
			m_s_maskinlagnamn						=xDoc.getChildNode(XML_TAG_TRAKT_Maskinlagnamn).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_DatumInv);
		if(numChild==1)
		{
			strTempDate=xDoc.getChildNode(XML_TAG_TRAKT_DatumInv).getText();
			m_s_datumInv							=formatToDateStr(strTempDate);	
			
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_DatumSlutfordAvv);
		if(numChild==1)
		{
			strTempDate=xDoc.getChildNode(XML_TAG_TRAKT_DatumSlutfordAvv).getText();	
			m_s_datumSlutfordAvv					=formatToDateStr(strTempDate);	

		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_UtfortGrotsk);
		if(numChild==1)
		{
			m_n_utfortGrotsk			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_UtfortGrotsk).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_DatumSlutfordGrotsk);
		if(numChild==1)
		{
			strTempDate=xDoc.getChildNode(XML_TAG_TRAKT_DatumSlutfordGrotsk).getText();	
			m_s_datumSlutfordGrotsk					=formatToDateStr(strTempDate);	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_NaturvardestradSum);
		if(numChild==1)
		{
			m_n_naturvardestradSum		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_NaturvardestradSum).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_FramtidstradSum);
		if(numChild==1)
		{
			m_n_framtidstradSum			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_FramtidstradSum).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_FarskaHogstubbarSum);
		if(numChild==1)
		{
			m_n_farskaHogstubbarSum		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_FarskaHogstubbarSum).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_SparadArealSum);
		if(numChild==1)
		{
			m_f_sparadArealSum			= my_atof(xDoc.getChildNode(XML_TAG_TRAKT_SparadArealSum).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_SparadVolymSum);
		if(numChild==1)
		{
			m_n_sparadVolymSum			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_SparadVolymSum).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_KvarglomtVirkeSum);
		if(numChild==1)
		{
			m_n_kvarglomtVirkeSum		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_KvarglomtVirkeSum).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_RisadeBasstrak);
		if(numChild==1)
		{
			m_n_risadeBasstrak			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_RisadeBasstrak).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_HansynskrBiotoper);
		if(numChild==1)
		{
			m_n_hansynskrBiotoper		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_HansynskrBiotoper).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_MindreAllvKorskada);
		if(numChild==1)
		{
			m_n_mindreAllvKorskada		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_MindreAllvKorskada).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_MindreAllvKorskadaTyp);
		if(numChild==1)
		{
			m_n_mindreAllvKorskadaTyp	= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_MindreAllvKorskadaTyp).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_AllvarligKorskada);
		if(numChild==1)
		{
			m_n_allvarligKorskada		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_AllvarligKorskada).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_AllvarligKorskadaTyp);
		if(numChild==1)
		{
			m_n_allvarligKorskadaTyp	= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_AllvarligKorskadaTyp).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_ForebyggtKorskador);
		if(numChild==1)
		{
			m_n_forebyggtKorskador		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_ForebyggtKorskador).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_ForebyggtKorskadorKommentar);
		if(numChild==1)
		{
			m_s_forebyggtKorskadorKommentar			=xDoc.getChildNode(XML_TAG_TRAKT_ForebyggtKorskadorKommentar).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_BasvagUtanforTrakt);
		if(numChild==1)
		{
			m_n_basvagUtanforTrakt		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_BasvagUtanforTrakt).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_KommentarAvKorskador);
		if(numChild==1)
		{
			m_s_kommentarAvKorskador				=xDoc.getChildNode(XML_TAG_TRAKT_KommentarAvKorskador).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_SkadadeVagdiken);
		if(numChild==1)
		{
			m_n_skadadeVagdiken			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_SkadadeVagdiken).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Parallellitet);
		if(numChild==1)
		{
			m_n_parallellitet			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Parallellitet).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_HogarnasBeskaffenhet);
		if(numChild==1)
		{
			m_n_hogarnasBeskaffenhet	= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_HogarnasBeskaffenhet).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_PlaceringAvHogar);
		if(numChild==1)
		{
			m_n_placeringAvHogar		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_PlaceringAvHogar).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_InnehallHogar);
		if(numChild==1)
		{
			m_n_innehallHogar			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_InnehallHogar).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_KvalitetPaValtor);
		if(numChild==1)
		{
			m_n_kvalitetPaValtor		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_KvalitetPaValtor).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_KommentarPaGrottillredning);
		if(numChild==1)
		{
			m_s_kommentarPaGrottillredning			=xDoc.getChildNode(XML_TAG_TRAKT_KommentarPaGrottillredning).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Myr);
		if(numChild==1)
		{
			m_n_myr						= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Myr).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Hallmark);
		if(numChild==1)
		{
			m_n_hallmark				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Hallmark).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_OmrSomLamnOrorda);
		if(numChild==1)
		{
			m_n_omrSomLamnOrorda		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_OmrSomLamnOrorda).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_ObjektSomSkallSkotas);
		if(numChild==1)
		{
			m_n_objektSomSkallSkotas	= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_ObjektSomSkallSkotas).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Kulturminnen);
		if(numChild==1)
		{
			m_n_kulturminnen			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Kulturminnen).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Fornminnen);
		if(numChild==1)
		{
			m_n_fornminnen				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Fornminnen).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Skyddszoner);
		if(numChild==1)
		{
			m_n_skyddszoner				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Skyddszoner).getText());
		}
		

		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Naturvardestrad);
		if(numChild==1)
		{
			m_n_naturvardestrad				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Naturvardestrad).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Framtidstrad);
		if(numChild==1)
		{
			m_n_framtidstrad				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Framtidstrad).getText());
		}		
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_FarskaHogstubbar);
		if(numChild==1)
		{
			m_n_farskaHogstubbar				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_FarskaHogstubbar).getText());
		}				
		
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_DodaTrad);
		if(numChild==1)
		{
			m_n_dodaTrad				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_DodaTrad).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Kalytebegransning);
		if(numChild==1)
		{
			m_n_kalytebegransning		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Kalytebegransning).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Nedskrapning);
		if(numChild==1)
		{
			m_n_nedskrapning			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Nedskrapning).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Avverkningstidpunkt);
		if(numChild==1)
		{
			m_n_avverkningstidpunkt		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Avverkningstidpunkt).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Upplevelsehansyn);
		if(numChild==1)
		{
			m_n_upplevelsehansyn		= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Upplevelsehansyn).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_TeknEkonimp);
		if(numChild==1)
		{
			m_n_teknEkonimp				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_TeknEkonimp).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_OvrigHansyn);
		if(numChild==1)
		{
			m_s_ovrigHansyn							=xDoc.getChildNode(XML_TAG_TRAKT_OvrigHansyn).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_OvrigaKommentarerNaturOchMiljo);
		if(numChild==1)
		{
			m_s_ovrigaKommentarerNaturOchMiljo		=xDoc.getChildNode(XML_TAG_TRAKT_OvrigaKommentarerNaturOchMiljo).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_BaskrTraktplOK);
		if(numChild==1)
		{
			m_n_baskrTraktplOK			= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_BaskrTraktplOK).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_BaskrTraktplBrister);
		if(numChild==1)
		{
			m_s_baskrTraktplBrister					=xDoc.getChildNode(XML_TAG_TRAKT_BaskrTraktplBrister).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_AvvikandeArbUtifranTraktdirektivet);
		if(numChild==1)
		{
			m_s_avvikandeArbUtifranTraktdirektivet	=xDoc.getChildNode(XML_TAG_TRAKT_AvvikandeArbUtifranTraktdirektivet).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_HansynTillEfterkommandeAtgarder);
		if(numChild==1)
		{
			m_s_hansynTillEfterkommandeAtgarder		=xDoc.getChildNode(XML_TAG_TRAKT_HansynTillEfterkommandeAtgarder).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_AndelHogaStubbar);
		if(numChild==1)
		{
			m_n_stubbhojd				= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_AndelHogaStubbar).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_KlaratStrategiskaMal);
		if(numChild==1)
		{
			m_n_klaratStrategiskaMal	= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_KlaratStrategiskaMal).getText());
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Forbattringsforslag);
		if(numChild==1)
		{
			m_s_forbattringsforslag					=xDoc.getChildNode(XML_TAG_TRAKT_Forbattringsforslag).getText();	
		}
		if(n_Keys>=6)
			n_Ret=1;
	}
	else
	{
		n_Ret=RETURN_ERR_TRAKT;
	}
	return n_Ret;
}






