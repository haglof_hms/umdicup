#include "stdafx.h"
#include "CTraktTabThree_Grottillredning.h"

#include "ResLangFileReader.h"
#include "UMDiCupGenerics.h"
//#include ".\dicuptraktpages.h"
// -------------------------------------------------------------------------
//	Trakt Page 3 Grottillredning
// -------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CTraktPageThree_Grottillredning, CXTResizeFormView)
BEGIN_MESSAGE_MAP(CTraktPageThree_Grottillredning, CXTResizeFormView)
END_MESSAGE_MAP()




CTraktPageThree_Grottillredning::CTraktPageThree_Grottillredning()
	: CXTResizeFormView(CTraktPageThree_Grottillredning::IDD)
{
	m_bInitialized=FALSE;
}
CTraktPageThree_Grottillredning::~CTraktPageThree_Grottillredning()
{
}

void CTraktPageThree_Grottillredning::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)

	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE3_PARALLELLITET, m_wndCBox_Trakt_Page3_Parallellitet);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE3_HOGBESK, m_wndCBox_Trakt_Page3_HogBesk);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE3_HOGPLAC, m_wndCBox_Trakt_Page3_HogPlac);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE3_HOGINN, m_wndCBox_Trakt_Page3_HogInneh);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE3_VALTKVAL, m_wndCBox_Trakt_Page3_KvalValt);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE3_GROTKOMM, m_wndEdit_Trakt_Page3_KommentarGrottillredning);
	
}

void CTraktPageThree_Grottillredning::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
		m_wndCBox_Trakt_Page3_Parallellitet.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page3_Parallellitet.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page3_HogBesk.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page3_HogBesk.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page3_HogPlac.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page3_HogPlac.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page3_HogInneh.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page3_HogInneh.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page3_KvalValt.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page3_KvalValt.SetDisabledColor(BLACK,INFOBK);

		m_wndEdit_Trakt_Page3_KommentarGrottillredning.SetEnabledColor(BLACK, WHITE );
		m_wndEdit_Trakt_Page3_KommentarGrottillredning.SetDisabledColor(BLACK, INFOBK );

		clearAll();

		setTraktValArrays();
		resetIsDirty();
		m_bInitialized = TRUE;
	}
}

void CTraktPageThree_Grottillredning::clearAll(void)
{
	m_wndEdit_Trakt_Page3_KommentarGrottillredning.SetWindowText(_T(""));

	m_wndCBox_Trakt_Page3_Parallellitet.SetCurSel(-1);;
	m_wndCBox_Trakt_Page3_HogBesk.SetCurSel(-1);;
	m_wndCBox_Trakt_Page3_HogPlac.SetCurSel(-1);;
	m_wndCBox_Trakt_Page3_HogInneh.SetCurSel(-1);;
	m_wndCBox_Trakt_Page3_KvalValt.SetCurSel(-1);;
}

void CTraktPageThree_Grottillredning::setAllReadOnly(BOOL ro1)
{
	m_wndEdit_Trakt_Page3_KommentarGrottillredning.SetReadOnly( ro1 );

	m_wndCBox_Trakt_Page3_Parallellitet.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page3_HogBesk.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page3_HogPlac.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page3_HogInneh.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page3_KvalValt.SetReadOnly( ro1 );
}
void CTraktPageThree_Grottillredning::setEnabled(BOOL enabled)
{
	m_wndEdit_Trakt_Page3_KommentarGrottillredning.EnableWindow( enabled );

	m_wndCBox_Trakt_Page3_Parallellitet.EnableWindow( enabled );
	m_wndCBox_Trakt_Page3_HogBesk.EnableWindow( enabled );
	m_wndCBox_Trakt_Page3_HogPlac.EnableWindow( enabled );
	m_wndCBox_Trakt_Page3_HogInneh.EnableWindow( enabled );
	m_wndCBox_Trakt_Page3_KvalValt.EnableWindow( enabled );

	setAllReadOnly(!enabled);	
}


void CTraktPageThree_Grottillredning::populateData(TRAKT_DATA data)
{
	CString s_Data=_T("");

	s_Data.Format(_T("%d"),data.m_n_parallellitet);
	if(data.m_n_parallellitet==-1)
		m_wndCBox_Trakt_Page3_Parallellitet.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page3_Parallellitet.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_hogarnasBeskaffenhet);
	if(data.m_n_hogarnasBeskaffenhet==-1)
		m_wndCBox_Trakt_Page3_HogBesk.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page3_HogBesk.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_placeringAvHogar);
	if(data.m_n_placeringAvHogar==-1)
		m_wndCBox_Trakt_Page3_HogPlac.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page3_HogPlac.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_innehallHogar);
	if(data.m_n_innehallHogar==-1)
		m_wndCBox_Trakt_Page3_HogInneh.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page3_HogInneh.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_kvalitetPaValtor);
	if(data.m_n_kvalitetPaValtor==-1)
		m_wndCBox_Trakt_Page3_KvalValt.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page3_KvalValt.SelectString(0,s_Data);

	m_wndEdit_Trakt_Page3_KommentarGrottillredning.SetWindowText(data.m_s_kommentarPaGrottillredning);

	setAllReadOnly(FALSE);
	resetIsDirty();
}

void CTraktPageThree_Grottillredning::getEnteredData(TRAKT_DATA *trakt)
{	
	trakt->m_n_parallellitet= m_wndCBox_Trakt_Page3_Parallellitet.GetItemData(m_wndCBox_Trakt_Page3_Parallellitet.GetCurSel());
	trakt->m_n_hogarnasBeskaffenhet=m_wndCBox_Trakt_Page3_HogBesk.GetItemData(m_wndCBox_Trakt_Page3_HogBesk.GetCurSel());
	trakt->m_n_placeringAvHogar=m_wndCBox_Trakt_Page3_HogPlac.GetItemData(m_wndCBox_Trakt_Page3_HogPlac.GetCurSel());
	trakt->m_n_innehallHogar=m_wndCBox_Trakt_Page3_HogInneh.GetItemData(m_wndCBox_Trakt_Page3_HogInneh.GetCurSel());
	trakt->m_n_kvalitetPaValtor=m_wndCBox_Trakt_Page3_KvalValt.GetItemData(m_wndCBox_Trakt_Page3_KvalValt.GetCurSel());
	trakt->m_s_kommentarPaGrottillredning= m_wndEdit_Trakt_Page3_KommentarGrottillredning.getText();
}

BOOL CTraktPageThree_Grottillredning::getIsDirty(void)
{
	if (	m_wndCBox_Trakt_Page3_Parallellitet.isDirty() ||
		m_wndCBox_Trakt_Page3_HogBesk.isDirty() ||
		m_wndCBox_Trakt_Page3_HogPlac.isDirty() ||
		m_wndCBox_Trakt_Page3_HogInneh.isDirty() ||
		m_wndCBox_Trakt_Page3_KvalValt.isDirty() ||
		m_wndEdit_Trakt_Page3_KommentarGrottillredning.isDirty())
	{
		return TRUE;
	}
	return FALSE;
}

void CTraktPageThree_Grottillredning::resetIsDirty(void)
{
	m_wndCBox_Trakt_Page3_Parallellitet.resetIsDirty();
	m_wndCBox_Trakt_Page3_HogBesk.resetIsDirty();
	m_wndCBox_Trakt_Page3_HogPlac.resetIsDirty();
	m_wndCBox_Trakt_Page3_HogInneh.resetIsDirty();
	m_wndCBox_Trakt_Page3_KvalValt.resetIsDirty();
	m_wndEdit_Trakt_Page3_KommentarGrottillredning.resetIsDirty();
}

void CTraktPageThree_Grottillredning::setTraktValArrays()
{	
	TRAKT_VAL_DATA data;
	CString s_Data=_T("");
	UINT i=0;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			vec_Trakt_Page3_Parallellitet.clear();
			vec_Trakt_Page3_Parallellitet.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page3_Parallellitet.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7081)),xml->str(IDS_STRING7080)));
			vec_Trakt_Page3_Parallellitet.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7083)),xml->str(IDS_STRING7082)));
			vec_Trakt_Page3_Parallellitet.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7085)),xml->str(IDS_STRING7084)));
			vec_Trakt_Page3_Parallellitet.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7087)),xml->str(IDS_STRING7086)));

			vec_Trakt_Page3_HogBesk.clear();
			vec_Trakt_Page3_HogBesk.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page3_HogBesk.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7081)),xml->str(IDS_STRING7080)));
			vec_Trakt_Page3_HogBesk.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7083)),xml->str(IDS_STRING7082)));
			vec_Trakt_Page3_HogBesk.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7085)),xml->str(IDS_STRING7084)));
			vec_Trakt_Page3_HogBesk.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7087)),xml->str(IDS_STRING7086)));

			vec_Trakt_Page3_HogPlac.clear();
			vec_Trakt_Page3_HogPlac.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page3_HogPlac.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7081)),xml->str(IDS_STRING7080)));
			vec_Trakt_Page3_HogPlac.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7083)),xml->str(IDS_STRING7082)));
			vec_Trakt_Page3_HogPlac.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7085)),xml->str(IDS_STRING7084)));
			vec_Trakt_Page3_HogPlac.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7087)),xml->str(IDS_STRING7086)));

			vec_Trakt_Page3_HogInneh.clear();
			vec_Trakt_Page3_HogInneh.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page3_HogInneh.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7081)),xml->str(IDS_STRING7080)));
			vec_Trakt_Page3_HogInneh.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7085)),xml->str(IDS_STRING7084)));
			vec_Trakt_Page3_HogInneh.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7087)),xml->str(IDS_STRING7086)));

			vec_Trakt_Page3_KvalValt.clear();
			vec_Trakt_Page3_KvalValt.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page3_KvalValt.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7081)),xml->str(IDS_STRING7080)));
			vec_Trakt_Page3_KvalValt.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7085)),xml->str(IDS_STRING7084)));
			vec_Trakt_Page3_KvalValt.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7087)),xml->str(IDS_STRING7086)));
		}
	}

	for (i = 0;i < vec_Trakt_Page3_Parallellitet.size();i++)
	{
		data = vec_Trakt_Page3_Parallellitet[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page3_Parallellitet.AddString(s_Data);
		m_wndCBox_Trakt_Page3_Parallellitet.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page3_HogBesk.size();i++)
	{
		data = vec_Trakt_Page3_HogBesk[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page3_HogBesk.AddString(s_Data);
		m_wndCBox_Trakt_Page3_HogBesk.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page3_HogPlac.size();i++)
	{
		data = vec_Trakt_Page3_HogPlac[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page3_HogPlac.AddString(s_Data);
		m_wndCBox_Trakt_Page3_HogPlac.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page3_HogInneh.size();i++)
	{
		data = vec_Trakt_Page3_HogInneh[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page3_HogInneh.AddString(s_Data);
		m_wndCBox_Trakt_Page3_HogInneh.SetItemData(i, data.m_nID);		
	}


	for (i = 0;i < vec_Trakt_Page3_KvalValt.size();i++)
	{
		data = vec_Trakt_Page3_KvalValt[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page3_KvalValt.AddString(s_Data);
		m_wndCBox_Trakt_Page3_KvalValt.SetItemData(i, data.m_nID);		
	}
}