#pragma once

#include "UMDiCupDB.h"
#include "Resource.h"

class CTraktPageThree_Grottillredning : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTraktPageThree_Grottillredning)

    BOOL m_bInitialized;
	CString m_sLangFN;

	CMyComboBox m_wndCBox_Trakt_Page3_Parallellitet;
	CMyComboBox m_wndCBox_Trakt_Page3_HogBesk;
	CMyComboBox m_wndCBox_Trakt_Page3_HogPlac;
	CMyComboBox m_wndCBox_Trakt_Page3_HogInneh;
	CMyComboBox m_wndCBox_Trakt_Page3_KvalValt;

	CMyExtEdit m_wndEdit_Trakt_Page3_KommentarGrottillredning;

	vector<TRAKT_VAL_DATA> vec_Trakt_Page3_Parallellitet;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page3_HogBesk;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page3_HogPlac;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page3_HogInneh;
	vector<TRAKT_VAL_DATA> vec_Trakt_Page3_KvalValt;

private:
	void setTraktValArrays();
	void setAllReadOnly(BOOL ro1);	
public:
	void setEnabled(BOOL enabled);
	void clearAll(void);
	void resetIsDirty(void);
	void populateData(TRAKT_DATA data);
	BOOL getIsDirty(void);
	void getEnteredData(TRAKT_DATA *trakt);
	virtual void OnInitialUpdate();

	enum { IDD = IDD_FORMVIEW_UMDICUP_TRAKT_PAGE3 };
	CTraktPageThree_Grottillredning();           // protected constructor used by dynamic creation
	virtual ~CTraktPageThree_Grottillredning();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()	
};