#pragma once

#include "UMDiCupDB.h"
#include "Resource.h"

class CTraktPageOne_Addering : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTraktPageOne_Addering)

    BOOL m_bInitialized;
	CString m_sLangFN;

	CMyExtEdit m_wndEdit_Trakt_Page1_NaturTrad;
	CMyExtEdit m_wndEdit_Trakt_Page1_FramtidsTrad;
	CMyExtEdit m_wndEdit_Trakt_Page1_Hogstubbar;
	CMyExtEdit m_wndEdit_Trakt_Page1_SparadAreal;
	CMyExtEdit m_wndEdit_Trakt_Page1_SparadVolym;
	CMyExtEdit m_wndEdit_Trakt_Page1_KvarVirke;

private:	
	void setAllReadOnly(BOOL ro1);
public:	
	void setEnabled(BOOL enabled);
	void clearAll(void);
	void resetIsDirty(void);
	void populateData(TRAKT_DATA data);
	BOOL getIsDirty(void);
	void getEnteredData(TRAKT_DATA *trakt);
	virtual void OnInitialUpdate();

	enum { IDD = IDD_FORMVIEW_UMDICUP_TRAKT_PAGE1 };
	CTraktPageOne_Addering();           // protected constructor used by dynamic creation
	virtual ~CTraktPageOne_Addering();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()	
};
