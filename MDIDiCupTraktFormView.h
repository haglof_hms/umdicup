#pragma once

#include "UMDiCupDB.h"

#include "DatePickerCombo.h"

#include "Resource.h"





// CMDIDiCupTraktFormView form view

class CMDIDiCupTraktFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIDiCupTraktFormView)

protected:
	CMDIDiCupTraktFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIDiCupTraktFormView();

	//Variabler
	CMyComboBox m_wndCBox_Trakt_InvTyp;
	CMyComboBox m_wndCBox_Trakt_Region;	
	CMyComboBox m_wndCBox_Trakt_District;
	CMyComboBox m_wndCBox_Trakt_Ursprung;
	CMyExtEdit m_wndEdit_Trakt_Nr;
	CMyExtEdit m_wndEdit_Trakt_Maskin;

	CMyExtEdit m_wndEdit_Trakt_Entr;
	CMyExtEdit m_wndEdit_Trakt_Namn;
	CMyExtEdit m_wndEdit_Trakt_Areal;
	CMyExtEdit m_wndEdit_Trakt_Avv_Vol;

	CDateTimeCtrl m_wndDatePicker_Trakt_Datum_Inv;
	CString strDatePicker_Trakt_Datum_Inv;

	CDateTimeCtrl m_wndDatePicker_Trakt_Datum_Slutford_Avv;
	CString strDatePicker_Trakt_Datum_Slutford_Avv;

	CDateTimeCtrl m_wndDatePicker_Trakt_Datum_Slutford_Grot;
	CString strDatePicker_Trakt_Datum_Slutford_Grot;

	CMyComboBox m_wndCBox_Trakt_Avv_Enl_Rus;
	CMyComboBox m_wndCBox_Trakt_Finns_Miljorapp;
	CMyComboBox m_wndCBox_Trakt_Utford_Grot;


	// Vektorer f�r valbara variabler med f�rdefinerade listor
	vector<TRAKT_VAL_DATA> vec_Trakt_Avv_Enl_Rus;
	vector<TRAKT_VAL_DATA> vec_Trakt_Finns_Miljorapp;
	vector<TRAKT_VAL_DATA> vec_Trakt_Utford_Grot;
	void setTraktValArrays();
	
	int get_TraktAvvEnlRus_setByUser(void);
	int get_TraktFinnsMiljorapp_setByUser(void);
	int get_TraktUtfordGrot_setByUser(void);


	CMyExtStatic m_wndLb_Trakt_Region;
	CMyExtStatic m_wndLb_Trakt_Distrikt;
	CMyExtStatic m_wndLb_Trakt_Maskinnr;
	CMyExtStatic m_wndLb_Trakt_Entr;
	CMyExtStatic m_wndLb_Trakt_InvTyp;
	CMyExtStatic m_wndLb_Trakt_Ursprung;
	CMyExtStatic m_wndLb_Trakt_Nr;
	CMyExtStatic m_wndLb_Trakt_Namn;
	CMyExtStatic m_wndLb_Trakt_Areal;
	CMyExtStatic m_wndLb_Trakt_Datum_Inv;
	CMyExtStatic m_wndLb_Trakt_Datum_Slutford_Avv;
	CMyExtStatic m_wndLb_Trakt_Avv_Enl_Rus;
	CMyExtStatic m_wndLb_Trakt_Finns_Miljorapp;
	CMyExtStatic m_wndLb_Trakt_Avv_Vol;
	CMyExtStatic m_wndLb_Trakt_Utford_Grot;
	CMyExtStatic m_wndLb_Trakt_Datum_Slutford_Grot;
	
	




	CXTResizeGroupBox m_wndIDGroup;
	CXTResizeGroupBox m_wndTraktGroup;

	CXTButton m_wndListMachineBtn;

	vecRegionData m_vecRegionData;
	vecDistrictData m_vecDistrictData;
	vecMachineData m_vecMachineData;
	vecTraktData m_vecTraktData;
	CString m_sMachineIDInfo;
	UINT m_nDBIndex;

	MACHINE_DATA m_recNewMachine;
	TRAKT_DATA m_recNewTrakt;
	TRAKT_DATA m_recActiveTrakt;

	CString m_sLangFN;
	CString m_sErrCap;
	CString m_sErrMsg;
	CString m_sUpdateTraktMsg;
	CString m_sNotOkToSave;
	CString m_sSaveData;
	CString m_sTraktExists;
	CString m_sCantSaveTraktMsg;
	enumACTION m_enumAction;
	
	void setLanguage(void);

	void getRegionsFromDB(void);
	void getDistrictFromDB(void);
	void getMachineFromDB(void);
	void getTraktsFromDB(void);
	void setupTraktTabs(void);
	void populateData(UINT);

	void setNavigationButtons(BOOL,BOOL);

	void addTraktManually(void);
	void deleteTrakt(void);
	void addTrakt(void);

	void setDefaultTypeAndOrigin(void);

	void setEnabled(BOOL ro1);
	void clearAll(void);
	void clearAllTabData(void);
	void setEnabledTabData(BOOL enabled);
	void getEnteredData(void);
	int getType_setByUser(void);
	CString getTypeName_setByUser(void);
	int getOrigin_setByUser(void);
	CString getOriginName_setByUser(void);

	BOOL getDatesIsDirty();
	TRAKT_IDENTIFER	m_structTraktIdentifer;

	CString getRegionAndDistrictData(int region,int dist,DISTRICT_DATA &rec);
	CString getMachineEntr(int region,int dist,int machine);

	void addRegionsToCBox(void);
	CString getRegionSelected(int region_num);
	int getRegionID(void);
	void addDistrictsToCBox(void);
	CString getDistrictSelected(int region_num,int district_num);
	int getDistrictID(void);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CMyTabControl m_wndTabControl;
	void showTabData(TRAKT_DATA data);
	int checkOtherVariablesToSave();

public:
	enum { IDD = IDD_FORMVIEW_UMDICUP_TRAKT };
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BOOL getIsDirty(void);
	BOOL getTabPagesIsDirty(void);
	void resetTabPagesIsDirty(void);
	void getEnteredDataTabPages(TRAKT_DATA *trakt);

	void resetIsDirty(void);	// Set ALL edit boxes etc. to NOT DIRTY; 061113 p�d
	// Needs to be Public, to be visible in CMDIDiCupTraktFrame; 061017 p�d
	void saveTrakt(void);
	// Check if it's ok to save trakt; 061113 p�d
	BOOL isOKToSave(TRAKT_IDENTIFER *old_trakt_id);
	// Check if active data's chnged by user.
	// I.e. manually enterd data; 061113 p�d
	BOOL isDataChanged(void);

	void doPopulate(UINT);

	void resetTrakt(enumRESET reset);

	
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
public:
	virtual void OnInitialUpdate();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIDiCupRegionFormView)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClicked_Trakt_Maskin();	
	afx_msg void OnCbnSelchangeTraktRegion();
	CString m_Text;
};


