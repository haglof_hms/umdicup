// MDIDataBaseFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "MDIDiCupTraktFrame.h"
#include "MDIDiCupTraktFormView.h"

#include "ResLangFileReader.h"

#include "UMDiCupGenerics.h"


//#include "AddDataToDataBase.h"

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc::CMDIFrameDoc()
{
}

CMDIFrameDoc::~CMDIFrameDoc()
{
}

BOOL CMDIFrameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIFrameDoc commands




/////////////////////////////////////////////////////////////////////////////
// CMDIDiCupTraktFrame

IMPLEMENT_DYNCREATE(CMDIDiCupTraktFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIDiCupTraktFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIDiCupTraktFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIDiCupTraktFrame::CMDIDiCupTraktFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIDiCupTraktFrame::~CMDIDiCupTraktFrame()
{
}

void CMDIDiCupTraktFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_DICUP_TRAKT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIDiCupTraktFrame::OnClose(void)
{

	CMDIDiCupTraktFormView *pView = (CMDIDiCupTraktFormView *)getFormViewByID(IDD_FORMVIEW_UMDICUP_TRAKT);
	if (pView)
	{
		if (pView->getIsDirty())
		{
			//if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
			//{
				pView->saveTrakt();
			//}
		}
	}

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070905 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	CMDIChildWnd::OnClose();
}

int CMDIDiCupTraktFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	if (!isDBConnection(m_sLangFN))
		return -1;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sMsgCap = xml->str(IDS_STRING109);
			m_sMsg1 = xml->str(IDS_STRING511);
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CMDIDiCupTraktFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_DICUP_TRAKT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIDiCupTraktFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

}

BOOL CMDIDiCupTraktFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIDiCupTraktFrame diagnostics

#ifdef _DEBUG
void CMDIDiCupTraktFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIDiCupTraktFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIDiCupTraktFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_DICUP_TRAKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_DICUP_TRAKT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIDiCupTraktFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIDiCupTraktFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIDiCupTraktFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_TRAKT_SELECT_LIST,m_sLangFN);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}

// MY METHODS

// CMDIDiCupTraktFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CDiCupTraktSelectListFrame

IMPLEMENT_DYNCREATE(CDiCupTraktSelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CDiCupTraktSelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CDiCupTraktSelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CDiCupTraktSelectListFrame::CDiCupTraktSelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CDiCupTraktSelectListFrame::~CDiCupTraktSelectListFrame()
{
}

void CDiCupTraktSelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_DICUP_LIST_TRAKT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CDiCupTraktSelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CDiCupTraktSelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_DICUP_LIST_TRAKT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CDiCupTraktSelectListFrame::OnSetFocus(CWnd*)
{
}

BOOL CDiCupTraktSelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CDiCupTraktSelectListFrame diagnostics

#ifdef _DEBUG
void CDiCupTraktSelectListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CDiCupTraktSelectListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CDiCupTraktSelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_DICUP_LIST_TRAKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_DICUP_LIST_TRAKT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CDiCupTraktSelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CDiCupTraktSelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CDiCupTraktSelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

// MY METHODS


// CDiCupTraktSelectListFrame message handlers


