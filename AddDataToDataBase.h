#if !defined(AFX_ADDDATATODATABASE_H)
#define AFX_ADDDATATODATABASE_H

#include "StdAfx.h"
#include "XmlReader.h"
#include "DBBaseClass_SQLApi.h"	// ... in DBTransaction_lib
#include "Resource.h"



// Define name of DiCup data file reading dll; 061003 p�d
//#define DICUP_DLL_FILENAME	_T("DiCup_Dll.dll")


// Setup classes to be used to hold data from data files; 061003 p�d

// Structrure, for identifying Trakt, entered from a File; 061207 p�d
typedef struct _trakt_identifer_struct
{
	int nRegionID;
	int nDistrictID;
	int nMachineID;
	int nTraktNum;
	int nType;
	int nOrigin;

} TRAKT_IDENTIFER;


// Create my own CFileDialog 

class CMyFileDialog : public CFileDialog
{
	CString m_sTitle;
public:
	CMyFileDialog(LPCTSTR,BOOL,LPCTSTR,LPCTSTR,DWORD,LPCTSTR);

	virtual BOOL OnInitDialog();
};

// Handle Jonas �. DLL.
class CJonasXML_handling
{
	//HINSTANCE m_hModule;

protected:
	int openHnd(char *);
	CString m_sUpdateTraktMsg;
	CString m_sErrCap;
public:
	CXmlReader mTrakt;	
	CJonasXML_handling(void);
	~CJonasXML_handling(void)
	{

	}

	BOOL enterDataToDB(DB_CONNECTION_DATA con,char *,char *,TRAKT_IDENTIFER *,
										 LPCTSTR cap = _T(""),LPCTSTR msg_1 = _T(""),LPCTSTR msg_2 = _T(""));


	bool doReplaceTrakt(TRAKT_IDENTIFER *Trakt);
	/*int nGetRegion(void)			{ return mTrakt.nGetRegion(); }
	int nGetDistrict(void)		{ return mTrakt.nGetDistrict(); }
	double fGetAreal(void)			{ return mTrakt.fGetAreal(); }
	int nGetType(void)			{ return mTrakt.nGetType(); }
	int nGetOrigin(void)			{ return mTrakt.nGetOrigin(); }
	int nGetTraktNum(void)		{ return mTrakt.nGetTraktNum(); }
	int nGetMachineNum(void)		{ return mTrakt.nGetMachineNum(); }*/

	//CString getObjectName(void)		{ return sObjectName; }
	//CString getContractor(void)		{ return sContractor; }
	
	//CString getDate(void)			{ return mTrakt.sDate; }
};

#endif
