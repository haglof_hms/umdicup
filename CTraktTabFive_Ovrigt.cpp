#include "stdafx.h"
#include "CTraktTabFive_Ovrigt.h"

#include "ResLangFileReader.h"
#include "UMDiCupGenerics.h"
//#include ".\dicuptraktpages.h"

// -------------------------------------------------------------------------
//	Trakt Page 5 �vrigt
// -------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CTraktPageFive_Ovrigt, CXTResizeFormView)
BEGIN_MESSAGE_MAP(CTraktPageFive_Ovrigt, CXTResizeFormView)
END_MESSAGE_MAP()

CTraktPageFive_Ovrigt::CTraktPageFive_Ovrigt()
	: CXTResizeFormView(CTraktPageFive_Ovrigt::IDD)
{
	m_bInitialized=FALSE;
}
CTraktPageFive_Ovrigt::~CTraktPageFive_Ovrigt()
{
}

void CTraktPageFive_Ovrigt::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)

	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE5_KVARVIRKE, m_wndEdit_Trakt_Page5_KvarVirke);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE5_BRISTER, m_wndEdit_Trakt_Page5_Brister);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE5_AVVIKANDE, m_wndEdit_Trakt_Page5_Avvikande);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE5_HANSYN, m_wndEdit_Trakt_Page5_Hansyn);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE5_ANDELHOGST, m_wndEdit_Trakt_Page5_HogstProc);
	DDX_Control(pDX, IDC_EDIT_TRAKT_PAGE5_FORBATTR, m_wndEdit_Trakt_Page5_Forbattr);

	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE5_BASKR, m_wndCBox_Trakt_Page5_Baskr);
	DDX_Control(pDX, IDC_COMBO_TRAKT_PAGE5_STRATEG, m_wndCBox_Trakt_Page5_Strateg);
}

void CTraktPageFive_Ovrigt::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

		m_wndEdit_Trakt_Page5_KvarVirke.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page5_KvarVirke.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page5_KvarVirke.SetAsNumeric();

		m_wndEdit_Trakt_Page5_Brister.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page5_Brister.SetDisabledColor(BLACK,INFOBK);

		m_wndEdit_Trakt_Page5_Avvikande.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page5_Avvikande.SetDisabledColor(BLACK,INFOBK);

		m_wndEdit_Trakt_Page5_Hansyn.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page5_Hansyn.SetDisabledColor(BLACK,INFOBK);

		m_wndEdit_Trakt_Page5_HogstProc.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page5_HogstProc.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit_Trakt_Page5_HogstProc.SetAsNumeric();

		m_wndEdit_Trakt_Page5_Forbattr.SetEnabledColor(BLACK,WHITE);
		m_wndEdit_Trakt_Page5_Forbattr.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page5_Baskr.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page5_Baskr.SetDisabledColor(BLACK,INFOBK);

		m_wndCBox_Trakt_Page5_Strateg.SetEnabledColor(BLACK,WHITE);
		m_wndCBox_Trakt_Page5_Strateg.SetDisabledColor(BLACK,INFOBK);


		clearAll();

		setTraktValArrays();
		resetIsDirty();
		m_bInitialized = TRUE;
	}
}

void CTraktPageFive_Ovrigt::clearAll(void)
{
	m_wndEdit_Trakt_Page5_KvarVirke.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page5_Brister.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page5_Avvikande.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page5_Hansyn.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page5_HogstProc.SetWindowText(_T(""));
	m_wndEdit_Trakt_Page5_Forbattr.SetWindowText(_T(""));
	m_wndCBox_Trakt_Page5_Baskr.SetCurSel(-1);
	m_wndCBox_Trakt_Page5_Strateg.SetCurSel(-1);
}

void CTraktPageFive_Ovrigt::setAllReadOnly(BOOL ro1)
{
	m_wndEdit_Trakt_Page5_KvarVirke.SetReadOnly( TRUE );
	m_wndEdit_Trakt_Page5_Brister.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page5_Avvikande.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page5_Hansyn.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page5_HogstProc.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Page5_Forbattr.SetReadOnly( ro1 );

	m_wndCBox_Trakt_Page5_Baskr.SetReadOnly( ro1 );
	m_wndCBox_Trakt_Page5_Strateg.SetReadOnly( ro1 );
}

void CTraktPageFive_Ovrigt::setEnabled(BOOL enabled)
{
	m_wndEdit_Trakt_Page5_Brister.EnableWindow( enabled );
	m_wndEdit_Trakt_Page5_Avvikande.EnableWindow( enabled );
	m_wndEdit_Trakt_Page5_Hansyn.EnableWindow( enabled );
	m_wndEdit_Trakt_Page5_HogstProc.EnableWindow( enabled );
	m_wndEdit_Trakt_Page5_Forbattr.EnableWindow( enabled );

	m_wndCBox_Trakt_Page5_Baskr.EnableWindow( enabled );
	m_wndCBox_Trakt_Page5_Strateg.EnableWindow( enabled );

	setAllReadOnly(!enabled);	
}

void CTraktPageFive_Ovrigt::populateData(TRAKT_DATA data)
{
	CString s_Data=_T("");

	s_Data.Format(_T("%d"),data.m_n_baskrTraktplOK);
	if(data.m_n_baskrTraktplOK==-1)
		m_wndCBox_Trakt_Page5_Baskr.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page5_Baskr.SelectString(0,s_Data);

	s_Data.Format(_T("%d"),data.m_n_klaratStrategiskaMal);
	if(data.m_n_klaratStrategiskaMal==-1)
		m_wndCBox_Trakt_Page5_Strateg.SetCurSel(0);
	else
		m_wndCBox_Trakt_Page5_Strateg.SelectString(0,s_Data);

	m_wndEdit_Trakt_Page5_KvarVirke.setInt(data.m_n_kvarglomtVirkeSum);
	m_wndEdit_Trakt_Page5_Brister.SetWindowText(data.m_s_baskrTraktplBrister);
	m_wndEdit_Trakt_Page5_Avvikande.SetWindowText(data.m_s_avvikandeArbUtifranTraktdirektivet);
	m_wndEdit_Trakt_Page5_Hansyn.SetWindowText(data.m_s_hansynTillEfterkommandeAtgarder);
	m_wndEdit_Trakt_Page5_HogstProc.setInt(data.m_n_stubbhojd);
	m_wndEdit_Trakt_Page5_Forbattr.SetWindowText(data.m_s_forbattringsforslag);

	setAllReadOnly(FALSE);
	resetIsDirty();
}

void CTraktPageFive_Ovrigt::getEnteredData(TRAKT_DATA *trakt)
{	

	trakt->m_n_baskrTraktplOK			=m_wndCBox_Trakt_Page5_Baskr.GetItemData(m_wndCBox_Trakt_Page5_Baskr.GetCurSel());
	trakt->m_n_klaratStrategiskaMal	=m_wndCBox_Trakt_Page5_Strateg.GetItemData(m_wndCBox_Trakt_Page5_Strateg.GetCurSel());

	//trakt->m_n_kvarglomtVirkeSum	=	m_wndEdit_Trakt_Page5_KvarVirke.getInt();
	trakt->m_s_baskrTraktplBrister	=	m_wndEdit_Trakt_Page5_Brister.getText();
	trakt->m_s_avvikandeArbUtifranTraktdirektivet	=	m_wndEdit_Trakt_Page5_Avvikande.getText();
	trakt->m_s_hansynTillEfterkommandeAtgarder		=	m_wndEdit_Trakt_Page5_Hansyn.getText();
	trakt->m_n_stubbhojd			=	m_wndEdit_Trakt_Page5_HogstProc.getInt();
	trakt->m_s_forbattringsforslag	=	m_wndEdit_Trakt_Page5_Forbattr.getText();
}

BOOL CTraktPageFive_Ovrigt::getIsDirty(void)
{
	if (m_wndEdit_Trakt_Page5_Brister.isDirty() ||
	m_wndEdit_Trakt_Page5_Avvikande.isDirty() ||
	m_wndEdit_Trakt_Page5_Hansyn.isDirty() ||
	m_wndEdit_Trakt_Page5_HogstProc.isDirty() ||
	m_wndEdit_Trakt_Page5_Forbattr.isDirty() ||
	m_wndCBox_Trakt_Page5_Baskr.isDirty() ||
	m_wndCBox_Trakt_Page5_Strateg.isDirty())
	{
		return TRUE;
	}
	return FALSE;
}

void CTraktPageFive_Ovrigt::resetIsDirty(void)
{
	m_wndEdit_Trakt_Page5_Brister.resetIsDirty();
	m_wndEdit_Trakt_Page5_Avvikande.resetIsDirty();
	m_wndEdit_Trakt_Page5_Hansyn.resetIsDirty();
	m_wndEdit_Trakt_Page5_HogstProc.resetIsDirty();
	m_wndEdit_Trakt_Page5_Forbattr.resetIsDirty();
	m_wndCBox_Trakt_Page5_Baskr.resetIsDirty();
	m_wndCBox_Trakt_Page5_Strateg.resetIsDirty();
}

void CTraktPageFive_Ovrigt::setTraktValArrays()
{	
	TRAKT_VAL_DATA data;
	CString s_Data=_T("");
	UINT i=0;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			vec_Trakt_Page5_Baskr.clear();
			vec_Trakt_Page5_Baskr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page5_Baskr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6201)),xml->str(IDS_STRING6200)));
			vec_Trakt_Page5_Baskr.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6203)),xml->str(IDS_STRING6202)));

			vec_Trakt_Page5_Strateg.clear();
			vec_Trakt_Page5_Strateg.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING7111)),xml->str(IDS_STRING7110)));
			vec_Trakt_Page5_Strateg.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6201)),xml->str(IDS_STRING6200)));
			vec_Trakt_Page5_Strateg.push_back(_trakt_val_data(_tstoi(xml->str(IDS_STRING6203)),xml->str(IDS_STRING6202)));
		}
	}

	for (i = 0;i < vec_Trakt_Page5_Baskr.size();i++)
	{
		data = vec_Trakt_Page5_Baskr[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page5_Baskr.AddString(s_Data);
		m_wndCBox_Trakt_Page5_Baskr.SetItemData(i, data.m_nID);		
	}

	for (i = 0;i < vec_Trakt_Page5_Strateg.size();i++)
	{
		data = vec_Trakt_Page5_Strateg[i];
		if(data.m_nID==-1)
			s_Data.Format(_T("%s"),data.m_sName);
		else
			s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Page5_Strateg.AddString(s_Data);
		m_wndCBox_Trakt_Page5_Strateg.SetItemData(i, data.m_nID);		
	}
}