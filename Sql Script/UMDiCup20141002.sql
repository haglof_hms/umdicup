use "databasnamn"

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'typAvUppfoljning')  
alter table holmen_slutavv_trakt_table add typAvUppfoljning int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'rg')  
alter table holmen_slutavv_trakt_table add rg int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'di')  
alter table holmen_slutavv_trakt_table add di int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'ursprung')  
alter table holmen_slutavv_trakt_table add ursprung int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'traktnr')  
alter table holmen_slutavv_trakt_table add traktnr int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'traktnamn')  
alter table holmen_slutavv_trakt_table add traktnamn nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'avvEnlRus')  
alter table holmen_slutavv_trakt_table add avvEnlRus int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'finnsMiljorapport')  
alter table holmen_slutavv_trakt_table add finnsMiljorapport int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'traktareal')  
alter table holmen_slutavv_trakt_table add traktareal float

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'avverkadVolym')  
alter table holmen_slutavv_trakt_table add avverkadVolym float

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'maskinlagnr')  
alter table holmen_slutavv_trakt_table add maskinlagnr int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'maskinlagnamn')  
alter table holmen_slutavv_trakt_table add maskinlagnamn nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'datumInv')  
alter table holmen_slutavv_trakt_table add datumInv nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'datumSlutfordAvv')  
alter table holmen_slutavv_trakt_table add datumSlutfordAvv nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'utfortGrotsk')  
alter table holmen_slutavv_trakt_table add utfortGrotsk int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'datumSlutfordGrotsk')  
alter table holmen_slutavv_trakt_table add datumSlutfordGrotsk nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'naturvardestradSum')  
alter table holmen_slutavv_trakt_table add naturvardestradSum int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'framtidstradSum')  
alter table holmen_slutavv_trakt_table add framtidstradSum int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'farskaHogstubbarSum')  
alter table holmen_slutavv_trakt_table add farskaHogstubbarSum int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'sparadArealSum')  
alter table holmen_slutavv_trakt_table add sparadArealSum float  

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'sparadVolymSum')  
alter table holmen_slutavv_trakt_table add sparadVolymSum int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'kvarglomtVirkeSum')  
alter table holmen_slutavv_trakt_table add kvarglomtVirkeSum int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'risadeBasstrak')  
alter table holmen_slutavv_trakt_table add risadeBasstrak int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'hansynskrBiotoper')  
alter table holmen_slutavv_trakt_table add hansynskrBiotoper int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'mindreAllvKorskada')  
alter table holmen_slutavv_trakt_table add mindreAllvKorskada int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'mindreAllvKorskadaTyp')  
alter table holmen_slutavv_trakt_table add mindreAllvKorskadaTyp int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'allvarligKorskada')  
alter table holmen_slutavv_trakt_table add allvarligKorskada int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'allvarligKorskadaTyp')  
alter table holmen_slutavv_trakt_table add allvarligKorskadaTyp int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'forebyggtKorskador')  
alter table holmen_slutavv_trakt_table add forebyggtKorskador nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'forebyggtKorskadorKommentar')  
alter table holmen_slutavv_trakt_table add forebyggtKorskadorKommentar nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'basvagUtanforTrakt')  
alter table holmen_slutavv_trakt_table add basvagUtanforTrakt int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'kommentarAvKorskador')  
alter table holmen_slutavv_trakt_table add kommentarAvKorskador nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'skadadeVagdiken')  
alter table holmen_slutavv_trakt_table add skadadeVagdiken int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'parallellitet')  
alter table holmen_slutavv_trakt_table add parallellitet int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'hogarnasBeskaffenhet')  
alter table holmen_slutavv_trakt_table add hogarnasBeskaffenhet int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'placeringAvHogar')  
alter table holmen_slutavv_trakt_table add placeringAvHogar int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'innehallHogar')  
alter table holmen_slutavv_trakt_table add innehallHogar int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'kvalitetPaValtor')  
alter table holmen_slutavv_trakt_table add kvalitetPaValtor int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'kommentarPaGrottillredning')  
alter table holmen_slutavv_trakt_table add kommentarPaGrottillredning nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'myr')  
alter table holmen_slutavv_trakt_table add myr int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'hallmark')  
alter table holmen_slutavv_trakt_table add hallmark int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'omrSomLamnOrorda')  
alter table holmen_slutavv_trakt_table add omrSomLamnOrorda int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'objektSomSkallSkotas')  
alter table holmen_slutavv_trakt_table add objektSomSkallSkotas int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'kulturminnen')  
alter table holmen_slutavv_trakt_table add kulturminnen int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'fornminnen')  
alter table holmen_slutavv_trakt_table add fornminnen int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'skyddszoner')  
alter table holmen_slutavv_trakt_table add skyddszoner int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'naturvardestrad')  
alter table holmen_slutavv_trakt_table add naturvardestrad int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'framtidstrad')  
alter table holmen_slutavv_trakt_table add framtidstrad int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'farskaHogstubbar')  
alter table holmen_slutavv_trakt_table add farskaHogstubbar int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'dodaTrad')  
alter table holmen_slutavv_trakt_table add dodaTrad int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'kalytebegransning')  
alter table holmen_slutavv_trakt_table add kalytebegransning int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'nedskrapning')  
alter table holmen_slutavv_trakt_table add nedskrapning int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'avverkningstidpunkt')  
alter table holmen_slutavv_trakt_table add avverkningstidpunkt int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'upplevelsehansyn')  
alter table holmen_slutavv_trakt_table add upplevelsehansyn int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'teknEkonimp')  
alter table holmen_slutavv_trakt_table add teknEkonimp int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'ovrigHansyn')  
alter table holmen_slutavv_trakt_table add ovrigHansyn nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'ovrigaKommentarerNaturOchMiljo')  
alter table holmen_slutavv_trakt_table add ovrigaKommentarerNaturOchMiljo nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'baskrTraktplOK')  
alter table holmen_slutavv_trakt_table add baskrTraktplOK int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'baskrTraktplBrister')  
alter table holmen_slutavv_trakt_table add baskrTraktplBrister nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'avvikandeArbUtifranTraktdirektivet')  
alter table holmen_slutavv_trakt_table add avvikandeArbUtifranTraktdirektivet nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'hansynTillEfterkommandeAtgarder')  
alter table holmen_slutavv_trakt_table add hansynTillEfterkommandeAtgarder nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'stubbhojd')  
alter table holmen_slutavv_trakt_table add stubbhojd int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'klaratStrategiskaMal')  
alter table holmen_slutavv_trakt_table add klaratStrategiskaMal int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'holmen_slutavv_trakt_table' and column_name = 'forbattringsforslag')  
alter table holmen_slutavv_trakt_table add forbattringsforslag nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'enterType;
alter table holmen_slutavv_trakt_table add enterType int
