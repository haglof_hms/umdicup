/////////////////////////////////////////////////////////////////////////////////////
// AddDataToDataBase; 
#include "StdAfx.h"
#include "AddDataToDataBase.h"
#include "ComfirmationDlg.h"
#include "ResLangFileReader.h"





CMyFileDialog::CMyFileDialog(LPCTSTR title,BOOL open,LPCTSTR def_ext,LPCTSTR filename,DWORD flags,LPCTSTR filter)
	: CFileDialog(open,def_ext,filename,flags,filter)
{
	m_sTitle = title;
}

BOOL CMyFileDialog::OnInitDialog()
{

	GetParent()->SetWindowText(m_sTitle);

	return CFileDialog::OnInitDialog();
}

///////////////////////////////////////////////////////////////////////////////////
//	CJonasDLL_handling

CJonasXML_handling::CJonasXML_handling()
{

	CString sLangFN=_T("");
	sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			m_sUpdateTraktMsg.Format(_T("%s\n\n%s\n%s\n\n%s"),
				xml->str(IDS_STRING5220),
				xml->str(IDS_STRING5221),
				xml->str(IDS_STRING5222),
				xml->str(IDS_STRING5223));
		}
	}
}

// PROTECTED
int CJonasXML_handling::openHnd(char *fn)
{
	int nReturn = 0;	
	nReturn = mTrakt.OpenFile(fn);
	return nReturn;
}


// PUBLIC

// This function takes care of entering data into the database.
// It enters: Machine,Trakt,Compartment and Plot for each selected
// file (fn); 061005 p�d
BOOL CJonasXML_handling::enterDataToDB(DB_CONNECTION_DATA con,char *fn,char *nopath_fn,
																			 TRAKT_IDENTIFER *data,LPCTSTR cap,LPCTSTR msg_1,LPCTSTR msg_2)
{
	// Datbase info data members

	int nReturn;
	BOOL bReturn;
	CString S;
	MACHINE_REG_DATA recMachineRegData;
	TRAKT_IDENTIFER trakt_ident;
	if ((nReturn = openHnd(fn)) == 1)
	{
		CDiCupDB *pDB = new CDiCupDB(con);
		CComfirmationDlg *dlg = new CComfirmationDlg();

#ifdef UNICODE
		USES_CONVERSION;
		dlg->setDataFileName(A2W(nopath_fn));
#else
		dlg->setDataFileName(nopath_fn);
#endif
		dlg->setRegion(mTrakt.nGetRegion());
		dlg->setDistrict(mTrakt.nGetDistrict());
		// Based on machinenumber set in file,
		// try to get the Contractors name from Databasetable
		// instead of the fileheader; 070419 p�d
		if (pDB != NULL)
		{
			recMachineRegData = MACHINE_REG_DATA(mTrakt.nGetMachineNum(),_T(""));
			pDB->getMachineInRegByNumber(recMachineRegData);
		}
		dlg->setMachine(mTrakt.nGetMachineNum(),
										recMachineRegData.m_sContractorName);
		dlg->setTrakt(&mTrakt);

		bReturn = (dlg->DoModal() == IDOK);
		if (bReturn)
		{
			// Get data from Dialog and add name of Contractor; 061005 p�d
			MACHINE_DATA recMachine;
			TRAKT_DATA recTrakt;

			// Get data set in Dialog; 061005 p�d
			dlg->getMachineData(recMachine);
			dlg->getTraktData(recTrakt);
			recTrakt.m_n_EnterType	= 0;	// = Data entered from a file; 061011 p�d

			// Add data to Database; 061005 p�d
			// Create instance of CDiCupDB class for
			// actual data transaction; 061005 p�d
			bReturn=FALSE;
			CDiCupDB *pDB = new CDiCupDB(con);
			if (pDB != NULL)
			{
					if (!pDB->addMachine(recMachine))
						pDB->updMachine(recMachine);

					if (!pDB->addTrakt(recTrakt)) //Traktdata finns, fr�ga om byta ut
					{
						trakt_ident.nRegionID		= recTrakt.m_n_rg;
						trakt_ident.nDistrictID	= recTrakt.m_n_di;
						trakt_ident.nMachineID	= recTrakt.m_n_maskinlagnr;
						trakt_ident.nTraktNum		= recTrakt.m_n_traktnr;
						trakt_ident.nType			= recTrakt.m_n_typAvUppfoljning;
						trakt_ident.nOrigin		= recTrakt.m_n_ursprung;
						
						if(doReplaceTrakt(&trakt_ident))
						{
							data->nRegionID		= recTrakt.m_n_rg;
							data->nDistrictID	= recTrakt.m_n_di;
							data->nMachineID	= recTrakt.m_n_maskinlagnr;
							data->nTraktNum		= recTrakt.m_n_traktnr;
							data->nType			= recTrakt.m_n_typAvUppfoljning;
							data->nOrigin		= recTrakt.m_n_ursprung;
							pDB->updTrakt(recTrakt,*data);	
							bReturn=TRUE;

						}
						else
							bReturn=FALSE;
					}
					else
					{
						bReturn=TRUE;
						data->nRegionID		= recTrakt.m_n_rg;
						data->nDistrictID	= recTrakt.m_n_di;
						data->nMachineID	= recTrakt.m_n_maskinlagnr;
						data->nTraktNum		= recTrakt.m_n_traktnr;
						data->nType			= recTrakt.m_n_typAvUppfoljning;
						data->nOrigin		= recTrakt.m_n_ursprung;					
					}
			}	// if (pDB != NULL)
		} // if (bReturn)
		if (dlg != NULL)
			delete dlg;
		if (pDB != NULL)
			delete pDB;
	}
	else
	{
		// Check for returnvalue and tell user accordingly. 
		// Returnvalues are set by Jons �.; 070425 p�d
		switch(nReturn)
		{
			case RETURN_ERR_FILEOPEN:
				::MessageBox(0,msg_1,cap,MB_ICONEXCLAMATION | MB_OK);
				break;
			case RETURN_ERR_TRAKT:
				::MessageBox(0,msg_2,cap,MB_ICONEXCLAMATION | MB_OK);
				break;
		}
		return FALSE;
	}

	return bReturn;
}
bool CJonasXML_handling::doReplaceTrakt(TRAKT_IDENTIFER *Trakt)
{
	bool bRet=false;
	if ((::MessageBox(0,m_sUpdateTraktMsg,m_sErrCap,MB_YESNO | MB_ICONSTOP) == IDYES))
		bRet=true;
	return bRet;
}

