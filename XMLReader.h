#if !defined(AFX_XMLREADER_H)
#define AFX_XMLREADER_H

//#4031 20140429 J� xml parser
#include "xmlParser.h"
//#include "UMDiCupDB.h"
#include <vector>
using namespace std;
#pragma once

#define XML_TAG_TRAKT_DICUPXML	_T("XML")
#define XML_TAG_NODE_TRAKT	_T("Trakt")

#define XML_TAG_TRAKT_Uppfdatum _T("Uppfdatum")


#define XML_TAG_TRAKT_TypAvUppfoljning	_T("TypAvUppfoljning")
#define XML_TAG_TRAKT_Region			_T("Region")
#define XML_TAG_TRAKT_Distrikt			_T("Distrikt")
#define XML_TAG_TRAKT_Ursprung			_T("Ursprung")
#define XML_TAG_TRAKT_Traktnr			_T("Traktnr")
#define XML_TAG_TRAKT_Traktnamn			_T("Traktnamn")
#define XML_TAG_TRAKT_AvvEnlRUS			_T("AvvEnlRUS")
#define XML_TAG_TRAKT_FinnsMiljorapport	_T("FinnsMiljorapport")
#define XML_TAG_TRAKT_Traktareal		_T("Traktareal")
#define XML_TAG_TRAKT_AvverkadVolym		_T("AvverkadVolym")
#define XML_TAG_TRAKT_Maskinlagnr		_T("Maskinlagnr")
#define XML_TAG_TRAKT_Maskinlagnamn		_T("Maskinlagnamn")
#define XML_TAG_TRAKT_DatumInv			_T("DatumInv")
#define XML_TAG_TRAKT_DatumSlutfordAvv	_T("DatumSlutfordAvv")
#define XML_TAG_TRAKT_UtfortGrotsk		_T("UtfortGrotsk")
#define XML_TAG_TRAKT_DatumSlutfordGrotsk	_T("DatumSlutfordGrotsk")
#define XML_TAG_TRAKT_NaturvardestradSum	_T("NaturvardestradSum")
#define XML_TAG_TRAKT_FramtidstradSum		_T("FramtidstradSum")
#define XML_TAG_TRAKT_FarskaHogstubbarSum	_T("FarskaHogstubbarSum")
#define XML_TAG_TRAKT_SparadArealSum		_T("SparadArealSum")
#define XML_TAG_TRAKT_SparadVolymSum		_T("SparadVolymSum")
#define XML_TAG_TRAKT_KvarglomtVirkeSum		_T("KvarglomtVirkeSum")
#define XML_TAG_TRAKT_RisadeBasstrak		_T("RisadeBasstrak")
#define XML_TAG_TRAKT_HansynskrBiotoper		_T("HansynskrBiotoper")
#define XML_TAG_TRAKT_MindreAllvKorskada	_T("MindreAllvKorskada")
#define XML_TAG_TRAKT_MindreAllvKorskadaTyp	_T("MindreAllvKorskadaTyp")
#define XML_TAG_TRAKT_AllvarligKorskada		_T("AllvarligKorskada")
#define XML_TAG_TRAKT_AllvarligKorskadaTyp	_T("AllvarligKorskadaTyp")
#define XML_TAG_TRAKT_ForebyggtKorskador	_T("ForebyggtKorskador")
#define XML_TAG_TRAKT_ForebyggtKorskadorKommentar	_T("ForebyggtKorskadorKommentar")
#define XML_TAG_TRAKT_BasvagUtanforTrakt			_T("BasvagUtanforTrakt")
#define XML_TAG_TRAKT_KommentarAvKorskador			_T("KommentarAvKorskador")
#define XML_TAG_TRAKT_SkadadeVagdiken				_T("SkadadeVagdiken")
#define XML_TAG_TRAKT_Parallellitet					_T("Parallellitet")
#define XML_TAG_TRAKT_HogarnasBeskaffenhet			_T("HogarnasBeskaffenhet")
#define XML_TAG_TRAKT_PlaceringAvHogar				_T("PlaceringAvHogar")
#define XML_TAG_TRAKT_InnehallHogar					_T("InnehallHogar")
#define XML_TAG_TRAKT_KvalitetPaValtor				_T("KvalitetPaValtor")
#define XML_TAG_TRAKT_KommentarPaGrottillredning	_T("KommentarPaGrottillredning")
#define XML_TAG_TRAKT_Myr							_T("Myr")
#define XML_TAG_TRAKT_Hallmark						_T("Hallmark")
#define XML_TAG_TRAKT_OmrSomLamnOrorda				_T("OmrSomLamnOrorda")
#define XML_TAG_TRAKT_ObjektSomSkallSkotas			_T("ObjektSomSkallSkotas")
#define XML_TAG_TRAKT_Kulturminnen					_T("Kulturminnen")
#define XML_TAG_TRAKT_Fornminnen					_T("Fornminnen")
#define XML_TAG_TRAKT_Skyddszoner					_T("Skyddszoner")
#define XML_TAG_TRAKT_Naturvardestrad				_T("Naturvardestrad")
#define XML_TAG_TRAKT_Framtidstrad					_T("Framtidstrad") 
#define XML_TAG_TRAKT_FarskaHogstubbar				_T("FarskaHogstubbar") 
#define XML_TAG_TRAKT_DodaTrad						_T("DodaTrad")
#define XML_TAG_TRAKT_Kalytebegransning				_T("Kalytebegransning")
#define XML_TAG_TRAKT_Nedskrapning					_T("Nedskrapning")
#define XML_TAG_TRAKT_Avverkningstidpunkt			_T("Avverkningstidpunkt")
#define XML_TAG_TRAKT_Upplevelsehansyn				_T("Upplevelsehansyn")
#define XML_TAG_TRAKT_TeknEkonimp					_T("TeknEkonimp")
#define XML_TAG_TRAKT_OvrigHansyn					_T("OvrigHansyn")
#define XML_TAG_TRAKT_OvrigaKommentarerNaturOchMiljo	_T("OvrigaKommentarerNaturOchMiljo")
#define XML_TAG_TRAKT_BaskrTraktplOK					_T("BaskrTraktplOK")
#define XML_TAG_TRAKT_BaskrTraktplBrister				_T("BaskrTraktplBrister")
#define XML_TAG_TRAKT_AvvikandeArbUtifranTraktdirektivet	_T("AvvikandeArbUtifranTraktdirektivet")
#define XML_TAG_TRAKT_HansynTillEfterkommandeAtgarder		_T("HansynTillEfterkommandeAtgarder")
#define XML_TAG_TRAKT_AndelHogaStubbar					_T("AndelHogaStubbar")
#define XML_TAG_TRAKT_KlaratStrategiskaMal				_T("KlaratStrategiskaMal")
#define XML_TAG_TRAKT_Forbattringsforslag				_T("Forbattringsforslag")


#define RETURN_ERR_FILEOPEN				-1
#define RETURN_ERR_TRAKT				-2



class CXmlReader
{
private:
	int	HXL_HandleHxlFile(XMLNode xNode);
	int HXL_RecTrakt(XMLNode xNode);	
	CString m_sLangFN;
public:	
	int m_n_typAvUppfoljning;
	int m_n_rg;
	int m_n_di;
	int m_n_ursprung;
	int m_n_traktnr;
	CString m_s_traktnamn;
	int m_n_avvEnlRus;
	int m_n_finnsMiljorapport;
	float m_f_traktareal;
	float m_f_avverkadVolym;
	int m_n_maskinlagnr;
	CString m_s_maskinlagnamn;
	CString m_s_datumInv;
	CString m_s_datumSlutfordAvv;
	int m_n_utfortGrotsk;
	CString m_s_datumSlutfordGrotsk;
	int m_n_naturvardestradSum;
	int m_n_framtidstradSum;
	int m_n_farskaHogstubbarSum;
	float m_f_sparadArealSum;
	int m_n_sparadVolymSum;
	int m_n_kvarglomtVirkeSum;
	int m_n_risadeBasstrak;
	int m_n_hansynskrBiotoper;
	int m_n_mindreAllvKorskada;
	int m_n_mindreAllvKorskadaTyp;
	int m_n_allvarligKorskada;
	int m_n_allvarligKorskadaTyp;
	int m_n_forebyggtKorskador;
	CString m_s_forebyggtKorskadorKommentar;
	int m_n_basvagUtanforTrakt;
	CString m_s_kommentarAvKorskador;
	int m_n_skadadeVagdiken;
	int m_n_parallellitet;
	int m_n_hogarnasBeskaffenhet;
	int m_n_placeringAvHogar;
	int m_n_innehallHogar;
	int m_n_kvalitetPaValtor;
	CString m_s_kommentarPaGrottillredning;
	int m_n_myr;
	int m_n_hallmark;
	int m_n_omrSomLamnOrorda;
	int m_n_objektSomSkallSkotas;
	int m_n_kulturminnen;
	int m_n_fornminnen;
	int m_n_skyddszoner;
	int m_n_naturvardestrad;
	int m_n_framtidstrad;
	int m_n_farskaHogstubbar;
	int m_n_dodaTrad;
	int m_n_kalytebegransning;
	int m_n_nedskrapning;
	int m_n_avverkningstidpunkt;
	int m_n_upplevelsehansyn;
	int m_n_teknEkonimp;
	CString m_s_ovrigHansyn;
	CString m_s_ovrigaKommentarerNaturOchMiljo;
	int m_n_baskrTraktplOK;
	CString m_s_baskrTraktplBrister;
	CString m_s_avvikandeArbUtifranTraktdirektivet;
	CString m_s_hansynTillEfterkommandeAtgarder;
	int m_n_stubbhojd;
	int m_n_klaratStrategiskaMal;
	CString m_s_forbattringsforslag;

	// Default constructor
	CXmlReader(void);
	
	~CXmlReader(void)
	{

	}
	/************************************************/
	/* �ppna klavefil                               */
	/************************************************/
	int OpenFile(char *filename);
	int nGetRegion(void){return m_n_rg;}
	int nGetDistrict(void){return m_n_di;}
	int nGetOrigin(void){return m_n_ursprung;}
	float fGetAreal(void){return m_f_traktareal;}
	int nGetType(void){return m_n_typAvUppfoljning;}
	int nGetTraktNum(void){return m_n_traktnr;}
	int nGetMachineNum(void){return m_n_maskinlagnr;}
	
	CString formatToDateStr(CString str);
};

#endif