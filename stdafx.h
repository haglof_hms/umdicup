// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT


#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#include <SQLAPI.h> // main SQLAPI++ header

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions


#include "pad_hms_miscfunc.h"
#include "pad_transaction_classes.h"	// HMSFuncLib header
// ... in PAD_DBTransactionLib
#include "DBBaseClass_SQLApi.h"	
#include "DBBaseClass_ADODirect.h"	// ... in DBTransaction_lib


/////////////////////////////////////////////////////////////////////
// My own includes

#define MSG_IN_SUITE				 				(WM_USER + 10)		// This identifer's used to send messages internally

/////////////////////////////////////////////////////////////////////
// Strings ids

#define IDS_STRING101					101
#define IDS_STRING102					102
#define IDS_STRING104					104
#define IDS_STRING105					105
#define IDS_STRING107					107
#define IDS_STRING108					108
#define IDS_STRING109					109
#define IDS_STRING110					110
#define IDS_STRING111					111
#define IDS_STRING112					112
#define IDS_STRING113					113

#define IDS_STRING200					200
#define IDS_STRING201					201
#define IDS_STRING202					202
#define IDS_STRING203					203
#define IDS_STRING204					204
#define IDS_STRING2040					2040
#define IDS_STRING2041					2041
#define IDS_STRING205					205
#define IDS_STRING206					206

#define IDS_STRING209					209
#define IDS_STRING210					210
#define IDS_STRING211					211
#define IDS_STRING2120					2120
#define IDS_STRING2121					2121
#define IDS_STRING2122					2122
#define IDS_STRING213					213
#define IDS_STRING214					214
#define IDS_STRING215					215
#define IDS_STRING216					216
#define IDS_STRING217					217
#define IDS_STRING218					218
#define IDS_STRING219					219
#define IDS_STRING220					220
#define IDS_STRING221					221
#define IDS_STRING222					222
#define IDS_STRING223					223
#define IDS_STRING224					224
#define IDS_STRING225					225
#define IDS_STRING226					226
#define IDS_STRING227					227
#define IDS_STRING228					228
#define IDS_STRING229					229
#define IDS_STRING230					230
#define IDS_STRING231					231
#define IDS_STRING232					232
#define IDS_STRING233					233
#define IDS_STRING234					234
#define IDS_STRING235					235
#define IDS_STRING236					236
#define IDS_STRING237					237
#define IDS_STRING238					238
#define IDS_STRING239					239
#define IDS_STRING240					240
#define IDS_STRING241					241
#define IDS_STRING242					242
#define IDS_STRING243					243
#define IDS_STRING244					244
#define IDS_STRING245					245
#define IDS_STRING246					246
#define IDS_STRING247					247
#define IDS_STRING248					248
#define IDS_STRING249					249
#define IDS_STRING250					250
#define IDS_STRING251					251
#define IDS_STRING254					254
#define IDS_STRING255					255
#define IDS_STRING256					256
#define IDS_STRING257					257
#define IDS_STRING258					258
#define IDS_STRING259					259
#define IDS_STRING260					260
#define IDS_STRING261					261
#define IDS_STRING262					262



#define IDS_STRING300					300
#define IDS_STRING301					301
#define IDS_STRING302					302
#define IDS_STRING303					303
#define IDS_STRING304					304
#define IDS_STRING305					305
#define IDS_STRING306					306
#define IDS_STRING307					307
#define IDS_STRING308					308
#define IDS_STRING311					311

#define IDS_STRING4000					4000
#define IDS_STRING4001					4001
#define IDS_STRING401					401
#define IDS_STRING4020					4020
#define IDS_STRING4021					4021
#define IDS_STRING403					403
#define IDS_STRING404					404
#define IDS_STRING405					405
#define IDS_STRING4060					4060
#define IDS_STRING4061					4061
#define IDS_STRING4062					4062


#define IDS_STRING5000					5000
#define IDS_STRING5010					5010
#define IDS_STRING5020					5020
#define IDS_STRING5030					5030
#define IDS_STRING5001					5001
#define IDS_STRING5011					5011
#define IDS_STRING5031					5031

#define IDS_STRING5032					5032
#define IDS_STRING5003					5003
#define IDS_STRING5033					5033
#define IDS_STRING5004					5004
#define IDS_STRING5041					5041
#define IDS_STRING5042					5042
#define IDS_STRING504					504
#define IDS_STRING505					505
#define IDS_STRING506					506
#define IDS_STRING507					507
#define IDS_STRING508					508
#define IDS_STRING509					509
#define IDS_STRING510					510
#define IDS_STRING511					511


#define IDS_STRING514					514
#define IDS_STRING5151					5151
#define IDS_STRING5152					5152
#define IDS_STRING5153					5153
#define IDS_STRING5161					5161
#define IDS_STRING5162					5162
#define IDS_STRING5163					5163
#define IDS_STRING5171					5171
#define IDS_STRING5172					5172
#define IDS_STRING5181					5181
#define IDS_STRING5182					5182
#define IDS_STRING5191					5191
#define IDS_STRING5192					5192
#define IDS_STRING5201					5201
#define IDS_STRING5202					5202
#define IDS_STRING5210					5210
#define IDS_STRING5211					5211
#define IDS_STRING5212					5212
#define IDS_STRING5220					5220
#define IDS_STRING5221					5221
#define IDS_STRING5222					5222
#define IDS_STRING5223					5223
#define IDS_STRING5224					5224

// ---  Trakt_Avv_Enl_Rus 
#define IDS_STRING6200					6200
#define IDS_STRING6201					6201
#define IDS_STRING6202					6202
#define IDS_STRING6203					6203

// --- Trakt_Finns_Miljorapp
#define IDS_STRING6210	6210
#define IDS_STRING6211	6211
#define IDS_STRING6212	6212
#define IDS_STRING6213	6213
// --- Trakt_Utford_Grot
#define IDS_STRING6220	6220
#define IDS_STRING6221	6221
#define IDS_STRING6222	6222
#define IDS_STRING6223	6223

#define IDS_STRING7000	7000
#define IDS_STRING7001	7001
#define IDS_STRING7002	7002
#define IDS_STRING7003	7003
#define IDS_STRING7004	7004
#define IDS_STRING7005	7005
#define IDS_STRING7006	7006

#define IDS_STRING7007	7007
#define IDS_STRING7008	7008
#define IDS_STRING7009	7009
#define IDS_STRING7010	7010
#define IDS_STRING7011	7011

#define IDS_STRING7020	7020
#define IDS_STRING7021	7021
#define IDS_STRING7022	7022
#define IDS_STRING7023	7023
#define IDS_STRING7024	7024
#define IDS_STRING7025	7025
#define IDS_STRING7026	7026
#define IDS_STRING7027	7027
#define IDS_STRING7028	7028
#define IDS_STRING7029	7029
#define IDS_STRING7030	7030
#define IDS_STRING7031	7031
#define IDS_STRING7032	7032
#define IDS_STRING7033	7033
#define IDS_STRING7034	7034
#define IDS_STRING70340	70340
#define IDS_STRING70341	70341
#define IDS_STRING7035	7035
#define IDS_STRING7036	7036
#define IDS_STRING7037	7037
#define IDS_STRING7038	7038
#define IDS_STRING7039	7039
#define IDS_STRING7040	7040
#define IDS_STRING7041	7041
#define IDS_STRING70420	70420
#define IDS_STRING70421	70421
#define IDS_STRING7042	7042
#define IDS_STRING7043	7043
#define IDS_STRING7044	7044
#define IDS_STRING7045	7045
#define IDS_STRING7046	7046
#define IDS_STRING7047	7047
#define IDS_STRING7048	7048
#define IDS_STRING7049	7049
#define IDS_STRING7050	7050
#define IDS_STRING7051	7051
#define IDS_STRING7052	7052
#define IDS_STRING7053	7053
#define IDS_STRING7054	7054
#define IDS_STRING7055	7055
#define IDS_STRING7056	7056
#define IDS_STRING7057	7057
#define IDS_STRING7058	7058
#define IDS_STRING7059	7059
#define IDS_STRING7060	7060
#define IDS_STRING7061	7061
#define IDS_STRING7062	7062
#define IDS_STRING7063	7063
#define IDS_STRING7064	7064
#define IDS_STRING7065	7065
#define IDS_STRING7066	7066
#define IDS_STRING7067	7067
#define IDS_STRING7068	7068
#define IDS_STRING7069	7069
#define IDS_STRING7070	7070
#define IDS_STRING7071	7071

#define IDS_STRING7080	7080
#define IDS_STRING7081	7081
#define IDS_STRING7082	7082
#define IDS_STRING7083	7083
#define IDS_STRING7084	7084
#define IDS_STRING7085	7085
#define IDS_STRING7086	7086
#define IDS_STRING7087	7087

#define IDS_STRING7090	7090
#define IDS_STRING7091	7091
#define IDS_STRING7092	7092
#define IDS_STRING7093	7093
#define IDS_STRING7094	7094
#define IDS_STRING7095	7095
#define IDS_STRING7096	7096
#define IDS_STRING7097	7097

#define IDS_STRING7098	7098
#define IDS_STRING7099	7099
#define IDS_STRING7100	7100
#define IDS_STRING7101	7101
#define IDS_STRING7102	7102
#define IDS_STRING7103	7103
#define IDS_STRING7104	7104
#define IDS_STRING7105	7105

#define IDS_STRING7110	7110
#define IDS_STRING7111	7111

#define IDC_REGION_REPORT				8000
#define IDC_DISTRICT_REPORT				8001
#define IDC_MACHINE_REPORT				8002
#define IDC_TRAKT_REPORT				8003

#define IDC_TABCONTROL_TRAKT			8010

#define ID_MSG_REGION_REPORT_SEL		8100
#define ID_MSG_DISTRICT_REPORT_SEL		8101
#define ID_MSG_MACHINE_REPORT_SEL		8102
#define ID_MSG_TRAKT_REPORT_SEL			8103
#define ID_MSG_COMPART_REPORT_SEL		8104
#define ID_MSG_PLOT_REPORT_SEL			8105

//////////////////////////////////////////////////////////////////////////////////////////
// Registry settings keys; 060918 p�d


//////////////////////////////////////////////////////////////////////////////////////////
// Misc. strings

#define PROGRAM_NAME				_T("UMDiCup")	// Used for Languagefile, registry entries etc.; 051114 p�d

//////////////////////////////////////////////////////////////////////////////////////////
// Enumerated value for Manually added or by file; 061201 p�d

typedef enum _action { MANUALLY,FROM_FILE,NOTHING } enumACTION;

//////////////////////////////////////////////////////////////////////////////////////////
// Enumerated value for reset values (e.g. resetTrakt); 061207 p�d

typedef enum _reset { 
					RESET_TO_FIRST_SET_NB,			// Set to first item and set Navigationbar
					RESET_TO_FIRST_NO_NB,			// Set to first item and no Navigationbar (disable all)
					RESET_TO_LAST_SET_NB,			// Set to last item and set Navigationbar
					RESET_TO_LAST_NO_NB,			// Set to last item and no Navigationbar (disable all)
					RESET_TO_JUST_ENTERED_SET_NB,	// Set to just entered data (from file or manually) and set Navigationbar
					RESET_TO_JUST_ENTERED_NO_NB		// Set to just entered data (from file or manually) and no Navigationbar
					} enumRESET;


//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of Window(s); 060209 p�d

#define MIN_X_SIZE_DICUP_REGION			400
#define MIN_Y_SIZE_DICUP_REGION			280

#define MIN_X_SIZE_DICUP_LIST_REGION		400
#define MIN_Y_SIZE_DICUP_LIST_REGION		280


#define MIN_X_SIZE_DICUP_DISTRICT			400
#define MIN_Y_SIZE_DICUP_DISTRICT			290

#define MIN_X_SIZE_DICUP_LIST_DISTRICT	400
#define MIN_Y_SIZE_DICUP_LIST_DISTRICT	280


#define MIN_X_SIZE_DICUP_MACHINES			400
#define MIN_Y_SIZE_DICUP_MACHINES			340

#define MIN_X_SIZE_DICUP_LIST_MACHINES	620
#define MIN_Y_SIZE_DICUP_LIST_MACHINES	400


#define MIN_X_SIZE_DICUP_TRAKT			780
#define MIN_Y_SIZE_DICUP_TRAKT			500

#define MIN_X_SIZE_DICUP_LIST_TRAKT		620
#define MIN_Y_SIZE_DICUP_LIST_TRAKT		400

#define MIN_X_SIZE_DICUP_MACHINE_REG		420
#define MIN_Y_SIZE_DICUP_MACHINE_REG		320


//////////////////////////////////////////////////////////////////////////////////////////
// Window placement registry key; 060904 p�d

#define REG_WP_DICUP_REGION_KEY			_T("UMDiCup\\Region\\Placement")
#define REG_WP_DICUP_LIST_REGION_KEY		_T("UMDiCup\\ListRegion\\Placement")

#define REG_WP_DICUP_DISTRICT_KEY			_T("UMDiCup\\District\\Placement")
#define REG_WP_DICUP_LIST_DISTRICT_KEY	_T("UMDiCup\\ListDistrict\\Placement")

#define REG_WP_DICUP_MACHINES_KEY			_T("UMDiCup\\Machines\\Placement")
#define REG_WP_DICUP_LIST_MACHINES_KEY	_T("UMDiCup\\ListMachines\\Placement")

#define REG_WP_DICUP_TRAKT_KEY			_T("UMDiCup\\Trakt\\Placement")
#define REG_WP_DICUP_LIST_TRAKT_KEY		_T("UMDiCup\\ListTrakt\\Placement")

#define REG_WP_DICUP_MACHINE_REG_KEY		_T("UMDiCup\\ActiveMachineReg\\Placement")

//////////////////////////////////////////////////////////////////////////////////////////
// Name of Tables in DiCup database; 061002 p�d

#define TBL_REGION				_T("region_table")
#define TBL_DISTRICT			_T("district_table")
#define TBL_MACHINE				_T("machine_table")
#define TBL_TRAKT				_T("holmen_slutavv_trakt_table")

#define TBL_ACTIVE_MACHINE		_T("active_machine_table")

//////////////////////////////////////////////////////////////////////////////////////////
// SQL Script files, used in DiCup; 061109 p�d
#define DICUP_TABLES			_T("DiCup_tables.sql")
#define DICUP_MACHINES_TABLE	_T("DiCup_machine_table.sql")

//////////////////////////////////////////////////////////////////////////////////////////
// Array of Type and Origin

struct _invertory_type_data
{
	int  sInvTypeNum;
	CString sInvTypeName;
};

const _invertory_type_data INVENTORY_TYPE_ARRAY[3] = { {0, _T("Central")}, {1, _T("Central/Distrikts")},{2, _T("Distrikts")} };


struct _origin_data
{
	int nOriginNum;
	CString sOriginName;
};

const _origin_data ORIGIN_ARRAY[2] = { {0, _T("Eget")}, {1, _T("K�p")} };

//////////////////////////////////////////////////////////////////////////////////////////
// Misc. strings

#define DEFAULT_EXTENSION			_T("*.xml")
//#define FILEOPEN_FILTER				_T("GallUp (*.txt;*.prn)|*.txt;*.prn|")

#define FILEOPEN_FILTER				_T("Slutavv (*.xml)|*.xml|")

// These files'll be created, if it doesn't already exists.
// It is used to hold information on settings, for report.
// I.e. FromRegion,ToRegion,FromDistrict,ToDistrict etc; 061023 p�d
#define REPORT_SETTINGS_FILE		_T("report_settings.txt")
#define REPORT_SETTINGS2_FILE		_T("report_settings2.txt")
#define REPORT_SETTINGS3_FILE		_T("report_settings3.txt")

// Set SQL Questions used in getMachines( ... )


//

#define SQL_MACHINE_QUESTION_FN		_T("SQL_machine.bin")

// Question 1 selects ALL machines for ALL regions and Districts; 061010 p�d
#define SQL_MACHINE_QUESTION1		_T("select a.machine_num,a.machine_contractor_num,c.region_num,c.region_name,b.district_num,b.district_name ") \
									_T("from %s a,%s b,%s c ") \
									_T("where a.machine_district_num = b.district_num and ") \
									_T("a.machine_region_num = b.district_region_num and ") \
									_T("a.machine_region_num = c.region_num ") \
									_T("order by a.machine_region_num,a.machine_district_num,a.machine_num")

// Question 2 selects machines for region = id and districts; 061010 p�d
#define SQL_MACHINE_QUESTION2		_T("select a.machine_num,a.machine_contractor_num,c.region_num,c.region_name,b.district_num,b.district_name ") \
									_T("from %s a,%s b,%s c ") \
									_T("where a.machine_district_num = b.district_num and ") \
									_T("a.machine_region_num = b.district_region_num and ") \
									_T("a.machine_region_num = c.region_num and ") \
									_T("c.region_num = %d ") \
									_T("order by a.machine_region_num,a.machine_district_num,a.machine_num")

// Question 3 selects machines for districts = id; 061010 p�d
#define SQL_MACHINE_QUESTION3		_T("select a.machine_num,a.machine_contractor_num,c.region_num,c.region_name,b.district_num,b.district_name ") \
									_T("from %s a,%s b,%s c ") \
									_T("where a.machine_district_num = b.district_num and ") \
									_T("a.machine_region_num = b.district_region_num and ") \
									_T("a.machine_region_num = c.region_num and ") \
									_T("b.district_num = %d ") \
									_T("order by a.machine_region_num,a.machine_district_num,a.machine_num")

// Question 3 selects machines for region = id1 and districts = id2; 061010 p�d
#define SQL_MACHINE_QUESTION4		_T("select a.machine_num,a.machine_contractor_num,c.region_num,c.region_name,b.district_num,b.district_name ") \
									_T("from %s a,%s b,%s c ") \
									_T("where a.machine_district_num = b.district_num and ") \
									_T("a.machine_region_num = b.district_region_num and ") \
									_T("a.machine_region_num = c.region_num and ") \
									_T("c.region_num = %d and b.district_num = %d ") \
									_T("order by a.machine_region_num,a.machine_district_num,a.machine_num")



// SELECT SQL question, used in Trakt,Compartment and Plots; 061016 p�d
#define SQL_SELECT					_T("select * from %s where ")

// Set SQL Questions used in getTrakts( ... )

#define SQL_TRAKT_QUESTION_FN		_T("SQL_DiCup_trakt.bin")

// Question 1 selects ALL "trakts" for region,district and machine; 061011 p�d
#define SQL_TRAKT_QUESTION1			_T("select * from %s order by region,distrikt,maskinlagnr,traktnr")


#define SQL_TRAKT_ORDER_BY			_T("order by region,distrikt,maskinlagnr,traktnr")

#define SQL_TRAKT_QUEST				_T("traktnr=%d ")
#define SQL_TRAKT_TYPE_QUEST		_T("typ=\'%d\' ")
#define SQL_TRAKT_ORIGIN_QUEST		_T("ursprung=%d ")
#define SQL_TRAKT_REGION_QUEST		_T("region=%d ")
#define SQL_TRAKT_DISTRICT_QUEST	_T("distrikt=%d ")
#define SQL_TRAKT_MACHINE_QUEST		_T("maskinlagnr=%d ")

// Set SQL Questions used in getCompart( ... )



//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions

long filesize(FILE *);

double my_atof(LPCTSTR var);

CString getInvType_Name(int nType);

CString getOrigin_Name(int nOrigin);

void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

class Scripts
{
	CString _TableName;
	CString _Script;
	CString _DBName;
public:
	enum actionTypes { TBL_CREATE,TBL_ALTER };

	Scripts(void)
	{
		_TableName = _T("");
		_Script = _T("");
		_DBName = _T("");
	}

	Scripts(LPCTSTR table_name,LPCTSTR script,LPCTSTR db_name)
	{
		_TableName = table_name;
		_Script = script;
		_DBName = db_name;
	}

	CString getTableName()	{ return _TableName; }
	CString getScript()			{ return _Script; }
	CString getDBName()			{ return _DBName; }
};


typedef std::vector<Scripts> vecScriptFiles;

BOOL runSQLScriptFileEx1(vecScriptFiles &vec,Scripts::actionTypes action);

// DATA STRUCTURES
typedef struct _trakt_val_data
{
	int m_nID;
	CString m_sName;

	_trakt_val_data(void)
	{
		m_nID = -1;
		m_sName = "";
	}

	_trakt_val_data(int id,LPCTSTR name)
	{
		m_nID = id;
		m_sName = name;
	}
} TRAKT_VAL_DATA;

